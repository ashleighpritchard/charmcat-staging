<?php
/**
 *	Add and display main admin settings page
 *
 *	@package UltimateWoo Pro
 *	@author UltimateWoo
 *	@since 1.0
 */

//* Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'UltimateWoo_Pro_Settings_Page' ) ) :

class UltimateWoo_Pro_Settings_Page {

	private $settings;

	public function __construct() {

		require_once 'module-sections.php';

		require_once 'process-settings.php';

		add_action( 'admin_init', array( $this, 'init' ) );

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );

		add_action( 'admin_menu', array( $this, 'settings_page_callback' ), 9998 );

		add_action( 'ultimatewoo_pro_module_box_after_payment_gateways', array( $this, 'autorize_net_sim_enable' ) );
	}

	/**
	 *	Initialize
	 */
	public function init() {

		$this->settings = ultimatewoo_get_settings();
	}

	/**
	 *	Load admin styles/scripts
	 */
	public function enqueues() {

		// Load on our settings page
		if ( get_current_screen()->base !== 'woocommerce_page_ultimatewoo' ) {
			return;
		}

		// Admin CSS
		wp_enqueue_style( 'ultimatewoo-admin-styles', ULTIMATEWOO_PLUGIN_DIR_URL . 'assets/css/admin.css', '', ULTIMATEWOO_PRO_VERSION );
	}

	/**
	 *	Add the settings page to the admin menu
	 */
	public function settings_page_callback() {

		add_submenu_page( 'woocommerce', __( 'UltimateWoo - Enable Modules', 'ultimatewoo-pro' ), __( 'UltimateWoo', 'ultimatewoo-pro' ), 'manage_options', 'ultimatewoo', array( $this, 'render_settings_page' ) );
	}

	/**
	 *	Render settings page
	 */
	public function render_settings_page() {

		// Add success feedback after settings are saved
		if ( isset( $_GET['settings-updated'] ) && $_GET['settings-updated'] === 'true' ) {
			include_once 'templates/settings-updated.php';
		}

		$ultimatewoo_options = $this->settings;

		$sections = ultimatewoo_get_module_sections();

		include_once 'templates/main-upper.php';

		if ( apply_filters( 'ultimatewoo_settings_page_remove_license_boxes', false ) == false ) {
			include_once 'templates/form-license.php';
		}

		if ( isset( $ultimatewoo_options['license']['site_status'] ) && $ultimatewoo_options['license']['site_status'] == 'valid' ) {
			include_once 'templates/module-boxes.php';
		} else {
			include_once 'templates/site-inactive.php';
		}

		include_once 'templates/main-lower.php';
	}

	/**
	 *	Add the link for enabling Authorize.net SIM when AIM is active
	 *	Change WC_Authorize_Net_AIM->get_file() to public scope
	 */
	public function autorize_net_sim_enable() {

		global $status, $page, $s;

		// add an action to enabled the legacy SIM gateway
		if ( isset( $this->settings['modules']['authorize_net_aim'] ) && $this->settings['modules']['authorize_net_aim'] == 1 ) :
			
			echo '<div class="ultimatewoo-info">';

			printf( '<strong>%s</strong> ', __( 'Additional Option:', 'ultimatewoo-pro' ) );

			// Activate option if currently disabled, else option to Deactivate
			if ( get_option( 'wc_authorize_net_aim_sim_active' ) ) {
				printf( '<a href="%s">%s</a>',
					esc_url( wp_nonce_url( add_query_arg( array(
								'action'        => 'wc_authorize_net_toggle_sim',
								'gateway'       => 'deactivate',
								'plugin_status' => $status,
								'paged'         => $page,
								's'             => $s
							), 'admin.php'
						), wc_authorize_net_aim()->get_file() ) ),
					__( 'Deactivate SIM gateway', 'ultimatewoo-pro' )
				);
			} else {
				printf( '<a href="%s">%s</a>',
					esc_url( wp_nonce_url( add_query_arg( array(
								'action'        => 'wc_authorize_net_toggle_sim',
								'gateway'       => 'activate',
								'plugin_status' => $status,
								'paged'         => $page,
								's'             => $s
							), 'admin.php'
						), wc_authorize_net_aim()->get_file() ) ),
					__( 'Activate SIM gateway', 'ultimatewoo-pro' )
				);
			}

			echo '</div>';

		endif;
	}
}

endif;

new UltimateWoo_Pro_Settings_Page;