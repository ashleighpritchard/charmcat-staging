<?php

class UPS_Drop_Shipping_Product_Settings { 


	function __construct() { 
	
		add_action( 'add_meta_boxes', array( &$this, 'add_meta_box' ) );
		
		add_action( 'save_post', array( &$this, 'save_post' ), 1, 1 );
		
		add_action( 'edit_post', array( &$this, 'save_post' ), 1, 1 );

	}

	
	function add_meta_box() {
	
		add_meta_box( 'woocommerce-ups-shipping', __( 'UPS Drop Shipping', 'ignitewoo_ups_drop_ship' ), array( &$this, 'meta_box_content' ), 'product', 'side', 'default' );

	}

	
	function meta_box_content() {
		global $woocommerce, $post;

		$s = get_post_meta( $post->ID, 'ups_drop_ship_origin', true );

		?>
		
		<div class="ups_box" style="overflow: auto" >
		
		<?php 
		
		$c = new WC_Countries();

		$sp = get_post_meta( $post->ID, 'country_state', true );

		if ( !isset( $sp ) )
			$sp = null;

		?>
		
		<style>
			.restrict_states_box .chzn-container { width: 230px !important; }
		</style>
		
		<div style="overflow: auto" class="restrict_states_box">
			<p>
				<?php _e( 'Select the origin country and state/province. You can leave this set to "None" if you are not using negotiated rates. For negotiated rates and for LTL freight rates you must set a country and state/province correctly.', 'ignitewoo_restrict_shipping' )?>
			</p>
			<p>
			<select name="country_state" class="chosen_select" style="width:200px">
				<option value="" <?php selected( $sp, '', true ) ?>><?php _e( 'None', 'ignitewoo_ups_drop_ship' ) ?></option>
			<?php
			
			$state_groups = array();
			
			foreach( $c->countries as $k => $v ) {

				if ( !empty( $c->states[ $k ] ) && count( $c->states[ $k ] ) > 0 ) { 
				
					echo '<optgroup label="' . $v . '">'; 
					
					foreach( $c->states[ $k ] as $kk => $vv ) { 

						if ( $k . ':' . $kk == $sp )
							$selected = ' selected="selected"';
						else
							$selected = '';
							
						echo '<option value="' . $k . ':' . $kk . '" ' . $selected . '>' . $vv . '</option>';
					}
					
					echo '</optgroup>';
					
					if ( 'US' !== $k )
						$state_groups[] = array( 'name' => $k, 'label' => $v, 'states' => $c->states[ $k ] );
				
				} else { 
				
					if ( $k == $sp )
						$selected = ' selected="selected"';
					else
						$selected = '';
						
					echo '<option value="' . $k . '"' . $selected . '>' . $v . '</option>';
					
				}
			
			}

			?>
			</select>
			</p>

			<p>
				<?php _e( 'Enter the origin zip code', 'ignitewoo_ups_drop_ship' )?>
			</p>
			
			<p>
				<input type="text" name="ups_drop_ship_origin" value="<?php echo $s ?>">
			</p>
		</div>
		
		</div>
		
		<?php
	}

	
	function save_post( $post_id ) {
		global $post;
		
		if ( empty( $post ) )
			return;

		if ( !$_POST ) return $post_id;
		if ( is_int( wp_is_post_revision( $post_id ) ) ) return;
		if ( is_int( wp_is_post_autosave( $post_id ) ) ) return;
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
		if ( !current_user_can( 'edit_post', $post_id )) return $post_id;
		if ( $post->post_type != 'product' ) return $post_id;

		update_post_meta( $post_id, 'ups_drop_ship_origin', $_POST['ups_drop_ship_origin'] );
		update_post_meta( $post_id, 'country_state', $_POST['country_state'] );
	}

}

$ign_drop_ship_product_settings = new UPS_Drop_Shipping_Product_Settings();