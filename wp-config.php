<?php
# Database Configuration
define( 'DB_NAME', 'charmcat_snapshot' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'root' );
define( 'DB_HOST', 'localhost' );
# define( 'DB_HOST_SLAVE', '127.0.0.1' );
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', 'utf8_unicode_ci');
$table_prefix = 'wp_';

# Security Salts, Keys, Etc
define('AUTH_KEY',         'iH]WBprb?1yjnAsG8)1f=XzQLNEOM=RGt-G*h4P*wOc$=?QF/Q@Fc6;1V${Y%koV');
define('SECURE_AUTH_KEY',  'h>f1N|9$;(yAY/^h0NG3|?#X8n]cEx^E9_=x#18 =RehsW&^-,_pRBWa{<pdK.F-');
define('LOGGED_IN_KEY',    '?3>_WJ L+CzS*_4b(Srm`L*+MMJ/$ws!G0%fENw.t(*Wl0- be6a@|HiOejMe9yY');
define('NONCE_KEY',        'O|H:MiuXdmsERdf%U2?fk_E![P]i:-LfuaIm#Ehqqq-g{NAv290Np1O~di_dp9v|');
define('AUTH_SALT',        '**xaYr,V0<VSvz86Th cggVHteATNX")utX`Pd2jb*pni4jt8BKEg5p6h:5jdVnf');
define('SECURE_AUTH_SALT', 'XPL[1~Gt=S)0)8Qz2+D^vGlJy8xJ(Z41g-:?X>!.1C$)G*28$hs?Wmk,)V0>YHba');
define('LOGGED_IN_SALT',   ']}3q-zS>8=G?qNbSSxjHM/!2O]05!f{1/,TpCd]QGZ:!ziCY)Gy~u$8eMa`6oIqn');
define('NONCE_SALT',       'U+Yr0$g$Z?322W%Lb{SSsmsF>DpC6n`IiY^kUB%_n)KuruZVH9QKZ=Y"Vy02-.vY');


# Localized Language Stuff

define( 'WP_CACHE', FALSE );

define( 'WP_AUTO_UPDATE_CORE', false );

define( 'PWP_NAME', 'charmcat' );

define( 'FS_METHOD', 'direct' );

define( 'FS_CHMOD_DIR', 0775 );

define( 'FS_CHMOD_FILE', 0664 );

define( 'PWP_ROOT_DIR', '/nas/wp' );

define( 'WPE_APIKEY', 'a2e315c892fece1dfe5b293c735b78adf3adfd35' );

define( 'WPE_FOOTER_HTML', "" );

define( 'WPE_CLUSTER_ID', '32080' );

define( 'WPE_CLUSTER_TYPE', 'pod' );

define( 'WPE_ISP', true );

define( 'WPE_BPOD', false );

define( 'WPE_RO_FILESYSTEM', false );

define( 'WPE_LARGEFS_BUCKET', 'largefs.wpengine' );

define( 'WPE_SFTP_PORT', 2222 );

define( 'WPE_LBMASTER_IP', '172.99.67.168' );

define( 'WPE_CDN_DISABLE_ALLOWED', true );

define( 'DISALLOW_FILE_MODS', FALSE );

define( 'DISALLOW_FILE_EDIT', FALSE );

define( 'DISABLE_WP_CRON', false );

define( 'WPE_FORCE_SSL_LOGIN', false );

define( 'FORCE_SSL_LOGIN', false );

/*SSLSTART*/ if ( isset($_SERVER['HTTP_X_WPE_SSL']) && $_SERVER['HTTP_X_WPE_SSL'] ) $_SERVER['HTTPS'] = 'on'; /*SSLEND*/

define( 'WPE_EXTERNAL_URL', false );

define( 'WP_POST_REVISIONS', FALSE );

define( 'WPE_WHITELABEL', 'wpengine' );

define( 'WP_TURN_OFF_ADMIN_BAR', false );

define( 'WPE_BETA_TESTER', false );

umask(0002);

$wpe_cdn_uris=array ( );

$wpe_no_cdn_uris=array ( );

$wpe_content_regexs=array ( );

$wpe_all_domains=array ( 0 => 'charmcat.net', 1 => 'www.charmcat.net', 2 => 'charmcat.wpengine.com', );

$wpe_varnish_servers=array ( 0 => 'pod-32080', );

$wpe_special_ips=array ( 0 => '172.99.67.168', );

$wpe_ec_servers=array ( );

$wpe_largefs=array ( );

$wpe_netdna_domains=array ( );

$wpe_netdna_domains_secure=array ( );

$wpe_netdna_push_domains=array ( );

$wpe_domain_mappings=array ( );

$memcached_servers=array ( );

define( 'WP_SITEURL', 'http://localhost:8888/' );

define( 'WP_HOME', 'http://localhost:8888/' );

define('WPLANG','');

# WP Engine ID


# WP Engine Settings







define('ALTERNATE_WP_CRON', false);
define('WP_MEMORY_LIMIT', '128M');
define('WPV_LOGGING_STATUS', 'debug');
define('WP_DEBUG', true);



# That's It. Pencils down
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');

$_wpe_preamble_path = null; if(false){}
