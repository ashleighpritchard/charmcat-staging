<?php
/**
 * Coupons
 *
 * Makes coupons publicly queryable for Types access. Adds an author box for assigning the code to an affiliate member.
 *
 * @version   0.1
 * @author    CharmCat
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( !class_exists( 'PF_Affiliate' ) ) {

class PF_Affiliate {
        
    public function get_affiliate_coupon_code(){
        global $current_user;
        wp_get_current_user();

        $args = array(
            'posts_per_page'   => 1,
            'post_type'        => 'shop_coupon',
            'post_status'      => 'publish',
            'author'           => $current_user->ID,
            'orderby'          => 'date',
            'order'            => 'DESC'
        );

        $coupons = get_posts( $args );

        foreach ($coupons as $coupon){
            $coupon_code = get_the_title( $coupon );
        }
        
        return $coupon_code; //returns only the most recent code for this affiliate
    }

}
    
}
add_action( 'plugins_loaded', 'PF_Affiliate::get_affiliate_coupon_code' );