<?php
/**
 *	Sidebar
 *
 *	@package UltimateWoo Pro
 *	@author UltimateWoo
 *	@since 1.0
 */

// Variables - see admin-page.php for $ultimatewoo_options
$key = isset( $ultimatewoo_options['license']['license_key'] ) ? $ultimatewoo_options['license']['license_key'] : '';
$status = isset( $ultimatewoo_options['license']['site_status'] ) ? $ultimatewoo_options['license']['site_status'] : '';
$exp = isset( $ultimatewoo_options['license']['license_exp_date'] ) ? $ultimatewoo_options['license']['license_exp_date'] : '';
$limit = isset( $ultimatewoo_options['license']['license_limit'] ) ? (int) $ultimatewoo_options['license']['license_limit'] : '';
$remaining = isset( $ultimatewoo_options['license']['activations_left'] ) ? $ultimatewoo_options['license']['activations_left'] : '';
?>


<?php if ( apply_filters( 'ultimatewoo_settings_page_remove_license_boxes', false ) == false ) : // License Data ?>
<div id="ultimatewoo-license-data" class="postbox">

	<div class="handlediv" title="Click to toggle"><br></div>
	<!-- Toggle -->

	<h2 class="hndle"><?php esc_attr_e( 'License Data', 'ultimatewoo-pro' ); ?></h2>

	<div class="inside">

		<?php if ( $key && $status ) : // Site Status ?>
			<p id="site-status">
				<?php printf( '<strong>%s</strong>', __( 'Site Status: ', 'ultimatewoo-pro' ) ); ?>
				<?php
					echo ucwords( $status );
					if ( $status == 'valid' ) {
						printf( ' <img src="%s" width="20px" />', ULTIMATEWOO_PLUGIN_DIR_URL . '/assets/img/green-check.png' );
					}
				?>
			</p>
		<?php endif; ?>

		<?php if ( $key && $exp ) : // Exp Date ?>
			<p id="expiration">
				<?php printf( '<strong>%s</strong>', __( 'Expires: ', 'ultimatewoo-pro' ) ); ?>
				<?php echo date( 'F j, Y', strtotime( $exp ) ); ?>
			</p>
		<?php endif; ?>

		<?php if ( $key && is_numeric( $limit ) ) : // Limit ?>
			<p id="limit">
				<?php printf( '<strong>%s</strong>', __( 'Limit: ', 'ultimatewoo-pro' ) ); ?>
				<?php
					if ( $limit === 0 ) {
						_e( 'Unlimited', 'ultimatewoo-pro' );
					} else {
						echo $limit;
					}
				?>
			</p>
		<?php endif; ?>

		<?php if ( $key && $remaining ) : // Activations Left ?>
			<p id="remaining">
				<?php printf( '<strong>%s</strong>', __( 'Activations Remaining: ', 'ultimatewoo-pro' ) ); ?>
				<?php
					if ( $limit == 0 ) {
						_e( 'Unlimited', 'ultimatewoo-pro' );
					} else {
						echo $remaining;
					}
				?>
			</p>
		<?php endif; ?>

		<?php if ( ! $key ) : // No license saved ?>
			<p id="no-license"><?php _e( 'No License', 'ultimatewoo-pro' ); ?></p>
		<?php endif; ?>

	</div><!-- .inside -->

</div><!-- #ultimatewoo-license-data .postbox -->
<?php endif; ?>





<?php // System Info ?>
<div id="ultimatewoo-system" class="postbox">

	<div class="handlediv" title="Click to toggle"><br></div>
	<!-- Toggle -->

	<h2 class="hndle"><?php esc_attr_e( 'System Information', 'ultimatewoo-pro' ); ?></h2>

	<div class="inside">

		<p><?php printf( '<strong>%s</strong> %s', __( 'UltimateWoo Version:', 'ultimatewoo-pro' ), ULTIMATEWOO_PRO_VERSION ); ?></p>
		<p><?php printf( '<strong>%s</strong> %s', __( 'WooCommerce Version:', 'ultimatewoo-pro' ), get_option( 'woocommerce_version' ) ); ?></p>
		<p><?php printf( '<strong>%s</strong> %s', __( 'WordPress Version:', 'ultimatewoo-pro' ), get_bloginfo( 'version' ) ); ?></p>

	</div><!-- .inside -->

</div><!-- #ultimatewoo-system .postbox -->





<?php // Support Info ?>
<div id="ultimatewoo-support" class="postbox">

	<div class="handlediv" title="Click to toggle"><br></div>
	<!-- Toggle -->

	<h2 class="hndle"><?php esc_attr_e( 'Need support?', 'ultimatewoo-pro' ); ?></h2>

	<div class="inside">

		<p><?php printf( '%s <a href="https://www.ultimatewoo.com/account" target="_blank">%s</a> %s',
		__( 'To receive support for this site, please log in to your', 'ultimatewoo-pro' ),
		__( 'UltimateWoo account', 'ultimatewoo-pro' ),
		__( 'and submit a support ticket.', 'ultimatewoo-pro' ) ); ?></p>

	</div><!-- .inside -->

</div><!-- #ultimatewoo-support .postbox -->





<?php // Email Opt-in ?>
<div id="ultimatewoo-email-optin" class="postbox">

	<div class="handlediv" title="Click to toggle"><br></div>
	<!-- Toggle -->

	<h2 class="hndle"><?php esc_attr_e( 'UltimateWoo Updates?', 'ultimatewoo-pro' ); ?></h2>

	<div class="inside">

		<div id="mc_embed_signup">

			<form action="//ultimatewoo.us9.list-manage.com/subscribe/post?u=6285efd766a43c081d3c85446&amp;id=73736030d8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

				<div id="mc_embed_signup_scroll">

					<p><input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Enter Your Email Address" style="width: 100%;"></p>
					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;"><input type="text" name="b_6285efd766a43c081d3c85446_73736030d8" tabindex="-1" value=""></div>
					<p><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button-primary ultimatewoo-button" formtarget="_blank" style="width: 100%;"></p>
					<p><?php _e( 'We only send information you need as an UltimateWoo user...no spam or other junk.', 'ultimatewoo-pro' ) ?></p>

				</div><!-- #mc_embed_signup_scroll -->

			</form><!-- #mc-embedded-subscribe-form -->

		</div><!-- #mc_embed_signup -->

	</div><!-- .inside -->

</div><!-- #ultimatewoo-email-optin .postbox -->