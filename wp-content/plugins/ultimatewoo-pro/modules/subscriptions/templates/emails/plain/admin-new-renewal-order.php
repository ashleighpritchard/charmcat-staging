<?php
/**
 * Admin new renewal order email (plain text)
 *
 * @author	Brent Shepherd
 * @package WooCommerce_Subscriptions/Templates/Emails/Plain
 * @version 1.4
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

echo $email_heading . "\n\n";

// translators: $1: customer's billing first name, $2: customer's billing last name
printf( _x( 'You have received a subscription renewal order from %1$s %2$s. Their order is as follows:', 'Used in admin email: new renewal order', 'ultimatewoo-pro' ), $order->billing_first_name , $order->billing_last_name );

echo "\n\n";

echo "****************************************************\n\n";

do_action( 'woocommerce_email_before_order_table', $order, true, true );

printf( __( 'Order number: %s', 'ultimatewoo-pro' ), $order->get_order_number() ) . "\n";
printf( __( 'Order date: %s', 'ultimatewoo-pro' ), date_i18n( _x( 'jS F Y', 'date format for order date in notification emails', 'ultimatewoo-pro' ), strtotime( $order->order_date ) ) ) . "\n";

do_action( 'woocommerce_email_order_meta', $order, true, true );

echo "\n" . WC_Subscriptions_Email::email_order_items_table( $order, array(
	'show_download_links' => false,
	'show_sku'            => true,
	'show_purchase_note'  => '',
	'show_image'          => '',
	'image_size'          => '',
	'plain_text'          => true,
) );

echo "----------\n\n";

if ( $totals = $order->get_order_item_totals() ) {
	foreach ( $totals as $total ) {
		echo $total['label'] . "\t " . $total['value'] . "\n";
	}
}

echo "\n****************************************************\n\n";

do_action( 'woocommerce_email_after_order_table', $order, true, true );

echo __( 'Customer details', 'ultimatewoo-pro' ) . "\n";

if ( $order->billing_email ) {
	// translators: placeholder is customer's billing email
	echo sprintf( __( 'Email: %s', 'ultimatewoo-pro' ), $order->billing_email ) . "\n";
}

if ( $order->billing_phone ) {
	// translators: placeholder is customer's billing phone number
	echo sprintf( __( 'Tel: %s', 'ultimatewoo-pro' ), $order->billing_phone ) . "\n";
}

wc_get_template( 'emails/plain/email-addresses.php', array( 'order' => $order ) );

echo "\n****************************************************\n\n";

echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );
