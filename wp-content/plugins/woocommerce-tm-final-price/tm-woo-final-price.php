<?php
/*
Plugin Name: WooCommerce TM Final Price
Plugin URI: http://finalprice.themecomplete.com/
Description: A WooCommerce plugin to display Final price x quantity on product page
Version: 1.4.1
Author: themecomplete
Author URI: http://themecomplete.com/
*/

define ( 'TM_FP_PLUGIN_SECURITY', 1 );
define ( 'TM_FP_TRANSLATION', 'tm-final-price' );


include_once ('external/tm-functions.php');
if ( tm_woocommerce_check() ) {
    
    /**
     * Load plugin textdomain.
     */
    function tm_fp_load_textdomain() {
      load_plugin_textdomain( 'tm-final-price', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' ); 
    }
    add_action( 'plugins_loaded', 'tm_fp_load_textdomain' );    

    /**
     * Load main plugin interface
     */
    include_once ( 'class-tm-final-price.php' );
    $_TM_Final_Price = new TM_Final_Price();
    unset($_TM_Final_Price);
}
?>