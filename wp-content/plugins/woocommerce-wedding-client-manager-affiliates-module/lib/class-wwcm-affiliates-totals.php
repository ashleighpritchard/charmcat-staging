<?php
/**
 * Coupons
 *
 * Makes coupons publicly queryable for Types access. Adds an author box for assigning the code to an affiliate member.
 *
 * @version   0.1
 * @author    CharmCat
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( !class_exists( 'PF_Affiliate_Totals' ) ) {

class PF_Affiliate_Totals extends PF_Affiliate {
    
    //define
        
    /**
     * Returns array of orders for the coupon_code
     */
    public function get_affiliate_orders(){
        global $wpdb;
        global $order_ids;
        
        $coupon_code = PF_Affiliate::get_affiliate_coupon_code();
        
        $order_item_type = 'coupon';
        $discount_meta = 'discount_amount';
            
        $order_ids = $wpdb->get_results ( $wpdb->prepare( 
        "SELECT wp_woocommerce_order_items.order_id, wp_woocommerce_order_itemmeta.meta_value, wp_posts.post_date
        
        FROM wp_woocommerce_order_items 
        INNER JOIN wp_woocommerce_order_itemmeta 
        ON wp_woocommerce_order_items.order_item_id = wp_woocommerce_order_itemmeta.order_item_id 
        INNER JOIN wp_posts 
        ON wp_woocommerce_order_items.order_id = wp_posts.id 
        
        WHERE wp_woocommerce_order_items.order_item_type = %s
        AND wp_woocommerce_order_items.order_item_name = %s
        AND wp_woocommerce_order_itemmeta.meta_key = %s
            
        ORDER BY wp_posts.post_date",
        $order_item_type,
        $coupon_code,
        $discount_meta), ARRAY_A );
        
        return $order_ids;
    }
    
    /**
     * Returns total for all order_ids
     */
    public function get_affiliate_lifetime_earned_total(){

        global $woocommerce;
        $order_ids = PF_Affiliate_Totals::get_affiliate_orders();
        
        if ( $order_ids ) {

            $sum = 0;
                
            foreach($order_ids as $order_id) {

                $id = $order_id['order_id'];
                $order_date = $order_id['post_date'];
                $discount_amount = $order_id['meta_value'];

                /**
                 * Calculate earned amount for each order 
                 */
                $earned_amount_each = $discount_amount * 0.5;

                /**
                 * Calculate earned lifetime total 
                 */
                $earned_total += $earned_amount_each;

            }
            
        $earned_total_formatted = wc_price( $earned_total );

        return( $earned_total_formatted );

        }
    }

    /**
     * Total of earned amount from orders for the current quarter
     */
    public function get_affiliate_current_quarter_earned_total(){

        global $woocommerce;
        $order_ids = PF_Affiliate_Totals::get_affiliate_orders();
                
        if ( $order_ids ) {

            $sum = 0; 
            
            /**
             * Define current quarter 
             */
            $current_month = date("n");

            if($current_month>=1 && $current_month<=3) {
                $quarter = 1;
                $start_date = strtotime( mktime(0, 0, 0, 1, 1, date("Y") ) );
                $end_date = strtotime( mktime(0, 0, 0, 3, 31, date("Y") ) );
                $deposit_date =strtotime( mktime(0, 0, 0, 2, 15, date("Y") ) );

            } else  if($current_month>=4 && $current_month<=6) {
                $quarter = 2;
                $start_date = mktime(0, 0, 0, 4, 1, date("Y") );
                $end_date = mktime(0, 0, 0, 6, 30, date("Y") );
                $deposit_date = mktime(0, 0, 0, 5, 15, date("Y") );

            } else  if($current_month>=7 && $current_month<=9) {
                $quarter = 3;
                $start_date = strtotime( mktime(0, 0, 0, 7, 1, date("Y") ) );
                $end_date = strtotime( mktime(0, 0, 0, 9, 30, date("Y") ) );
                $deposit_date = strtotime( mktime(0, 0, 0, 8, 15, date("Y") ) );

            } else  if($current_month>=10 && $current_month<=12) {
                $quarter = 4;
                $start_date = strtotime( mktime(0, 0, 0, 10, 1, date("Y") ) );
                $end_date = strtotime( mktime(0, 0, 0, 12, 31, date("Y") ) );
                $deposit_date = strtotime( mktime(0, 0, 0, 11, 15, date("Y") +1 ) );
            }

            $current_quarter = new PF_Affiliate_Quarter($quarter, $start_date, $end_date, $deposit_date); 
                
            echo '<pre>Start ';
            echo $this->start_date;
            echo '<br/>End ';
            echo $this->end_date;
            echo '</pre>';
                
            foreach($order_ids as $order_id) {

                $id = $order_id['order_id'];
                $order_date = $order_id['post_date'];
                $discount_amount = $order_id['meta_value'];
                
                if (($order_date >= $start_date) && ($order_date <= $end_date)) {

                    /**
                     * Calculate earned amount for each order in current quarter
                     */
                    $current_quarter_earned_amount_each = $discount_amount * 0.5;

                    /**
                     * Calculate earned lifetime total in current quarter
                     */
                    $current_quarter_earned_total += $current_quarter_earned_amount_each;

                }
            
            $current_quarter_earned_total_formatted = wc_price( $current_quarter_earned_total );

            return( $current_quarter_earned_total_formatted );

            }
        }
    }

    /**
     * Display table with discount amount and order date
     */
    function display_affiliate_orders_table(){
        
        global $woocommerce;
        $order_ids = PF_Affiliate_Totals::get_affiliate_orders();
        
        if ( $order_ids ) {
            
        /**
         * Pre Table Fun Stuff.
         */
            echo '<div class="row"><div class="col-sm-12 col-md-6"><div>Your Affiliate Code: ';
            echo PF_Affiliate::get_affiliate_coupon_code();
            echo '</div>';

            echo '<div>Your Lifetime Affiliate Earnings: ';
            echo PF_Affiliate_Totals::get_affiliate_lifetime_earned_total();
            echo '</div></div>';

            echo '<div class="col-sm-12 col-md-6"><div>Your earnings this quarter: ';
            echo PF_Affiliate_Totals::get_affiliate_current_quarter_earned_total();
            echo '<div>This quarter started on ' . $this->start_date . ' and ends on ' . $this->end_date . '.</div>';
            echo '</div></div></div>';
        
        /**
         * Start Table.
         */
            echo '<div class="row"><div class="col-sm-12"><table class="table"><thead><th>Commission Earned</th><th>Order Date</th></thead>';

                foreach($order_ids as $order_id) {
                    
                    $id = $order_id['order_id'];
                    $order_date = $order_id['post_date'];
                    $discount_amount = $order_id['meta_value'];
                    
                    /**
                     * Calculate earned amount for each order 
                     */
                    $earned_amount_each = $discount_amount * 0.5;
                    $earned_amount_formatted = wc_price( $earned_amount_each );
                                                            
                    echo '<tr><td>', $earned_amount_formatted, '</td>';

                    echo '<td>', $order_date, '</td></tr>';
                    
                }

            echo '</table></div></div>';

        }else{
            echo 'You don\'t have any earnings yet.';
        }
    }

}
}