<?php
/**
 *	Form for enabling UltimateWoo modules
 *
 *	@package UltimateWoo Pro
 *	@author UltimateWoo
 *	@since 1.0
 */
?>

<form action="<?php echo add_query_arg( 'ultimatewoo-settings', 'modules', ULTIMATEWOO_SETTINGS_PAGE_URL ); ?>" method="post" id="ultimatewoo-modules-form">

	<?php foreach ( $sections as $section_key => $section ) : ?>

	<?php $count = 0; ?>

	<div class="postbox">

		<div class="handlediv" title="Click to toggle"><br></div>

		<h2 class="hndle"><?php echo $section['section_title']; ?></h2>

		<div class="inside">
			
			<?php foreach ( $section['section_modules'] as $module ) : ?>

				<?php

					// Assemble the key for proper saving
					$key = 'ultimatewoo[modules][' . $module['key'] . ']';

					// Whether option is enabled
					if ( isset( $ultimatewoo_options['modules'][$module['key']] ) && intval( $ultimatewoo_options['modules'][$module['key']] ) === 1 ) {
						$value = 1;
					} else {
						$value = '';
					}

					// CSS classes
					if ( $count === 0 || 0 === $count % 3 ) {
						$classes = 'one-third first';
					} else {
						$classes = 'one-third';
					}
				?>

				<p class="<?php echo $classes; ?>">
					<label for="<?php echo $key; ?>"><?php echo $module['title']; ?></label>
					<input type="checkbox" class="uw-settings-checkbox" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="1" <?php checked( intval( $value ), 1 ); ?> />
				</p>

				<?php

					$count++;

					// Clear after every three and last
					if ( 0 === $count % 3 || $count == sizeof( $section['section_modules'] ) ) {
						echo '<br class="clear">';
					}

				?>

			<?php endforeach; ?>

			<?php do_action( 'ultimatewoo_pro_module_box_after_' . $section_key ); ?>

			<?php // Submit button at bottom of each box ?>
			<p class="save-settings"><input type="submit" class="button-primary ultimatewoo-button" value="<?php _e( 'Save All Modules', 'ultimatewoo-pro' ); ?>"></p>

		</div><!-- .inside -->

	</div><!-- .postbox -->

	<?php endforeach; ?>

	<?php wp_nonce_field( 'ultimatewoo_modules_nonce', 'ultimatewoo_modules_nonce' ); ?>

</form>