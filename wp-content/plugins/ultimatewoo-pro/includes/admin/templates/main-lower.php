<?php
/**
 *	Main (upper) HTML markup for settings page.
 *
 *	@package UltimateWoo Pro
 *	@author UltimateWoo
 *	@since 1.0
 */
?>

				</div><!-- .meta-box-sortables .ui-sortable -->

			</div><!-- post-body-content -->

			<!-- sidebar -->
			<div id="postbox-container-1" class="postbox-container">

				<div class="meta-box-sortables">

					<?php do_action( 'ultimatewoo_settings_page_before_sidebar' ); ?>

					<?php include_once 'side.php'; ?>

					<?php do_action( 'ultimatewoo_settings_page_after_sidebar' ); ?>

				</div><!-- .meta-box-sortables -->

			</div><!-- #postbox-container-1 .postbox-container -->

		</div><!-- #post-body .metabox-holder .columns-2 -->

		<br class="clear">

		<?php do_action( 'ultimatewoo_settings_page_after_module_settings' ); ?>
	
	</div><!-- #poststuff -->

</div> <!-- .wrap -->