<?php
/**
WooCommerce UPS Drop Shipping classes
Copyright (c) 2014 - IgniteWoo.com  -- ALL RIGHTS RESERVED

No re-distribution rights. 

*/ 
if ( class_exists( 'woocommerce_shipping_method' ) ) {
	class ups_drop_ship_rate_base extends woocommerce_shipping_method {}
} else if ( class_exists( 'WC_Shipping_Method' ) ) {
	class ups_drop_ship_rate_base extends WC_Shipping_Method {}
}

class ups_drop_shipping_rate extends ups_drop_ship_rate_base {
	
	function __construct() { 

		$this->id = 'ups_drop_shipping_rate';

		$this->method_title = __( 'UPS Drop Shipping', 'woocommerce' );

		// Load form fields
		$this->init_form_fields();

		// Load settings
		$this->init_settings();

		// Debug messages
		$this->debug_messages = array();
		
		$this->temp_rates = array();

		add_action( 'woocommerce_review_order_after_submit', array( &$this, 'print_debug' ) );
		
		$this->freight_classes = array(
			'freight_50'   => 'CLASS_050',
			'freight_55'   => 'CLASS_055',
			'freight_60'   => 'CLASS_060',
			'freight_65'   => 'CLASS_065',
			'freight_70'   => 'CLASS_070',
			'freight_77_5' => 'CLASS_077_5',
			'freight_85'   => 'CLASS_085',
			'freight_92_5' => 'CLASS_092_5',
			'freight_100'  => 'CLASS_100',
			'freight_110'  => 'CLASS_100',
			'freight_125'  => 'CLASS_125',
			'freight_150'  => 'CLASS_150',
			'freight_175'  => 'CLASS_175',
			'freight_200'  => 'CLASS_200',
			'freight_250'  => 'CLASS_250',
			'freight_300'  => 'CLASS_300',
			'freight_400'  => 'CLASS_400',
			'freight_500'  => 'CLASS_500'
		);
		
		// User config vars
		$this->enabled		= isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : '';
		$this->title 		= isset(  $this->settings['title'] ) ? $this->settings['title'] : '';
		$this->availability 	= isset( $this->settings['availability'] ) ? $this->settings['availability'] : '';
		$this->countries 	= isset( $this->settings['countries'] ) ? $this->settings['countries'] : '';
		$this->type 		= isset( $this->settings['type'] ) ? $this->settings['type'] : '';
		$this->tax_status	= isset( $this->settings['tax_status'] ) ? $this->settings['tax_status'] : '';
		$this->fee 		= isset( $this->settings['fee'] ) ? $this->settings['fee'] : '';
		$this->license 		= isset( $this->settings['license'] ) ? $this->settings['license'] : '';
		$this->userid 		= isset( $this->settings['userid'] ) ? $this->settings['userid'] : '';
		$this->password 	= isset( $this->settings['password'] ) ? $this->settings['password'] : '';
		$this->shippernumber	= isset( $this->settings['shippernumber'] ) ? $this->settings['shippernumber'] : '';
		$this->origincountry	= isset( $this->settings['origincountry'] ) ? $this->settings['origincountry'] : '';
		$this->negotiated_rates	= isset( $this->settings['negotiated_rates'] ) ? $this->settings['negotiated_rates'] : '';
		$this->originstate	= isset( $this->settings['originstate'] ) ? $this->settings['originstate'] : '';
		$this->originzip	= isset( $this->settings['originzip'] ) ? $this->settings['originzip'] : '';
		$this->dimension_unit 	= isset( $this->settings['dimension_unit'] ) ? $this->settings['dimension_unit'] : '';
		$this->weight_unit	= isset( $this->settings['weight_unit'] ) ? $this->settings['weight_unit'] : '';
		$this->pickup_type	= isset( $this->settings['pickup_type'] ) ? $this->settings['pickup_type'] : '';
		$this->include_value	= isset( $this->settings['include_value'] ) ? $this->settings['include_value'] : '';
		$this->packaging_type	= isset( $this->settings['packaging_type'] ) ? $this->settings['packaging_type'] : '';
		$this->dev_server	= isset( $this->settings['dev_server'] ) ? $this->settings['dev_server'] : '';
		$this->destination_type = isset( $this->settings['destination_type'] ) ? $this->settings['destination_type'] : '';
		$this->fallback_rate 	= isset( $this->settings['fallback_rate'] ) ? $this->settings['fallback_rate'] : '';
		$this->shipping_style = isset( $this->settings['shipping_style'] ) ? $this->settings['shipping_style'] : '';
		$this->exclude_dimensions = isset( $this->settings['exclude_dimensions'] ) ? $this->settings['exclude_dimensions'] : '';
		$this->multi_packages = isset( $this->settings['multi_packages'] ) ? $this->settings['multi_packages'] : '';

		if ( isset( $this->settings['debug'] ) && 'yes' == $this->settings['debug'] )
			$this->debug = true;
		else if ( !isset( $this->settings['debug'] ) )
			$this->debug = false;
		else
			$this->debug = false;

		$this->use_cache	= 'no'; //$this->settings['use_cache']; 

		if ( !isset( $this->settings['origincountry'] ) || empty( $this->settings['origincountry'] ) )
			$this->origincountry = get_option( 'woocommerce_ups_rate_country', false );

		if ( !isset( $this->settings['originstate'] ) || empty( $this->settings['originstate'] ) )
			$this->originstate = get_option( 'woocommerce_ups_rate_state', false );

		if ( !isset( $this->settings['originzip'] ) || empty( $this->settings['originzip'] ) )
			$this->originzip = get_option( 'woocommerce_ups_rate_originzip', false );
		
		if ( is_admin() && isset( $_GET['ign_freight_test'] ) ) { 
			$res = $this->freight_test();
			
			if ( empty( $res ) ) { 
				update_option( 'ign_ups_freight_test_results', 'Error running Freight API test' );
			} else if ( is_array( $res ) && !empty( $res['error'] ) ) {
				update_option( 'ign_ups_freight_test_results', $res['error'] );
			} else if ( is_array( $res) && !empty( $res['rate'] ) ) {
				update_option( 'ign_ups_freight_test_results', $res['rate'] );
			}

			wp_redirect( admin_url( '/admin.php?page=wc-settings&tab=shipping&section=ups_drop_shipping_rate' ) );
			die;
			// re-init so the result is loaded 
			//$this->init_settings();
		}



		// admin settings 
		// As of WooCom 1.5.4 these hooks are use
		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_ups_rates' ) );
		
		// Earlier versions of WooCom rely on these hooks instead - keep for backward compatibility
		add_action('woocommerce_update_options_shipping_methods', array(&$this, 'process_admin_options'));
		add_action('woocommerce_update_options_shipping_methods', array(&$this, 'process_ups_rates'));

		// order page meta box
		add_action( 'add_meta_boxes', array( &$this, 'add_order_meta_box' ), -1 );

		// remove shipping method if no zip code is set
		add_action( 'plugins_loaded', array( &$this, 'remove_shipping_method' ), 99 );


		// checkout page - zip change script
		add_action( 'wp_head', array( &$this, 'insert_script' ) );

		// check for jurassic era servers
		add_action( 'init', array( &$this, 'software_tests' ), -1 );

		//add_action( 'woocommerce_after_order_notes', array( &$this, 'after_order_notes' ) );

	} 

	
	function get_title() { 
	
		return $this->method_title;
	
	}

	// insert script that removes previous shipping options while new rates are calculated
	function insert_script() { 
		global $woocommerce;
	?>
		<script>
		jQuery( document ).ready( function() { 
			jQuery('#billing_postcode').change( function() {
				jQuery('#shipping_method').replaceWith('<?php _e( '<img src="'.$woocommerce->plugin_url(). '/assets/images/ajax-loader.gif" align="absmiddle"> Calculating rates...', 'woocommerce' )?>');
			});
			jQuery('#shipping_postcode').change( function() {
				jQuery('#shipping_method').replaceWith('<?php _e( '<img src="'.$woocommerce->plugin_url(). '/assets/images/ajax-loader.gif" align="absmiddle"> Calculating rates...', 'woocommerce' )?>');
			});
		});
		</script>
	<?php
	}


	// Make sure the site is not running on Jurassic era software
	function software_tests() { 
		global $woocommerce;

		if ( is_admin() && !function_exists( 'curl_init' ) ) { 

			add_action( 'admin_notices',  array( &$this, 'curl_nag' ) );

		}

		if ( is_admin() && !class_exists( 'SimpleXMLElement' ) ) { 

			add_action( 'admin_notices',  array( &$this, 'simplexml_nag' ) );

		}

		if ( version_compare( $woocommerce->version, '1.4.0') <= 0) {

			add_action( 'admin_notices', array( &$this, 'woocomver_nag' ) );

		}

	}


	function curl_nag() { 

		echo '<div style="background-color:#cf0000;color:#fff;font-weight:bold;font-size:16px;margin: -1px 15px 0 5px;padding:5px 10px">';
		_e( 'Your server does not support CURL so the UPS Shipping Module will not work for you.<br/>Ask your hosting company to install CURL for PHP', 'woocommerce' );
		echo '</div>';

	}


	function simplexml_nag() { 

		echo '<div style="background-color:#cf0000;color:#fff;font-weight:bold;font-size:16px;margin: -1px 15px 0 5px;padding:5px 10px">';
		_e( 'Your server does not support SimpleXMLElement so the UPS Shipping Module will not work for you.<br/>Ask your hosting company to enable it for PHP.', 'woocommerce' );
		echo '</div>';

	}

	function woocomver_nag() { 
		global $woocommerce;

		echo '<div style="background-color:#cf0000;color:#fff;font-weight:bold;font-size:16px;margin: -1px 15px 0 5px;padding:5px 10px">';
		
		_e( 'The UPS Shipping module requires WooCommerce 1.4.0 or newer to work correctly. You\'re using version', 'woocommerce' );
		echo ' ' . $woocommerce->version; 
		
		echo '</div>';

	}
	
	
	function is_available( $package = null ) {
		global $woocommerce;
		
		if ( 'no' == $this->enabled ) return false;
			
		if ( 'specific' == $this->availability ) {
			
			if ( is_array( $this->countries ) ) 
				if ( !in_array( $woocommerce->customer->get_shipping_country(), $this->countries ) ) 
					return false;
			
		} 

		return apply_filters( 'woocommerce_shipping_' . $this->id . '_is_available', true );

	} 


	function remove_shipping_method() { 
		global $woocommerce;

		if ( is_admin() ) return;

		if ( $woocommerce->session->customer['postcode'] && '' == $woocommerce->session->customer['shipping_postcode'] || $woocommerce->session->customer['country'] ) {

			$unset = false;

			for ( $i = 0; $i < count( $woocommerce->shipping->shipping_methods ); $i++ ) { 

				if ( $woocommerce->shipping->shipping_methods[ $i ]->id = 'ups_drop_shipping_rate' ) 
					$unset = $i;

			}

			if ( $unset !== false ) 
				unset( $woocommerce->shipping->shipping_methods[ $unset ] );

		}

	}


	// Init form fields - modeled on WooCommerce core code
	function init_form_fields() {
		global $woocommerce;

		$classes = array();
		
		$tclasses = get_terms( 'product_shipping_class', array( 'hide_empty' => false ) );
		
		if ( $tclasses && ! is_wp_error( $tclasses ) ) {
			foreach( $tclasses as $c ) 
				$classes[ $c->slug ] = $c->slug;
		}

		$this->form_fields = array(
				'enabled' => array(
							'title' 		=> __( 'Enable/Disable', 'woocommerce' ), 
							'type' 			=> 'checkbox', 
							'label' 		=> __( 'Enable UPS Drop Shipping', 'woocommerce' ), 
							'default' 		=> 'yes'
							), 
				'title' => array(
							'title' 		=> __( 'Method Title', 'woocommerce' ), 
							'type' 			=> 'text', 
							'description' 		=> __( 'This controls the title which the user sees during checkout.', 'woocommerce' ), 
							'default'		=> __( 'UPS', 'woocommerce' )
							),
				'availability' => array(
							    'title' 		=> __( 'Method availability', 'woocommerce' ), 
							    'type' 		=> 'select', 
							    'default' 		=> 'all',
							    'class'		=> 'availability',
							    'options'		=> array(
								    'all' 	=> __('All allowed countries', 'woocommerce'),
								    'specific' 	=> __('Specific Countries', 'woocommerce')
							    )
							),
				'countries' => array(
							'title' 		=> __( 'Specific Countries', 'woocommerce' ), 
							'type' 			=> 'multiselect', 
							'class'			=> 'chosen_select',
							'css'			=> 'width: 450px;',
							'default' 		=> '',
							'options'		=> $woocommerce->countries->countries
							),

				'tax_status' => array(
							    'title' 		=> __( 'Tax Status', 'woocommerce' ), 
							    'type' 		=> 'select', 
							    'description' 	=> '', 
							    'default' 		=> 'taxable',
							    'options'		=> array(
								    'taxable' 	=> __('Taxable', 'woocommerce'),
								    'none' 	=> __('None', 'woocommerce')
							    )
							),

				'fee' => array(
							'title' 		=> __( 'Default Handling Fee', 'woocommerce' ), 
							'type' 			=> 'text', 
							'description'		=> __('Handlng fee - fixed cost or percentage. This amount will be added to the shipping fee. Percent Example: 15% ( adds 15% of the shipping cost to the shipping amount ) &nbsp; &nbsp; Fixed Fee Example: 5 ( adds $5 to the shipping amount )', 'woocommerce'),
							'default'		=> ''
							),

				'license' => array(
							'title' 		=> __( 'UPS Access Key', 'woocommerce' ), 
							'type' 			=> 'text', 
							'description'		=> '',
							'default'		=> ''
							),
				'userid' => array(
							'title' 		=> __( 'UPS User ID', 'woocommerce' ), 
							'type' 			=> 'text', 
							'description'		=> '',
							'default'		=> ''
							),
				'password' => array(
							'title' 		=> __( 'UPS Password', 'woocommerce' ), 
							'type' 			=> 'password', 
							'description'		=> '',
							'default'		=> ''
							),
				'shippernumber' => array(
							'title' 		=> __( 'UPS Account Number', 'woocommerce' ), 
							'type' 			=> 'text', 
							'description'		=> __( 'Optional. However you MUST enter this value if you want to use rates that you have negotiated with UPS', 'woocommerce' ),
							'default'		=> ''
							),

				'negotiated_rates' => array(
							'title' 		=> __( 'Use Negotiated Rates', 'woocommerce' ), 
							'type' 			=> 'checkbox', 
							'description'		=> __( 'DO NOT enable unless you have already negotiated preferred rates with UPS!', 'woocommerce' ),
							),
				'fallback_rate' => array(
							'title' 		=> __( 'Fallback Rate', 'woocommerce' ), 
							'type' 			=> 'text', 
							'description'		=> __( 'Optional: If UPS does not return a rate quote use this amount instead (do not include a currency symbol ). This can happen if your packages are too big or too heavy', 'woocommerce' ),
							'default' 		=> '',
							),
				'dimension_unit' => array(
							    'title' 		=> __( 'Dimensions Unit', 'woocommerce' ), 
							    'type' 		=> 'select', 
							    'default' 		=> 'IN',
							    'options'		=> array(
								    'IN' 	=> __('Inches', 'woocommerce'),
								    'CM' 	=> __('Centimeters', 'woocommerce')
							    )
							),

				'weight_unit' => array(
							    'title' 		=> __( 'Weight Unit', 'woocommerce' ), 
							    'type' 		=> 'select', 
							    'default' 		=> 'LBS',
							    'options'		=> array(
								    'LBS' 	=> __('Pounds', 'woocommerce'),
								    'KGS' 	=> __('Kilograms', 'woocommerce')
							    )
							),

				'pickup_type' => array(
							    'title' 		=> __( 'Pickup Type', 'woocommerce' ), 
							    'type' 		=> 'select', 
							    'description'	=> 'Your typical pickup method.',
							    'default' 		=> '01',
							    'options'		=> array(
								    '01' 	=> __('Daily Pickup', 'woocommerce'),
								    '03' 	=> __('Drop off at Customer Counter', 'woocommerce'),
								    '06'	=> __('One-time Pickup', 'woocommerce')
							    )
							),

				'packaging_type' => array(
							    'title' 		=> __( 'Packaging Type', 'woocommerce' ), 
							    'type' 		=> 'select', 
							    'description'	=> 'Your typical packaging',
							    'default' 		=> '02',
							    'options'		=> array(
								    '02' 	=> __('UPS Packaging', 'woocommerce'),
								    '00' 	=> __('Your Packaging', 'woocommerce'),
							    )
							),
				'exclude_dimensions' => array(
							'title' 		=> __( 'Exclude Dimensions', 'woocommerce' ), 
							'type' 			=> 'checkbox', 
							'label'			=> __( 'Enable', 'woocommerce' ),
							'description'		=> __( 'When enabled no package dimensions are sent to UPS when requesting a rate quote. Instead only the total weight for all items in the cart is sent to UPS for the rate quote.', 'woocommerce' ),
							),
				'use_dimensional_weight' => array(
							'title' 		=> __( 'Use Dimensional Weight', 'woocommerce' ), 
							'type' 			=> 'checkbox', 
							'label'			=> __( 'Enable', 'woocommerce' ),
							'description'		=> __( 'When enabled YOU MUST NOT TURN ON THE EXCLUDE DIMENSIONS SETTING, and all of your products must have dimensions and weights set.', 'woocommerce' ),
							),
				'shipping_style' => array(
							'title' 		=> __( 'Shipping Style', 'woocommerce' ), 
							'type' 			=> 'select', 
							'description'		=> __( 'Ship Individually - each item in the cart is considered a separate package when asking UPS for rates. <br/><br/>Ship Together - all items in the cart are considered one package when asking UPS for rates, when the origin zip codes match<br/><br/>Multi Select - all items in the cart are considered individual packages when asking UPS for rates, and allow the shopper to pick a shipping method for each individual item ', 'woocommerce' ),
							'options' => array( 
								'individually' => __( 'Ship Individually', 'woocommerce' ),
								'together' => __( 'Ship Together', 'woocommerce' ),
								 'multi_package' => __( 'Multi Select', 'woocommerce' ),
								 )
							),
				'include_value' => array(
							'title' 		=> __( 'Include Declared Value', 'woocommerce' ), 
							'type' 			=> 'checkbox', 
							'description'		=> __( 'When enabled, the total value of the order is sent to UPS as the "Declared Value" of the shipment. Note that when declaring a package value shipping cost estimates will increase.', 'woocommerce' ),
							),
				'destination_type' => array(
							'title' 		=> __( 'Typical Destination Type', 'woocommerce' ), 
							'type' 			=> 'select', 
							'description'		=> ' Rates for residential shipments are usually higher than commercial destinations. Set this to reflect the destintation type for the majority of your shipments to improve the accuracy of shipping cost estimates.',
							'default' 		=> 'res',
							'options'		=> array(
									'res' 	=> __('Residential', 'woocommerce'),
									'com' 	=> __('Commercial', 'woocommerce'),
								)
							),
				'exclude_classes' => array(
							'title' 		=> __( 'Exclude classes', 'woocommerce' ), 
							'type' 			=> 'multiselect', 
							'description'		=> ' Products with matching classes will not be included in UPS shipping calculations.',
							'default' 		=> '',
							'class'			=> 'chosen_select',
							'options'		=> $classes
								
							),
				'dev_server' => array(
							    'title' 		=> __( 'Use UPS test server', 'woocommerce' ), 
							    'type' 		=> 'checkbox', 
							    'description'	=> 'Enable this for testing. DO NOT FORGET to disable this when your store goes live!',
							),
				
				'debug' => array(
							    'title' 		=> __( 'Show debug info on checkout page', 'woocommerce' ),
							    'type' 		=> 'checkbox',
							    'description'	=> 'Enable this for testing only if you have problems obtaining rate requests. DO NOT FORGET to disable this when your store goes live!',
							),

				/*
				'use_cache' => array(
							    'title' 		=> __( 'Use caching', 'woocommerce' ), 
							    'type' 		=> 'checkbox', 
							    'description'	=> 'Enable this to cache shipping cost lookups for customer carts. Improves cart performance.',
							),
				*/
		);

	}

	/*
	function after_order_notes( $checkout = null ) { 
		?>

		<p id="order_comments_field" class="form-row notes">
			<input type="checkbox" value="1" name="shiptoresidential" checked="checked" class="input-checkbox" id="shiptoresidential">
			<label class="" for="shiptoresidential" style="display:inline"> Shipping address is residential</label>

		</p>

		<?php 
	}
	*/

/**
TODO: Add UPS label printing via Shipping API
TODO: Add UPS tracking interface
*/

	function freight_test() {
		global $woocommerce;


		if ( $this->debug ) { 
			
			//$this->debug_messages[] = 'UPS Endpoint URL: '. $url;
			
			//$this->debug_messages[] = '<p>' . $url . '</p><p>REQUEST XML: </p><pre> ' . htmlspecialchars( $data ) . '</pre><p></p>'; 
			
		}
		
		$test_transaction = array();
		$test_transaction['name'] = 'Consignee Test 1';
		$test_transaction['address'] = '1000 Consignee Street';
		$test_transaction['city'] = 'Allanton';
		$test_transaction['state'] = 'MO';
		$test_transaction['country'] = 'US';
		$test_transaction['postcode'] = '63025';
		
		$test_transaction['class'] = '92.5';
		$test_transaction['commodity'] = '116030';
		$test_transaction['subcode'] = '1';
		
		$test_transaction['fname'] = 'Developer Test 1';
		$test_transaction['faddress'] = '101 Developer Way';
		$test_transaction['fcity'] = 'Richmond';
		$test_transaction['fstate'] = 'VA';
		$test_transaction['fcountry'] = 'US';
		$test_transaction['fpostcode'] = '23224';

		$items[] = array( 'country' => 'US', 'state_province' => 'MO', 'class' => '92.5', 'length' => '', 'width' => '', 'height' => '', 'weight' => 1500, 'qty' => 1, 'value' => '' );
			
				
		$test = true; // true = sandbox dev, false = live production

		$x = new UPSFreightRatesAndService( $this->userid, $this->password, $this->license, $test );

		$result = $x->getFreightRate( $weight = 1500, $width = '', $length = '', $height = '', '23244', $items, $this->debug, $this, $test_transaction );
		
		// Errors
		if ( is_object( $result ) ) { 
			$msg = array();
			if ( !empty( $result->detail->Errors->ErrorDetail->PrimaryErrorCode->Code ) || !empty($result->detail->Errors->ErrorDetail->PrimaryErrorCode->Description ) ) {

				if ( !empty( $result->detail->Errors->ErrorDetail->PrimaryErrorCode->Code ) )
					$msg[] = $result->detail->Errors->ErrorDetail->PrimaryErrorCode->Code;
					
				if ( !empty( $result->detail->Errors->ErrorDetail->PrimaryErrorCode->Description ) )
					$msg[] = $result->detail->Errors->ErrorDetail->PrimaryErrorCode->Description;
					
				$msg = implode( ': ', $msg );
			} else { 
				$msg[] = 'Unknown';
			}
			
			return array( 'error' => $msg );
		}
		
		try { 
			$cost_data = $this->XMLtoArray( $result );
		} catch( Exception $e ){
			//var_dump($e);exit;
		}

		if ( !$cost_data ) 
			return false;

		$rate = null; 
		
		if ( !empty( $cost_data["SOAPENV:ENVELOPE"]["SOAPENV:BODY"]["FREIGHTRATE:FREIGHTRATERESPONSE"]["FREIGHTRATE:TOTALSHIPMENTCHARGE"]["FREIGHTRATE:MONETARYVALUE"] ) ) { 
		
			if ( $this->debug ) { 
				$this->debug_messages[] = 'UPS LTL COST RESPONSE: ' . $cost_data["SOAPENV:ENVELOPE"]["SOAPENV:BODY"]["FREIGHTRATE:FREIGHTRATERESPONSE"]["FREIGHTRATE:TOTALSHIPMENTCHARGE"]["FREIGHTRATE:MONETARYVALUE"];
			}
		
			$rate = $cost_data["SOAPENV:ENVELOPE"]["SOAPENV:BODY"]["FREIGHTRATE:FREIGHTRATERESPONSE"]["FREIGHTRATE:TOTALSHIPMENTCHARGE"]["FREIGHTRATE:MONETARYVALUE"];
		
		}

		return array( 'rate' => $rate );
		
	}
	
	// Talk to UPS
	function get_rates( $data ) {
		global $woocommerce;

		if ( 'yes' == $this->dev_server ) 
			$url = 'https://wwwcie.ups.com/ups.app/xml/Rate';
		    else 
		// $url = 'https://www.ups.com/ups.app/xml/Rate';
		$url = 'https://onlinetools.ups.com/ups.app/xml/Rate';
			
		if ( $this->debug ) { 
			
			$this->debug_messages[] = 'UPS Endpoint URL: '. $url;
			
			$this->debug_messages[] = '<p>' . $url . '</p><p>REQUEST XML: </p><pre> ' . htmlspecialchars( $data ) . '</pre><p></p>'; 
			
		}

		$ch = curl_init( $url ); 

		curl_setopt( $ch, CURLOPT_HEADER, 1 );
		curl_setopt( $ch, CURLOPT_POST,1 );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 60 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 ); 
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );

		$result = curl_exec ($ch);  


		$pos = strpos( $result, '<?' );
		
		if ( false !== $pos ) 
			$data = trim( substr( $result, $pos ) );
		else 
			$data = null;
			
			//$data = strstr( $result, '<?' );  
/*
echo '<pre>';
print_r( htmlentities( $data ) );
//echo "\n\n<p>RESULT<p>\n\n";
//print_r( htmlentities( $result ) );
echo '</pre><p>===================</p>';
*/
		
		curl_close($ch);  

		libxml_use_internal_errors(true);

		try { 
			$cost_data = new SimpleXMLElement( $data );
		} catch( Exception $e ){
			//var_dump($e);exit;
		}

		if ( !$cost_data ) 
		return false;

		if ( $this->debug ) { 
			$cost_data->formatOutput = true;
			$cost_data->preserveWhiteSpace = false;
			$this->debug_messages[] = '<p>UPS RESULT XML:</p> <pre>' . htmlspecialchars( $cost_data->saveXML() ) . '</pre><p></p>'; //json_encode( print_r( $result, true ) );
		}
			
		$rates = array();

		if ( isset( $cost_data->RatedShipment ) ) 
		foreach ( $cost_data->RatedShipment as $rs ) { 

			$nrate = '';

			if ( isset( $rs->NegotiatedRates->NetSummaryCharges->GrandTotal->MonetaryValue ) )
				$nrate = (string)$rs->NegotiatedRates->NetSummaryCharges->GrandTotal->MonetaryValue;

			if ( '' != $nrate ) 
				$rates [ (string)$rs->Service->Code ] = $nrate; 
			else 
				$rates[ (string)$rs->Service->Code ] = (string)$rs->TotalCharges->MonetaryValue; 

		}

		return $rates; 

	} 


	// Main calc routine - calculates estimated package size and weight, gets rates for each enabled shipping type, caches results
	function calculate_shipping( $package = array() ) {
		global $woocommerce;

		$services = get_option( 'woocommerce_ups_rate_types', false );

		if ( !$services ) 
			return;
		
		if ( sizeof( $woocommerce->cart->get_cart() ) <= 0 ) {
			$this->id = '';
			$this->title = '';
			$this->shipping_total = '';
			$this->shipping_tax = '';
			return false; 
		}

		$this->shipping_total = 0;
		
		$this->shipping_tax = 0;
		
		$items = array();

		foreach ( $package['contents'] as $index => $values ) {

						
			$class = $values['data']->get_shipping_class();

			if ( !empty( $class ) && !empty( $this->settings['exclude_classes'] ) && in_array( $class, $this->settings['exclude_classes'] ) )
				continue;

			if ( isset( $values['data']->virtual ) && 'no' != $values['data']->virtual )
			    continue;

			if ( '' == trim( $values['data']->weight ) && '' == trim( $values['data']->length ) && '' == trim( $values['data']->height ) && '' == trim( $values['data']->width ) )
			    continue;


			if ( '' == trim( $values['data']->length ) )
				$length = 1;
			else
				$length = floatval( $values['data']->length );


			if ( '' == trim( $values['data']->width ) )
				$width = 1;
			else
				$width = floatval( $values['data']->width );


			if ( '' == trim( $values['data']->height ) )
				$height = 1;
			else
				$height = floatval( $values['data']->height );


			if ( '' == trim( $values['data']->weight ) )
				$weight = 1;
			else
				$weight = floatval( $values['data']->weight );


			if ( !empty( $this->use_dimensional_weight ) && 'yes' == $this->use_dimensional_weight ) { 

				$t = round( ( round( $height, 0 ) * round( $length, 0 ) * round( $width, 0 ) ), 0 );

				if ( 'in' == $this->dimensions_unit ) {

					$t = $t / 139;
					
					if ( $t > $weight ) 
						$weight = round( $t, 0 ); 
				
				} else if ( 'cm' == $this->dimensions_unit ) { 

					$t = $t / 353.06; 
					
					if ( $t > $weight ) 
						$weight = round( $t, 0 ); 

				}

			} 
			
			if ( '' == trim( $values['quantity'] ) )
				$quantity = 1;
			else
				$quantity = intval( $values['quantity'] );
				
			if ( !empty( $values['data']->price ) )
				$value = round( ( $quantity * $values['data']->price ), 2 );
			else
				$value = 0;

			$origin = get_post_meta( $values['product_id'], 'ups_drop_ship_origin', true );
			$state_province = get_post_meta( $values['product_id'], 'country_state', true );

			if ( empty( $origin ) )
				$origin = $this->originzip;
			
			if ( empty( $state_province ) ) { 
			
				$country = $this->origincountry;
				
				$state_province = $this->originstate;
			
			} else { 
			
				// Country & state? 
				if ( false !== strpos( $state_province, ':' ) ) {
				
					$parts = explode( ':', $state_province );
					
					$country = $parts[0];
					
					$state_province = $parts[1]; 
				
				// Must only be a country in the $state_province var, so use that for country and empty the state_province var
				} else { 
				
					$country = $state_province; 
					
					$state_province = null;
					
				
				}
				
			}


			$items[ $origin ][] = array( 'country' => $country, 'state_province' => $state_province, 'class' => $class, 'length' => $length, 'width' => $width, 'height' => $height, 'weight' => $weight, 'qty' => $quantity, 'value' => $value );

		}


		if ( count( $items ) <= 0 ) {
			$this->id = '';
			$this->title = '';
			$this->shipping_total = '';
			$this->shipping_tax = '';
			return false; 
		}

		$packages = '';

		if ( 'individually' == $this->shipping_style || 'multi_package' == $this->shipping_style )
			$packages = $this->individual_shipping( $items );
		else
			$packages = $this->combined_shipping( $items );

		if ( !empty( $this->temp_rates ) ) { 
		
			foreach( $this->temp_rates as $code => $rate ) { 

				if ( '' == $code || floatval( $rate ) <= 0 )
					continue; 

				foreach( $services as $name => $service_code ) { 

					if ( $code != $service_code )
						continue;

					$next_rate = new stdClass(); 

					$next_rate->id = 'ups_drop_shipping_rate_UPS ' . $name;

					//$next_rate->title = $name;
					$next_rate->label = $this->title . ' ' . $name;

					// $next_rate->shipping_total = floatval( $the_rate[0] ) ; 
					$next_rate->cost = floatval( $rate ) ; 

					// handling fee, if any
					if ( $this->fee ) { 
					
						$percent = strpos( $this->fee, '%' );
						
						if ( false !== $percent ) {
						
							$fee = str_replace( '%', '', $this->fee );
							
							$fee = ( $fee / 100 );
							
							$next_rate->cost = ( $fee * $next_rate->cost ) + $next_rate->cost;
						
						} else { 
						
							$next_rate->cost += $this->fee;
							
						}

					}

					$this->ups_drop_shipping_rate[] = $next_rate;

					$this->add_rate( $next_rate );
		
					$this->multiple_rates = true;
		
				}
			}
				
		} else if ( !empty( $this->fallback_rate ) ) { 
		
			$default_rate = new stdClass(); 
			
			$default_rate->id = 'ups_drop_shipping_rate_UPS Shipping';
			
			$default_rate->label = 'UPS Fallback Rate';
			
			$default_rate->cost = $this->fallback_rate;
			
			$this->ups_drop_shipping_rate[] = $default_rate;
			
			$this->add_rate( $default_rate );
			
			$this->multiple_rates = false;
		}
	
//var_dump( $this->temp_rates ); 
	

//die( 'done' );
		/** 
		Combine the rates based on shipping method
		*/
	} 

	
	function ups_request( $packages, $the_rates = array() ) { 

		$this->rates = array();

		$this->ups_drop_shipping_rate = array();

		$the_rates = $this->get_rates( $packages );
//var_dump( $packages );
//echo '<p>';
//var_dump( $the_rates );  

		if ( !empty( $the_rates ) ) { 
		
			foreach( $the_rates as $key => $rate ) { 
			
				if ( empty( $this->temp_rates[ $key ] ) )
					$this->temp_rates[ $key ] = $rate;
				else
					$this->temp_rates[ $key ] += $rate;
			
			}
			
		} 
//var_dump( $this->temp_rates ); 
//echo '<p>----------</p>';
		return;
	
	}
	
	function get_xml_header( $origin, $country = null, $sp = null, $surepost = false ) { 
		global $woocommerce;
		
		if ( method_exists( $woocommerce->customer, 'get_shipping_postcode' ) )
			$dest_zip = $woocommerce->customer->get_shipping_postcode();
		else if ( isset( $_SESSION['customer']['shipping_postcode'] ) )
			$dest_zip = $_SESSION['customer']['shipping_postcode'];

		/*
		if ( !$dest_zip ) 
			$dest_zip = $woocommerce->session->customer['postcode'];
		*/
		
		if ( method_exists( $woocommerce->customer, 'get_shipping_state' ) )
			$dest_state = $woocommerce->customer->get_shipping_state();
		else if ( isset( $_SESSION['customer']['shipping_state'] ) )
			$dest_state = $_SESSION['customer']['shipping_state'];


		if ( method_exists( $woocommerce->customer, 'get_shipping_country' ) )
			$dest_country = $woocommerce->customer->get_shipping_country();
		else if ( isset( $_SESSION['customer']['shipping_country'] ) )
			$dest_country = $_SESSION['customer']['shipping_country'];

		if ( $surepost ) { 
		
			$data ="<?xml version=\"1.0\"?>  
			<AccessRequest xml:lang=\"en-US\">  
				<AccessLicenseNumber>{$this->license}</AccessLicenseNumber>  
				<UserId>{$this->userid}</UserId>  
				<Password>{$this->password}</Password>  
			</AccessRequest>  
			<RatingServiceSelectionRequest xml:lang=\"en-US\">  
				<Request>  
					<TransactionReference>  
						<CustomerContext>Bare Bones Rate Request</CustomerContext>  
						<XpciVersion>1.0001</XpciVersion>  
					</TransactionReference>  
					<RequestAction>Rate</RequestAction>  
					<RequestOption>Rate</RequestOption>  
				</Request>  
				<PickupType>  
				<Code>{$this->pickup_type}</Code>  
				</PickupType>  
				<Shipment>  
					<Shipper>  
						<Address>  
							<PostalCode>{$origin}</PostalCode>  
							<CountryCode>{$this->origincountry}</CountryCode>  
						</Address>  
						<ShipperNumber>{$this->shippernumber}</ShipperNumber>  
					</Shipper>  
					<ShipTo>  
						<Address>  
							<StateProvinceCode>$dest_state</StateProvinceCode>
							<PostalCode>$dest_zip</PostalCode>  
							<CountryCode>$dest_country</CountryCode>";

					if ( 'res' == $this->destination_type ) { 
			$data .= "
							<ResidentialAddressIndicator>1</ResidentialAddressIndicator>
			";
					} 

			$data .= "		</Address>  
					</ShipTo>  
					<ShipFrom>  
						<Address>  
						<PostalCode>{$origin}</PostalCode>
				";

			if ( !empty( $sp ) ) {
				$data .= "		<StateProvinceCode>{$sp}</StateProvinceCode>
				";
			} else if ( empty( $sp ) && !empty( $country ) ) { // no state, but there is a country? don't write the state.
				//$data .= "		<StateProvinceCode>{$this->originstate}</StateProvinceCode>
				//";
			} else if ( empty( $sp ) && !empty( $this->originstate ) ) {
				$data .= "		<StateProvinceCode>{$this->originstate}</StateProvinceCode>
				";
			}
			
			if ( !empty( $country ) )
				$data .= "		<CountryCode>{$country}</CountryCode>
						";
			else 
				$data .= "		<CountryCode>{$this->origincountry}</CountryCode>
						";
			
			$data .= "
						</Address>  
					</ShipFrom>
					<Service>
						<Code>93</Code>
					</Service>
					";
					
		} else { 
		
			$data ="<?xml version=\"1.0\"?>  
			<AccessRequest xml:lang=\"en-US\">  
				<AccessLicenseNumber>{$this->license}</AccessLicenseNumber>  
				<UserId>{$this->userid}</UserId>  
				<Password>{$this->password}</Password>  
			</AccessRequest>  
			<RatingServiceSelectionRequest xml:lang=\"en-US\">  
				<Request>  
					<TransactionReference>  
						<CustomerContext>Bare Bones Rate Request</CustomerContext>  
						<XpciVersion>1.0001</XpciVersion>  
					</TransactionReference>  
					<RequestAction>Rate</RequestAction>  
					<RequestOption>Shop</RequestOption>  
				</Request>  
				<PickupType>  
				<Code>{$this->pickup_type}</Code>  
				</PickupType>  
				<Shipment>  
					<Shipper>  
						<Address>  
							<PostalCode>{$origin}</PostalCode>  
							<CountryCode>{$this->origincountry}</CountryCode>  
						</Address>  
						<ShipperNumber>{$this->shippernumber}</ShipperNumber>  
					</Shipper>  
					<ShipTo>  
						<Address>  
							<StateProvinceCode>$dest_state</StateProvinceCode>
							<PostalCode>$dest_zip</PostalCode>  
							<CountryCode>$dest_country</CountryCode>";

					if ( 'res' == $this->destination_type ) { 
			$data .= "
							<ResidentialAddressIndicator>1</ResidentialAddressIndicator>
			";
					} 

			$data .= "		</Address>  
					</ShipTo>  
					<ShipFrom>  
						<Address>  
						<PostalCode>{$origin}</PostalCode>
				";

			if ( !empty( $sp ) ) {
				$data .= "		<StateProvinceCode>{$sp}</StateProvinceCode>
				";
			} else if ( empty( $sp ) && !empty( $country ) ) { // no state, but there is a country? don't write the state.
				//$data .= "		<StateProvinceCode>{$this->originstate}</StateProvinceCode>
				//";
			} else if ( empty( $sp ) && !empty( $this->originstate ) ) {
				$data .= "		<StateProvinceCode>{$this->originstate}</StateProvinceCode>
				";
			}
			
			if ( !empty( $country ) )
				$data .= "		<CountryCode>{$country}</CountryCode>
						";
			else 
				$data .= "		<CountryCode>{$this->origincountry}</CountryCode>
						";
			
			$data .= "
						</Address>  
					</ShipFrom>  
					";
		}
		
		return $data;
	
	}
		
	function XMLtoArray( $XML ) {
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $XML, $vals);
		xml_parser_free($xml_parser);
		// wyznaczamy tablice z powtarzajacymi sie tagami na tym samym poziomie
		$_tmp='';
		foreach ($vals as $xml_elem) {
			$x_tag=$xml_elem['tag'];
			$x_level=$xml_elem['level'];
			$x_type=$xml_elem['type'];
			if ($x_level!=1 && $x_type == 'close') {
				if (isset($multi_key[$x_tag][$x_level]))
					$multi_key[$x_tag][$x_level]=1;
				else
					$multi_key[$x_tag][$x_level]=0;
			}
			if ($x_level!=1 && $x_type == 'complete') {
				if ($_tmp==$x_tag)
					$multi_key[$x_tag][$x_level]=1;
				$_tmp=$x_tag;
			}
		}
		// jedziemy po tablicy
		foreach ($vals as $xml_elem) {
			$x_tag=$xml_elem['tag'];
			$x_level=$xml_elem['level'];
			$x_type=$xml_elem['type'];
			if ($x_type == 'open')
				$level[$x_level] = $x_tag;
			$start_level = 1;
			$php_stmt = '$xml_array';
			if ($x_type=='close' && $x_level!=1)
				$multi_key[$x_tag][$x_level]++;
			while ($start_level < $x_level) {
				$php_stmt .= '[$level['.$start_level.']]';
				if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
					$php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
				$start_level++;
			}
			$add='';
			if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete')) {
				if (!isset($multi_key2[$x_tag][$x_level]))
					$multi_key2[$x_tag][$x_level]=0;
				else
					$multi_key2[$x_tag][$x_level]++;
				$add='['.$multi_key2[$x_tag][$x_level].']';
			}
			if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes', $xml_elem)) {
				if ($x_type == 'open')
					$php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
				else
					$php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
				eval($php_stmt_main);
			}
			if (array_key_exists('attributes', $xml_elem)) {
				if (isset($xml_elem['value'])) {
					$php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
					eval($php_stmt_main);
				}
				foreach ($xml_elem['attributes'] as $key=>$value) {
					$php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
					eval($php_stmt_att);
				}
			}
		}
		return $xml_array;
	}

	function do_freight_request( $weight, $width = 0, $length = 0, $height = 0, $origin = '', $items = array() ) { 
	
		// Call the freight API, then put rate into the temp_rates var 
		// $this->temp_rates[ $key ] = $rate;
		
		if ( 'yes' == $this->dev_server )
			$test = true;
		else 
			$test = false;

		$x = new UPSFreightRatesAndService( $this->userid, $this->password, $this->license, $test );

		$r = $x->getFreightRate( $weight, $width, $length, $height, $origin, $items, $this->debug, $this );

		if ( $this->debug && !empty( $r->faultstring ) && !empty( $r->faultcode ) && !empty( $r->detail ) ) { 

			$this->debug_messages[] = 'UPS ERROR: ' . $r->detail->Errors->ErrorDetail->Severity . ': ' . $r->detail->Errors->ErrorDetail->PrimaryErrorCode->Description;
			return;
		}
		
		//libxml_use_internal_errors(true);

		$pos = strpos( $r, '<?' );
		
		if ( false !== $pos ) 
			$data = trim( substr( $r, $pos ) );
		else 
			$data = $r;

		try { 
			$cost_data = $this->XMLtoArray( $data );
		} catch( Exception $e ){
			//var_dump( $e );exit;
		}
		
		if ( !empty( $cost_data["SOAPENV:ENVELOPE"]["SOAPENV:BODY"]["FREIGHTRATE:FREIGHTRATERESPONSE"]["FREIGHTRATE:TOTALSHIPMENTCHARGE"]["FREIGHTRATE:MONETARYVALUE"] ) ) { 
		
			if ( $this->debug ) { 
				$this->debug_messages[] = 'UPS LTL COST RESPONSE: ' . $cost_data["SOAPENV:ENVELOPE"]["SOAPENV:BODY"]["FREIGHTRATE:FREIGHTRATERESPONSE"]["FREIGHTRATE:TOTALSHIPMENTCHARGE"]["FREIGHTRATE:MONETARYVALUE"];
				
				$this->debug_messages[] = '<pre><strong>ENTIRE RESPONSE</strong><br>' . print_r( $cost_data, true ) . '</pre>';
			}
		
			$rate = $cost_data["SOAPENV:ENVELOPE"]["SOAPENV:BODY"]["FREIGHTRATE:FREIGHTRATERESPONSE"]["FREIGHTRATE:TOTALSHIPMENTCHARGE"]["FREIGHTRATE:MONETARYVALUE"];
			
			if ( isset( $this->temp_rates[308] ) )
				$this->temp_rates[308] = floatval( $this->temp_rates[308] ) + $rate; 
			else 
				$this->temp_rates[308] = $rate; 
		
		}
		
		
/* response sample:
<soapenv:envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <soapenv:header>
        <soapenv:body>
            <freightrate:freightrateresponse xmlns:freightrate="http://www.ups.com/XMLSchema/XOLTWS/FreightRate/v1.0">
                <common:response xmlns:common="http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0">
                    <common:responsestatus>
                        <common:code>1</common:code>
                        <common:description>Success</common:description>
                    </common:responsestatus>
                    <common:alert>
                        <common:code>9369055</common:code>
                        <common:description>User is not eligible for contract rates.</common:description>
                    </common:alert>
                </common:response>
                <freightrate:rate>
                    <freightrate:type>
                        <freightrate:code>DSCNT</freightrate:code>
                        <freightrate:description>DSCNT</freightrate:description>
                    </freightrate:type>
                    <freightrate:factor>
                        <freightrate:value>292.09</freightrate:value>
                        <freightrate:unitofmeasurement>
                            <freightrate:code>USD</freightrate:code>
                        </freightrate:unitofmeasurement>
                    </freightrate:factor>
                </freightrate:rate>
                <freightrate:rate>
                    <freightrate:type>
                        <freightrate:code>DSCNT_RATE</freightrate:code>
                        <freightrate:description>DSCNT_RATE</freightrate:description>
                    </freightrate:type>
                    <freightrate:factor>
                        <freightrate:value>60.00</freightrate:value>
                        <freightrate:unitofmeasurement>
                            <freightrate:code>%</freightrate:code>
                        </freightrate:unitofmeasurement>
                    </freightrate:factor>
                </freightrate:rate>
                <freightrate:rate>
                    <freightrate:type>
                        <freightrate:code>2</freightrate:code>
                        <freightrate:description>2</freightrate:description>
                    </freightrate:type>
                    <freightrate:factor>
                        <freightrate:value>44.59</freightrate:value>
                        <freightrate:unitofmeasurement>
                            <freightrate:code>USD</freightrate:code>
                        </freightrate:unitofmeasurement>
                    </freightrate:factor>
                </freightrate:rate>
                <freightrate:rate>
                    <freightrate:type>
                        <freightrate:code>LND_GROSS</freightrate:code>
                        <freightrate:description>LND_GROSS</freightrate:description>
                    </freightrate:type>
                    <freightrate:factor>
                        <freightrate:value>486.81</freightrate:value>
                        <freightrate:unitofmeasurement>
                            <freightrate:code>USD</freightrate:code>
                        </freightrate:unitofmeasurement>
                    </freightrate:factor>
                </freightrate:rate>
                <freightrate:rate>
                    <freightrate:type>
                        <freightrate:code>AFTR_DSCNT</freightrate:code>
                        <freightrate:description>AFTR_DSCNT</freightrate:description>
                    </freightrate:type>
                    <freightrate:factor>
                        <freightrate:value>194.72</freightrate:value>
                        <freightrate:unitofmeasurement>
                            <freightrate:code>USD</freightrate:code>
                        </freightrate:unitofmeasurement>
                    </freightrate:factor>
                </freightrate:rate>
                <freightrate:commodity>
                    <freightrate:description>No Description</freightrate:description>
                    <freightrate:weight>
                        <freightrate:value>170</freightrate:value>
                        <freightrate:unitofmeasurement>
                            <freightrate:code>LBS</freightrate:code>
                        </freightrate:unitofmeasurement>
                    </freightrate:weight>
                    <freightrate:adjustedweight>
                        <freightrate:value>170</freightrate:value>
                        <freightrate:unitofmeasurement>
                            <freightrate:code>LBS</freightrate:code>
                        </freightrate:unitofmeasurement>
                    </freightrate:adjustedweight>
                </freightrate:commodity>
                <freightrate:totalshipmentcharge>
                    <freightrate:currencycode>USD</freightrate:currencycode>
                    <freightrate:monetaryvalue>239.31</freightrate:monetaryvalue>
                </freightrate:totalshipmentcharge>
                <freightrate:billableshipmentweight>
                    <freightrate:value>170</freightrate:value>
                    <freightrate:unitofmeasurement>
                        <freightrate:code>LBS</freightrate:code>
                    </freightrate:unitofmeasurement>
                </freightrate:billableshipmentweight>
                <freightrate:dimensionalweight>
                    <freightrate:value>0</freightrate:value>
                    <freightrate:unitofmeasurement>
                        <freightrate:code>LBS</freightrate:code>
                    </freightrate:unitofmeasurement>
                </freightrate:dimensionalweight>
                <freightrate:service>
                    <freightrate:code>308</freightrate:code>
                </freightrate:service>
                <freightrate:guaranteedindicator>
                    <freightrate:minimumchargeappliedindicator>
                        <freightrate:ratingschedule>
                            <freightrate:code>02</freightrate:code>
                            <freightrate:description>Published Rates</freightrate:description>
                        </freightrate:ratingschedule>
                    </freightrate:minimumchargeappliedindicator>
                </freightrate:guaranteedindicator>
            </freightrate:freightrateresponse>
        </soapenv:body>
    </soapenv:header>
</soapenv:envelope>"
*/
	}
	
	/**
	Do a request for each origin zip code with items listed as individual packages
	*/
	function individual_shipping( $item_list ) { 

		$zip = null;
		
		$packs = array();

		$services = get_option( 'woocommerce_ups_rate_types', false );
		
		$surepost = false; 
		
		if ( !empty( $services ) )
		foreach( $services as $name => $service_code ) {
			if ( '93' == $service_code ) {
				$surepost = true; 
				break;
			}		
		}
		
		$freight = false;
		
		foreach( $item_list as $origin => $items ) { 
		
			$packages = '';
			
			$header = $this->get_xml_header( $origin, $items[0]['country'], $items[0]['state_province'] );
			
			foreach( $items as $item ) { 
			
				$item['weight'] = ceil( $item['weight'] );
			
				if ( 'g' == get_option( 'woocommerce_weight_unit' ) )
					$item['weight'] = round( 1000 / $item['weight'], 2 );
				else if ( 'oz' == get_option( 'woocommerce_weight_unit' ) )
					$item['weight'] = round( $item['weight'] / 16, 2 );
				
				// If the item is over 150LBS do a freight request and return
				if ( 'oz' == get_option( 'woocommerce_weight_unit' ) && $item['weight'] > 2400 ) { 
				
					// Convert to LBS
					$total_weight = ceil( $item['weight'] / 16 );
					
					$this->do_freight_request( $total_weight, $item['width'], $item['length'], $item['height'], $origin, $items );
					
					$freight = true; 
					continue;
		
				} else if ( 'lbs' == get_option( 'woocommerce_weight_unit' ) && $item['weight'] > 150 ) { 
				
					$total_weight = ceil( $item['weight'] ); // round up

					$this->do_freight_request( $total_weight, $item['width'], $item['length'], $item['height'], $origin, $items );
					
					$freight = true; 
					continue;
					
				} else if ( 'g' == get_option( 'woocommerce_weight_unit' ) && $item['weight'] > 68038.9 ) { 
				
					// Convert to KGS? Grams are not support, only LBS and KGS
					$total_weight = ceil( $item['weight'] / 0.001 );
					
					$this->do_freight_request( $total_weight, $item['width'], $item['length'], $item['height'], $origin, $items );
					
					$freight = true; 
					continue;
					
				} 
				

				for ( $i = 0; $i < $item['qty']; $i++ ) {
				$packages .= "
				<Package>  
					<PackagingType>  
						<Code>{$this->packaging_type}</Code>  
					</PackagingType>  
					";
					
					if ( 'yes' != $this->exclude_dimensions ) {
					$packages .= "
					<Dimensions>  
						<UnitOfMeasurement>  
								<Code>{$this->dimension_unit}</Code>  
						</UnitOfMeasurement>  
						<Length>{$item['length']}</Length>  
						<Width>{$item['width']}</Width>  
						<Height>{$item['height']}</Height>  
					</Dimensions>  
					";
					}
					
					$packages .= "
					<PackageWeight>  
						<UnitOfMeasurement>  
								<Code>{$this->weight_unit}</Code>  
						</UnitOfMeasurement>  
						<Weight>{$item['weight']}</Weight>  
					</PackageWeight>";

					// Cannot ask for insurance when using SurePost
					if ( 'yes' == $this->include_value && !$surepost ) { 
						
						$packages .= "
							<PackageServiceOptions>
							<InsuredValue>
								<CurrencyCode>" . get_option('woocommerce_currency') . "</CurrencyCode>
								<MonetaryValue>" . $item['value'] . "</MonetaryValue>
							</InsuredValue>
							</PackageServiceOptions> 
						";
					}

				$packages .= "\n
				</Package>";
			
				}
			}

			if ( $freight ) 
				continue; 
				
			if ( 'yes' == $this->negotiated_rates )
			$packages .= "		<RateInformation>
							<NegotiatedRatesIndicator/>
						</RateInformation>";

			$packages .= "
					</Shipment>  
					</RatingServiceSelectionRequest>";  
			
			
			$packs[] = $header . $packages;
			
			if ( $surepost ) { 
				$header = $this->get_xml_header( $origin, $items[0]['country'], $items[0]['state_province'], true );
				$packs[] = $header . $packages;
			}
		
		}

		if ( $freight ) 
			return;
				
		foreach( $packs as $pack ) { 		
			$this->ups_request( $pack );
		}
		
		return;
	}
	

	function combined_shipping( $packages ) {

		$packs = array();
		
		$services = get_option( 'woocommerce_ups_rate_types', false );
		
		$surepost = false; 
		
		if ( !empty( $services ) )
		foreach( $services as $name => $service_code ) {
			if ( '93' == $service_code ) {
				$surepost = true; 
				break;
			}		
		}

		if ( $this->debug ) 
			$this->debug_messages[] = '<pre><strong>Packages:</strong><br/>' . print_r( $packages, true ) . '</pre>';
			
		foreach( $packages as $origin => $items ) {

			// guesstimate the box size required if all items are stacked on top of each other vertically
			$total_height = 0;
			$total_width = 0;
			$total_length = 0;
			$total_weight = 0;
			$total_value = 0;
			
			$package = '';
			
			$header = $this->get_xml_header( $origin, $items[0]['country'], $items[0]['state_province'] );
		
			foreach( $items as $i ) { 
			
				$total_height += $i['height'];
			
				for( $x = 1; $x <= $i['qty']; $x++ )
					$total_weight += $i['weight'];

				if ( $i['width'] > $total_width ) 
					$total_width = $i['width'];

				if ( $i['length'] > $total_length ) 
					$total_length = $i['length'];
					
				$total_value += $i['value'];

				if ( ( 'yes' != $this->exclude_dimensions && ( $total_height <= 0 || $total_width <= 0 || $total_length <= 0 ) ) || $total_weight <= 0 ) {
					$this->id = '';
					$this->title = '';
					$this->shipping_total = '';
					$this->shipping_tax = '';
					continue;
				}
			}
				
			if ( 'g' == get_option( 'woocommerce_weight_unit' ) )
				$total_weight = round( 1000 / $total_weight, 2 );
			else if ( 'oz' == get_option( 'woocommerce_weight_unit' ) )
				$total_weight = round( $total_weight / 16, 2 );
				
			// If the package is over 150 then it must go freight, so do a freight call and return;
			if ( 'oz' == get_option( 'woocommerce_weight_unit' ) && $total_weight > 2400 ) { 
			
				// Convert to LBS
				$total_weight = ceil( $total_weight / 16 );
				
				$this->do_freight_request( $total_weight, $total_width, $total_length, $total_height, $origin, $items );
				
				continue;
	
			} else if ( 'lbs' == get_option( 'woocommerce_weight_unit' ) && $total_weight > 150 ) { 
			
				$total_weight = ceil( $total_weight ); // round up
			
				$this->do_freight_request( $total_weight, $total_width, $total_length, $total_height, $origin, $items );
				
				continue;
				
			} else if ( 'g' == get_option( 'woocommerce_weight_unit' ) && $total_weight > 68038.9 ) { 
			
				// Convert to KGS? Grams are not support, only LBS and KGS
				$total_weight = ceil( $total_weight / 0.001 );
				
				$this->do_freight_request( $total_weight, $total_width, $total_length, $total_height, $origin, $items );
				
				continue;
				
			}
			
			$total_weight = ceil( $total_weight );

			$package .= "
				<Package>  
				<PackagingType>  
					<Code>{$this->packaging_type}</Code>  
				</PackagingType>  
				";
				
			if ( 'yes' != $this->exclude_dimensions ) { 
				$package .= "
				<Dimensions>  
					<UnitOfMeasurement>  
					<Code>{$this->dimension_unit}</Code>  
					</UnitOfMeasurement>  
					<Length>{$total_length}</Length>  
					<Width>{$total_width}</Width>  
					<Height>{$total_height}</Height>  
				</Dimensions> 
				";
			}
				
				$package .= " 
				<PackageWeight>  
					<UnitOfMeasurement>  
					<Code>{$this->weight_unit}</Code>  
					</UnitOfMeasurement>  
					<Weight>{$total_weight}</Weight>  
				</PackageWeight>";

			// Cannot ask for insurance when using SurePost
			if ( 'yes' == $this->include_value && !$surepost ) { 
				
				$package .= "
					<PackageServiceOptions>
					<InsuredValue>
						<CurrencyCode>" . get_option('woocommerce_currency') . "</CurrencyCode>
						<MonetaryValue>{$total_value}</MonetaryValue>
					</InsuredValue>
					</PackageServiceOptions> 
				";
			}

			$package .= "\n</Package>";

			if ( 'yes' == $this->negotiated_rates )
			$package .= "		<RateInformation>
							<NegotiatedRatesIndicator/>
						</RateInformation>";

			$package .= "
					</Shipment>  
					</RatingServiceSelectionRequest>";  
		
			$packs[] = $header . $package;
			
			if ( $surepost ) { 
				$header = $this->get_xml_header( $origin, $items[0]['country'], $items[0]['state_province'], true );
				$packs[] = $header . $package;
			}
		}

		foreach( $packs as $pack ) {
			$this->ups_request( $pack );
		}
		
		return;
	}
	
	// Admin panel
	function admin_options() {
		global $woocommerce;
	?>

		<div style="float:right; position:relative; top: 10px; width:250px; border: 3px solid #000; padding: 12px 0; font-weight:bold; font-style:italic; margin-top: 15px; text-align:center; border-radius:7px;-webkit-border-radius:7px">
			<a title='<?php _e( ' More Extensions + Custom WooCommerce Site Development ', 'woocommerce' )?>' href="http://ignitewoo.com" target="_blank" style="color:#0000cc; text-decoration:none">
			</a>
			<br>
			<?php _e( 'Get more custom plugins <br/> + Custom development', 'woocommerce' ); ?>
			<br><br>
			<a title=" More Extensions + Custom WooCommerce Site Development " href="http://ignitewoo.com" target="_blank" style="color:#0000cc; text-decoration:none">Contact us!<br>IgniteWoo.com now!</a>
		</div>

		<h3><?php _e('UPS Shipping', 'woocommerce'); ?></h3>
		<p><?php _e('Configure your API access credentials and other details.', 'woocommerce'); ?></p>

		<p style="color:#000;font-weight:bold">
			<?php _e('NOTE: You must ensure that the following is done or UPS will not be able to quote shipping rates:', 'woocommerce'); ?>
		</p>
			<?php echo '<ul style="color:#000;font-weight:bold; margin-left: 30px">'; ?>
				<li style="list-style-type:disc"><?php _e('Configure weights and dimensions for your products', 'woocommerce');?></li>
				<li style="list-style-type:disc"><?php _e('Configure your catalog settings to use weights and dimensions of pounds/inches or kilograms/centimeters', 'woocommerce');?></li>
			<?php echo '</ul>'; ?>
		<p style="color:#000;font-weight:bold">
			<?php _e('Please do not contact us for support until you have verified correct settings. Be sure to read the <a href="http://ignitewoo.com/ignitewoo-software-documentation/" target="_blank">documentation</a>', 'woocommerce'); ?>
		</p>

		<table class="form-table" >

			<?php $this->generate_settings_html(); ?>

		<?php

			$countries = $woocommerce->countries->countries;

			$country = $this->origincountry;

			$state = $this->originstate;

			if ( '' == $state )
				$state = '*';


                ?>
			<tr valign="top">
				<th scope="rpw" class="titledesc">Shipping Origin Country / State</th>
				<td class="forminp">
					<select class="chosen_select" name="woocommerce_ups_rate_country_state" data-placeholder="<?php _e('Choose a country&raquo;', 'woocommerce'); ?>" title="Country">
						    <?php echo $woocommerce->countries->country_dropdown_options( $country, $state ); ?>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th class="titledesc" scope="row">Package Origin Zip Code</th>
				<td class="forminp">
					<fieldset><legend class="screen-reader-text"><span>Shipping Origin Zip Code</span></legend>
						<label for="woocommerce_ups_rate_originzip"><input type="text" value="<?php echo $this->originzip ?>" style="" id="woocommerce_ups_rate_originzip" name="woocommerce_ups_rate_originzip" class="input-text wide-input "><span class="description">Zip Code where your packages are labeled as shipping from</span></label>
					</fieldset>
				</td>
			</tr>

			<?php $rate_types = get_option( 'woocommerce_ups_rate_types', false ); ?>

			<?php 
				$debug_on = @ini_get( 'display_errors' ); 
				@ini_set( 'display_errors', false );
			?>
			
			<tr valign="top">
			    <th scope="row" class="titledesc"><?php _e('Rates Types to Offer', 'woocommerce'); ?>:</th>
			    <td class="forminp" id="flat_rates">

				    <table>
					<tbody>
					    <thead>
						<tr><th>Domestic</th><th>International</th><th>Poland</th></tr>
					    </thead>

					    <tr><td valign="top">
					    <input type="checkbox" name="rate_types[Next Day Air Early AM]" value="14"  <?php if ( $rate_types['Next Day Air Early AM'] ) echo 'checked="checked"'; ?>> Next Day Air Early AM<br/>
					    <input type="checkbox" name="rate_types[Next Day Air]" value="01" <?php if ( $rate_types['Next Day Air'] ) echo 'checked="checked"'; ?>> Next Day Air<br/>
					    <input type="checkbox" name="rate_types[Next Day Air Saver]" value="13" <?php if ( $rate_types['Next Day Air Saver'] ) echo 'checked="checked"'; ?>> Next Day Air Saver<br/>
					    <input type="checkbox" name="rate_types[2nd Day Air AM]" value="59" <?php if ( $rate_types['2nd Day Air AM'] ) echo 'checked="checked"'; ?>> 2nd Day Air AM<br/>
					    <input type="checkbox" name="rate_types[2nd Day Air]" value="02" <?php if ( $rate_types['2nd Day Air'] ) echo 'checked="checked"'; ?>> 2nd Day Air<br/>
					    <input type="checkbox" name="rate_types[3 Day Select]" value="12" <?php if ( $rate_types['3 Day Select'] ) echo 'checked="checked"'; ?>> 3 Day Select<br/>
					    <input type="checkbox" name="rate_types[Ground]" value="03" <?php if ( $rate_types['Ground'] ) echo 'checked="checked"'; ?>> Ground<br/>
					     <input type="checkbox" name="rate_types[Ground LTL Freight]" value="308" <?php if ( $rate_types['Ground LTL Freight'] ) echo 'checked="checked"'; ?>> Ground LTL Freight<br/>
					     <input type="checkbox" name="rate_types[SurePost]" value="93" <?php if ( isset( $rate_types['SurePost'] ) ) echo 'checked="checked"'; ?>> SurePost (Max weight 70 lbs)<br/>
					    </td>

					    <td valign="top">
					    <input type="checkbox" name="rate_types[Standard]" value="11" <?php if ( $rate_types['Standard'] ) echo 'checked="checked"'; ?>> Standard<br/>
					    <input type="checkbox" name="rate_types[Worldwide Express]" value="07" <?php if ( $rate_types['Worldwide Express'] ) echo 'checked="checked"'; ?>> Worldwide Express<br/>
					    <input type="checkbox" name="rate_types[Worldwide Express Plus]" value="54" <?php if ( $rate_types['Worldwide Express Plus'] ) echo 'checked="checked"'; ?>> Worldwide Express Plus<br/>
					    <input type="checkbox" name="rate_types[Worldwide Expedited]" value="08" <?php if ( $rate_types['Worldwide Expedited'] ) echo 'checked="checked"'; ?>> Worldwide Expedited<br/>
					    <input type="checkbox" name="rate_types[Saver]" value="65" <?php if ( $rate_types['Saver'] ) echo 'checked="checked"'; ?>> Saver<br/>
					    </td>

					    <td valign="top">
					    <input type="checkbox" name="rate_types[UPS Today Standard]" value="82" <?php if ( $rate_types['UPS Today Standard'] ) echo 'checked="checked"'; ?>> UPS Today Standard<br/>
					    <input type="checkbox" name="rate_types[UPS Today Dedicated Courier]" value="83" <?php if ( $rate_types['UPS Today Dedicated Courier'] ) echo 'checked="checked"'; ?>> UPS Today Dedicated Courier<br/>
					    <input type="checkbox" name="rate_types[UPS Today Intercity]" value="84" <?php if ( $rate_types['UPS Today Intercity'] ) echo 'checked="checked"'; ?>> UPS Today Intercity<br/>
					    <input type="checkbox" name="rate_types[UPS Today Express]" value="85" <?php if ( $rate_types['UPS Today Express'] ) echo 'checked="checked"'; ?>> UPS Today Express<br/>
					    <input type="checkbox" name="rate_types[UPS Today ExpressSaver]" value="86" <?php if ( $rate_types['UPS Today ExpressSaver'] ) echo 'checked="checked"'; ?>> UPS Today ExpressSaver<br/>
					    </td></tr>

					</tbody>
				    </table>
			    </td>

			</tr>
			
			<?php @ini_set( 'display_errors', $debug_mode ); ?>

			<tr valign="top">
				<th scope="rpw" class="titledesc">UPS Freight API Test</th>
				<td class="forminp">
					<p><a href="<?php echo add_query_arg( array( 'ign_freight_test' => 'go' ), $_SERVER['REQUEST_URI'] )?>" class="ign_freight_test">Generate API test results.</a>  &nbsp; Clicking the link tests the UPS Freight Rate API using your UPS account as configured into this plugin (be sure to save your settings at least once before performing the test). When the page reloads you will find a cost value below as returned by the test. Copy the cost and configure that into your UPS account as instructed by UPS.
					</p>
					<?php 
					$ftr = get_option( 'ign_ups_freight_test_results', null ); 
					
					if ( !empty( $ftr ) ) {
						?>
						<p><em><strong>Rate result:</strong></em><code><?php echo $ftr ?></code></p>
						<?php
					}
					?>
					

				</td>
			</tr>
			
		</table>

	<?php
	} 


	// process rate type settings
	function process_ups_rates() {

		if ( isset( $_POST['rate_types'] ) )
			$ups_rate_types = $_POST['rate_types'];
		    
		update_option( 'woocommerce_ups_rate_types', $ups_rate_types );

		if ( isset( $_POST['woocommerce_ups_rate_country_state'] ) )
			$data = explode( ':', $_POST['woocommerce_ups_rate_country_state'] );

		update_option( 'woocommerce_ups_rate_country', $data[0] );

		if ( empty( $data[1] ) )
			$data[1] = '';

		update_option( 'woocommerce_ups_rate_state', $data[1] );

		if ( isset( $_POST['woocommerce_ups_rate_originzip'] ) )
			update_option( 'woocommerce_ups_rate_originzip', $_POST['woocommerce_ups_rate_originzip'] );

	}


	public static function add_order_meta_box() { 

		add_meta_box( 'woocommerce-ups-tracking', __('UPS Tracking ID', 'woocommerce'), array( 'ups_rate', 'ups_tracking_number_meta_box' ), 'shop_order', 'side', 'default');

	}

	// add UPS tracking number metabox so admins can enter the number and have it automatically sent to the customer
	function ups_tracking_number_meta_box() {
		global $woocommerce, $post;

		?>

		<div class="add_note">
			<h4><?php _e( 'Add UPS Tracking Number(s)', 'woocommerce'); ?></h4>
			<p><?php _e( 'Enter the UPS tracking numbers (one per line) and click the Update button to save. The customer will be notified.', 'woocommerce'); ?></p>
			<p><textarea name="ups_tracking" id="ups_tracking_id"><?php echo get_post_meta( $_GET['post'], 'ups_tracking_number', true ); ?></textarea><br/>
			<a href="#" class="add_tracking button"><?php _e('Update', 'woocommerce'); ?></a>
		</div>
		<script type="text/javascript">
			
			jQuery( 'a.add_tracking' ).click( function(){
				
				if ( !jQuery( 'textarea#ups_tracking_id' ).val() ) return;
				
				var data = {
					action: 'woocommerce_add_ups_tracking_id',
					post_id:'<?php echo $post->ID; ?>',
					the_ids: jQuery('textarea#ups_tracking_id').val(),
					security: '<?php echo wp_create_nonce( "add-ups-tracking_number" ); ?>'
				};

				jQuery.post( '<?php echo admin_url('admin-ajax.php'); ?>', data, function( response ) {} );
				
				return false;
				
			});
			
		</script>
		<?php
	}
	
	function print_debug() { 

		if ( empty( $this->debug_messages ) )
			return;
			
		?>
		<script>
			//jQuery( document ).ready( function () { 
				//jQuery( "#ups_debug" ).html( '' );
			//}/);
		</script>
		
		<?php
		foreach( $this->debug_messages as $msg ) {
			echo '<p id="ups_debug" style="background-color:#f7f7f7; color: #333"><code>' . $msg . '</code></p>';
		}
	}

}



class UPSFreightRatesAndService {

	protected $username = '';
	protected $password = '';
	protected $accessKey = '';
	protected $sandbox = '';
	protected $endpointUrl = '';
	protected static $endpointUrls = array(
		'sandbox' => 'https://wwwcie.ups.com/webservices/FreightRate',
		'production' => 'https://onlinetools.ups.com/webservices/FreightRate',
	);
	
	public $response = null;
	public $errors = array();
	public $exception = null;
	public $lastRequestParams = array();
	
	public function __construct( $username, $password, $accessKey, $sandbox = false ) {
		
		$this->username = $username;
		$this->password = $password;
		$this->accessKey = $accessKey;
		$this->sandbox = $sandbox;
		
		if ($this->sandbox) {
			$this->endpointUrl = self::$endpointUrls['sandbox'];
		}
		else {
			$this->endpointUrl = self::$endpointUrls['production'];
		}
		
		$this->response = new stdClass();
	}
	
	static public function isOversizedOrOverweightForShipping($length, $width, $height, $weight) {
		$ci =& get_instance();
		$ci->config->load('shipping',true);
		if ($length > $ci->config->item('shipping_max_length','shipping')) {
			return true;
		}
		else if ($weight > $ci->config->item('shipping_max_weight','shipping')) {
			return true;
		}
		else if (($length + $width * 2 + $height * 2) > $ci->config->item('shipping_max_length_and_grith','shipping')) {
			return true;
		}
		else {
			return false;
		}
	}	

	public function getFreightRate( $weight, $width, $length, $height, $origin, $items, $debug = false, &$ups_drop_obj, $test_transaction = array() ) {
		global $woocommerce;
		
		if ( $debug )
			$ups_drop_obj->debug_messages[] = '<pre><strong>BUILDING REQUEST FOR ORIGIN:</strong><br>' . $origin .  '</pre>';
			
		if ( 'g' == get_option( 'woocommerce_weight_unit' ) )
			$weight_unit = 'KGS';
		else if ( 'oz' == get_option( 'woocommerce_weight_unit' ) )
			$weight_unit = 'LBS';
		else 
			$weight_unit = strtoupper( get_option( 'woocommerce_weight_unit' ) );
			
		$dim_unit = strtoupper( get_option( 'woocommerce_dimension_unit' ) );
		
		$to_name = 'My Customers';
		$to_address = '100 Street';
		$to_city = 'Anytown';
		
		$from_name = 'Good Incorporation';
		$from_address = '100 Main';
		$from_city = 'Anytown';
		$from_state = get_option( 'woocommerce_ups_rate_state' );
		$from_country = get_option( 'woocommerce_ups_rate_country' );
		$from_postcode = get_option( 'woocommerce_ups_rate_originzip' );
		
		if ( !empty( $test_transaction ) ) {

			$to_name = $test_transaction['name'];
			$to_address = $test_transaction['address'];
			$to_city = $test_transaction['city'];
			$to_state = $test_transaction['state'];
			$to_country = $test_transaction['country'];
			$to_postcode = $test_transaction['postcode'];
			
			$freight_class = $test_transaction['class'];
			$commodity = $test_transaction['commodity'];
			$subcode = $test_transaction['subcode'];
			
			$from_name = $test_transaction['fname'];
			$from_address = $test_transaction['faddress'];
			$from_city = $test_transaction['fcity'];
			$from_state = $test_transaction['fstate'];
			$from_country = $test_transaction['fcountry'];
			$from_postcode = $test_transaction['fpostcode'];
			
		} else { 
		
			$to_state = $woocommerce->customer->get_shipping_state();
			$to_country = $woocommerce->customer->get_shipping_country();
			$to_postcode = $woocommerce->customer->get_shipping_postcode();
		}

		$request['Request'] = array(
			'RequestOption' => 'Ground'
		);
		
		if ( !empty( $test_transaction ) ) {
			
			$request['ShipFrom'] = array(
				'Name' => $from_name,
				'Address' => array(
					'AddressLine' => $from_address,
					'City' => $from_city,
					'StateProvinceCode' => $from_state,
					'PostalCode' => $from_postcode,
					'CountryCode' => $from_country,
				)
			);
			
		} else { 

			$request['ShipFrom'] = array(
				'Name' => 'Business',
				'Address' => array(
					'AddressLine' => '',
					'City' => '',
					//'StateProvinceCode' => 'CA', // not required
					'PostalCode' => $origin, //get_option( 'woocommerce_ups_rate_originzip' ),  // required 
					'CountryCode' => $items[0]['country'], //get_option( 'woocommerce_ups_rate_country' ), // required
				)
			);
		}
		
		if ( $debug )
			$ups_drop_obj->debug_messages[] = '<pre><strong>SHIP FROM WILL BE:</strong><br>' . print_r( $request['ShipFrom'], true ) . '</pre>';
			
		$request['ShipTo'] = array(
			// All required data: 
			'Name' => $to_name,
			'Address' => array(
				'AddressLine' => $to_address,
				'City' => $to_city,
				'StateProvinceCode' => $to_state,
				'PostalCode' => $to_postcode,
				'CountryCode' => $to_country,
			)
		);
	
		
		$request['PaymentInformation'] = array(
			'ShipmentBillingOption' => array(
				'Code' => '10',
				'Description' => 'PREPAID'
			),
			// All required data:
			'Payer' => array( 
				'Name' => $from_name,
				'Address' => array(
					'AddressLine' => $from_address,
					'City' => $from_city,
					'StateProvinceCode' => $from_state,
					'PostalCode' => $from_postcode,
					'CountryCode' => $from_country,
				)
			)
		);
		
		
		$request['Service'] = array(
			'Code' => '308', // 309 - guaranteed, 334 - guaranteed AM deliver -- DO NOT USE, many pkgs do not qualify
			//'Description' => 'UPS Freight LTL'
		);
	
		$request['HandlingUnitOne'] = array(
			'Quantity' => '1',
			'Type' => array(
				'Code' => 'PLT',
			)
		);
	
		$request['Commodity'] = array(
			'CommodityID' => '',
			'Description' => 'No Description',
			'Weight' => array(
				'UnitOfMeasurement' => array
					(
						'Code' => $weight_unit,
						//'Description' => 'Pounds'
					),
				'Value' => $weight
			),
		);
		
		if ( !empty( $width ) && !empty( $length ) && !empty( $height ) ) { 
			$request['Commodity']['Dimensions'] = array(
				'UnitOfMeasurement' => array(
					'Code' => $dim_unit,
					//'Description' => 'Inches'
				),
				'Length' => ceil( $length ),
				'Width' => ceil( $width ),
				'Height' => ceil( $height )
			);
		}
		
		$request['Commodity']['NumberOfPieces'] = '1'; // number of piece in the HandlingUnitOne
		$request['Commodity']['PackagingType'] = array(
				'Code' => 'PLT',
				'Description' => 'PALLET'
			);
			//'DangerousGoodsIndicator' => '',
			/*'CommodityValue' => array(
				'CurrencyCode' => 'USD',
				'MonetaryValue' => '100'
			),*/
			
		
		$highest_freight_class = '50';
	
		if ( empty( $freight_class ) )
		foreach( $items as $item ) { 
		
			if ( !empty( $item['class'] ) ) {

				$class_val = 0;
				
				$tid = get_term_by( 'slug', $item['class'], 'product_shipping_class' );
				
				if ( !empty( $tid->term_id ) ) { 
				
					$class_val = get_woocommerce_term_meta( $tid->term_id, 'fedex_freight_class', true );
					
					if ( !empty( $class_val ) ) { 
						$freight_class = str_replace( 'freight_', '', $class_val );
						$freight_class = str_replace( '_', '.', $freight_class );
						$freight_class = floatval( $freight_class );
					}
				}
				
				if ( $freight_class > $highest_freight_class ) {
					$highest_freight_class = $freight_class;
				}
			}
		}

		$request['Commodity']['FreightClass'] = $freight_class;
		$request['Commodity']['NMFCCommodityCode'] = '';
		if ( !empty( $commodity ) ) {
			//$request['Commodity']['NMFCCommodityCode'] = $commodity;
			$request['Commodity']['NMFCCommodity']['PrimeCode'] = $commodity;
			$request['Commodity']['NMFCCommodity']['SubCode'] = $subcode;
		}

		if ( 'res' == $ups_drop_obj->settings['destination_type'] )
			$request['ShipmentServiceOptions']['DeliveryOptions']['ResidentialDeliveryIndicator'] = '1';
			
		if ( $debug )
			$ups_drop_obj->debug_messages[] = '<pre><strong>REQUEST:</strong><br>' . print_r( $request, true ) . '</pre>';
			
		/*
		$shipmentserviceoptions['PickupOptions'] = array
		(
			  'HolidayPickupIndicator' => '',
			  'InsidePickupIndicator' => '',
			  'ResidentialPickupIndicator' => '',
			  'WeekendPickupIndicator' => '',
			  'LiftGateRequiredIndicator' => ''
		);
			
		$shipmentserviceoptions['OverSeasLeg'] = array
		(
			'Dimensions' => array
			(
				 'Volume' => '20',
				 'UnitOfMeasurement' => array
				 (
					 'Code' => 'CF',
					 'Description' => 'String'
				 )
			),
			'Value' => array
			(
				 'Cube' => array
				 (
					  'CurrencyCode' => 'USD',
					  'MonetaryValue' => '5670'
				 )
			),
			'COD' => array
			(
				 'CODValue' => array
				 (
					  'CurrencyCode' => 'USD',
					  'MonetaryValue' => '363'
				 ),
				 'CODPaymentMethod' => array
				 (
					  'Code' => 'M',
					  'Description' => 'For Company Check'
				 ),
				 'CODBillingOption' => array
				 (
					  'Code' => '01',
					  'Description' => 'Prepaid'
				 ),
				 'RemitTo' => array
				 (
					  'Name' => 'RemitToSomebody',
					  'Address' => array
					  (
							'AddressLine' => '640 WINTERS AVE',
							'City' => 'PARAMUS',
							'StateProvinceCode' => 'NJ',
							'PostalCode' => '07652',
							'CountryCode' => 'US'
					  ),
					  'AttentionName' => 'C J Parker'
				 )
			 ),
			 'DangerousGoods' => array
			 (
				   'Name' => 'Very Safe',
				   'Phone' => array
				   (
					   'Number' => '453563321',
					   'Extension' => '1111'
				   ),
				   'TransportationMode' => array
				   (
					   'Code' => 'CARGO',
					   'Description' => 'Cargo Mode'
				   )
			 ),
			 'SortingAndSegregating' => array
			 (
				   'Quantity' => '23452'
			 ),
			 'CustomsValue' => array
			 (
				   'CurrencyCode' => 'USD',
				   'MonetaryValue' => '23457923'
			 ),
			 'HandlingCharge' => array
			 (
				   'Amount' => array
				   (
					   'CurrencyCode' => 'USD',
					   'MonetaryValue' => '450'
				   )
			 )
		);
		$request['ShipmentServiceOptions'] = $shipmentserviceoptions;
		*/

		$this->lastRequestParams = $request;
		$ret = $this->request($request);
		
		//if ( $debug )
		// 	$ups_drop_obj->debug_messages[] = '<pre>' . print_r( $ret, true ) . '</pre>';
			
		return $ret;

	}

	protected function request($request) {

		try {

			// initialize soap client
			$client = new SoapClient( dirname(__FILE__) . '/schemas/FreightRate.wsdl', array(
				'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
				'trace' => 1
			));
		
			//set endpoint url
			$client->__setLocation( $this->endpointUrl );
		
			//create soap header
			$UPSSecurity = array(
				'UsernameToken' => array(
					'Username' => $this->username,
					'Password' => $this->password
				),
				'ServiceAccessToken' => array(
					'AccessLicenseNumber' => $this->accessKey
				)
			);
	
			$header = new SoapHeader ('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', $UPSSecurity );

			$client->__setSoapHeaders($header);
		
			//get response
			$this->response = $client->__soapCall( 'ProcessFreightRate' ,array( $request ) );
			//var_dump( $client->__getLastRequestHeaders(), $client->__getLastRequest() ); die;	
			//print_r( $client->__getLastResponse() );
			
			return $client->__getLastResponse();
			
			
		} catch(Exception $e) {
		
			$this->exception = $e;
			
			//print_r( $this->exception );
			
			return $this->exception;
		}
		
	}

} 



// Ajax action callback for tracking number meta box input
add_action('wp_ajax_woocommerce_add_ups_tracking_id', 'woocommerce_add_ups_drop_ship_tracking_id' );

function woocommerce_add_ups_drop_ship_tracking_id() { 

	check_ajax_referer( 'add-ups-tracking_number', 'security' );

	// save tracking number to order meta data
	update_post_meta( $_POST['post_id'], 'ups_tracking_number', $_POST['the_ids'] );

	$the_ids = explode( "\n", $_POST['the_ids'] );

	// send a note to the customer

        $order = new WC_Order( $_POST['post_id'] );

	if ( !$order ) return;
            
        $email_heading = __( 'A note has been added to your order', 'woocommerce' );
            
        $subject = sprintf(__( '[%s] A note has been added to your order', 'woocommerce' ), get_bloginfo( 'name' ) );

	ob_start();

	?>

	<p><?php _e( "Hello, your order has a UPS tracking number:", 'woocommerce' ); ?></p>

	<p><?php _e( 'Order #', 'woocommerce' ) . ' ' . $_POST['post_id']; ?></p>

	<?php foreach( $the_ids as $id ) { ?>

		<p>UPS Tracking ID: <a href="https://wwwapps.ups.com/WebTracking/processInputRequest?AgreeToTermsAndConditions=yes&loc=en_US&tracknum=<?php echo $id ?>"><?php echo $id ?></a></p>

	<?php } ?>

	<p><?php _e( 'Thanks for your business!', 'woocommerce' ) ?></p>

	<?php

        $message = ob_get_clean();

        woocommerce_mail( $order->billing_email, $subject, $message );

	die;

}


// TEMPLATE TAG FOR DISPLAYING TRACKING NUMBERS: 
function insert_ups_drop_ship_track_ids( $order_id, $before = '<div>', $after = '</div>' ) { 

	$the_ids = get_post_meta( $order_id, 'ups_tracking_number', true );

	if ( !$the_ids ) return;

	$the_ids = explode( "\n", $the_ids );

	foreach( $the_ids as $id )
		echo $before . 'UPS Tracking ID: <a target="_blank" href="https://wwwapps.ups.com/WebTracking/processInputRequest?AgreeToTermsAndConditions=yes&loc=en_US&tracknum=' . $id .'">'. $id . '</a>' . $after;

}


/*******************************************************************************************

class ups_tracker { 

	    var $AccessLicenseNumber;  
	    var $UserId;  
	    var $Password;
	    var $credentials;

	    function __construct($access,$user,$pass) {
		$this->AccessLicenseNumber = $access;
		$this->UserID = $user;
		$this->Password = $pass;	
		$this->credentials = 1;
	    }

	    function getTrack($trackingNumber) {
		if ($this->credentials != 1) {
			print 'Please set your credentials';
			die();
		}
		
		$data ="<?xml version=\"1.0\"?>
		<AccessRequest xml:lang='en-US'>
		    <AccessLicenseNumber>$this->AccessLicenseNumber</AccessLicenseNumber>
		    <UserId>$this->UserID</UserId>
		    <Password>$this->Password</Password>
		</AccessRequest>
		<?xml version=\"1.0\"?>
		<TrackRequest>
		    <Request>
			<TransactionReference>
			    <CustomerContext>
				<InternalKey>blah</InternalKey>
			    </CustomerContext>t_
			    <XpciVersion>1.0</XpciVersion>
			</TransactionReference>
			<RequestAction>Track</RequestAction>
		    </Request>
		    <TrackingNumber>$trackingNumber</TrackingNumber>
		</TrackRequest>";
		$ch = curl_init('https://www.ups.com/ups.app/xml/Track');
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_TIMEOUT, 60);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$result=curl_exec ($ch);
		// echo '<!-- '. $result. ' -->';
		$data = strstr($result, '<?');
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $data, $vals, $index);
		xml_parser_free($xml_parser);
		$params = array();
		$level = array();
		foreach ($vals as $xml_elem) {
		if ($xml_elem['type'] == 'open') {
		    if (array_key_exists('attributes',$xml_elem)) {
			list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
		    } else {
			$level[$xml_elem['level']] = $xml_elem['tag'];
		    }
		}
		if ($xml_elem['type'] == 'complete') {
		    $start_level = 1;
		    $php_stmt = '$params';
		    while($start_level < $xml_elem['level']) {
			$php_stmt .= '[$level['.$start_level.']]';
			$start_level++;
		    }
		    $php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
		    eval($php_stmt);
		}
		}
		curl_close($ch);
		return $params;
	    }

}


class woocommerce_ups_shipping_label { 


	function __construct() { 
	}


	// ================= Shipment Accept Request ===============
	function accept_request() { 

		$digest = $this->get_digest();

		if ( !$digest || '' == $digest ) 
			return false;

		$xml_request="<?xml version=”1.0″ encoding=”ISO-8859-1″?>
			<AccessRequest>
				<AccessLicenseNumber>ACCESS LICENCE NUMBER</AccessLicenseNumber>
				<UserId>UPS USERNAME</UserId>
				<Password>UPS PASSWORD</Password>
			</AccessRequest>
			<ShipmentAcceptRequest>
				<Request>
					<TransactionReference>
						<CustomerContext>Customer Comment</CustomerContext>
					</TransactionReference>
					<RequestAction>ShipAccept</RequestAction>
					<RequestOption>1</RequestOption>
				</Request>
				<ShipmentDigest>{$digest}</ShipmentDigest>
			</ShipmentAcceptRequest>";

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, “https://wwwcie.ups.com/ups.app/xml/ShipAccept”);

		// uncomment the next line if you get curl error 60: error setting certificate verify locations
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_request);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3600);

		$xml = curl_exec ($ch); // SHIP ACCEPT RESPONSE

		if ( !$xml || '' == $xml ) 
			return false;

		preg_match_all( "/\<ShipmentAcceptResponse\>(.*?)\<\/ShipmentAcceptResponse\>/s", $xml, $bookblocks );

		foreach( $bookblocks[1] as $block ) {

			preg_match_all( "/\<GraphicImage\>(.*?)\<\/GraphicImage\>/", $block, $label ); // GET LABEL

			preg_match_all( "/\<TrackingNumber\>(.*?)\<\/TrackingNumber\>/", $block, $tracking ); // GET TRACKING NUMBER
			//echo( $author[1][0].”\n” );
		}

		echo '<img src="data:image/gif;base64,'. $label[1][0] . '"/>';

	}


	// ==================== GET DIGEST ==========================
	function get_digest() { 

		$xml_request='<?xml version=”1.0″?>
		<AccessRequest xml:lang=”en-US”>
			<AccessLicenseNumber>ACCESS LICENCE NUMBER</AccessLicenseNumber>
			<UserId>UPS USERNAME</UserId>
			<Password>UPS PASSWORD</Password>
		</AccessRequest>
		<ShipmentConfirmRequest xml:lang=”en-US”>
			<Request>
				<TransactionReference>
					<CustomerContext>Customer Comment</CustomerContext>
					<XpciVersion/>
				</TransactionReference>
				<RequestAction>ShipConfirm</RequestAction>
				<RequestOption>validate</RequestOption>
			</Request>
			<LabelSpecification>
				<LabelPrintMethod>
					<Code>GIF</Code>
					<Description>gif file</Description>
				</LabelPrintMethod>
				<HTTPUserAgent>Mozilla/4.5</HTTPUserAgent>
				<LabelImageFormat>
					<Code>GIF</Code>
					<Description>gif</Description>
				</LabelImageFormat>
			</LabelSpecification>
			<Shipment>
				<RateInformation>
					<NegotiatedRatesIndicator/>
				</RateInformation>
				<Description/>
				<Shipper>
					<Name>TEST</Name>
					<PhoneNumber>111-111-1111</PhoneNumber>
					<ShipperNumber>SHIPPER NUMBER</ShipperNumber>
					<TaxIdentificationNumber>1234567890</TaxIdentificationNumber>
					<Address>
						<AddressLine1>AIRWAY ROAD SUITE 7</AddressLine1>
						<City>SAN DIEGO</City>
						<StateProvinceCode>CA</StateProvinceCode>
						<PostalCode>92154</PostalCode>
						<PostcodeExtendedLow></PostcodeExtendedLow>
						<CountryCode>US</CountryCode>
					</Address>
				</Shipper>
				<ShipTo>
					<CompanyName>Yats</CompanyName>
					<AttentionName>Yats</AttentionName>
					<PhoneNumber>123.456.7890</PhoneNumber>
					<Address>
						<AddressLine1>AIRWAY ROAD SUITE 7</AddressLine1>
						<City>SAN DIEGO</City>
						<StateProvinceCode>CA</StateProvinceCode>
						<PostalCode>92154</PostalCode>
						<CountryCode>US</CountryCode>
					</Address>
				</ShipTo>
				<ShipFrom>
					<CompanyName>Ship From Company Name</CompanyName>
					<AttentionName>Ship From Attn Name</AttentionName>
					<PhoneNumber>1234567890</PhoneNumber>
					<TaxIdentificationNumber>1234567877</TaxIdentificationNumber>
					<Address>
						<AddressLine1>AIRWAY ROAD SUITE 7</AddressLine1>
						<City>SAN DIEGO</City>
						<StateProvinceCode>CA</StateProvinceCode>
						<PostalCode>92154</PostalCode>
						<CountryCode>US</CountryCode>
					</Address>
				</ShipFrom>
				<PaymentInformation>
					<Prepaid>
					<BillShipper>
						<AccountNumber>SHIPPER NUMBER</AccountNumber>
					</BillShipper>
					</Prepaid>
				</PaymentInformation>
				<Service>
					<Code>02</Code>
					<Description>2nd Day Air</Description>
				</Service>
				<Package>
					<PackagingType>
						<Code>02</Code>
						<Description>Customer Supplied</Description>
					</PackagingType>
					<Description>Package Description</Description>
					<ReferenceNumber>
						<Code>00</Code>
						<Value>Package</Value>
					</ReferenceNumber>
					<PackageWeight>
						<UnitOfMeasurement/>
						<Weight>60.0</Weight>
					</PackageWeight>
					<LargePackageIndicator/>
					<AdditionalHandling>0</AdditionalHandling>
				</Package>
			</Shipment>
		</ShipmentConfirmRequest>';

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, “https://wwwcie.ups.com/ups.app/xml/ShipConfirm”);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_request);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3600);

		$xml = curl_exec ($ch); // SHIP CONFORMATION RESPONSE

		if ( !$xml || '' == $xml )
			return false;

		preg_match_all( "/\<ShipmentConfirmResponse\>(.*?)\<\/ShipmentConfirmResponse\>/s", $xml, $bookblocks );

		foreach( $bookblocks[1] as $block ) {
			preg_match_all( "/\<ShipmentDigest\>(.*?)\<\/ShipmentDigest\>/", $block, $label ); // SHIPPING DIGEST
			return $label[1][0]; 
		}

		return false;

	}

}

*****************************************************************************************/
