(function($) {
    "use strict";

    $(document).ready(function() {
        /**
         * Return a formatted currency value
         */
        function tm_set_price(value) {
            return accounting.formatMoney(value, {
                symbol: tm_final_price_js.currency_format_symbol,
                decimal: tm_final_price_js.currency_format_decimal_sep,
                thousand: tm_final_price_js.currency_format_thousand_sep,
                precision: tm_final_price_js.currency_format_num_decimals,
                format: tm_final_price_js.currency_format
            });
        }
        
        function tm_calculate_product_price(totals){
            var rules=totals.data('product-price-rules'),
                price=parseFloat(totals.data('price')),
                $cart=$('.cart');
            
            if (!rules || !$cart){
                return price;
            }else{
                var qty_element = $cart.find('input.qty'),
                    qty = parseFloat(qty_element.val()),
                    current_variation=$cart.find('input[name^=variation_id]').val();

                if (!current_variation) {
                    current_variation = 0;
                }
                if(!rules[current_variation])current_variation = 0;
                if (isNaN(qty)){
                    if (totals.attr("data-is-sold-individually") || qty_element.length==0){
                        qty=1;
                    }
                }
                if (rules[current_variation]){
                    $(rules[current_variation]).each(function(id,rule){
                        var min=parseFloat(rule['min']),
                            max=parseFloat(rule['max']),
                            type=rule['type'],
                            value=parseFloat(rule['value']);

                        if (min <= qty && qty <= max){
                            switch (type){
                                case "percentage":
                                    price= price*(1-value/100);
                                    price = Math.ceil(price * Math.pow(10, tm_final_price_js.currency_format_num_decimals) - 0.5) * Math.pow(10, -( parseInt(tm_final_price_js.currency_format_num_decimals) ));
                                    if(price < 0){
                                        price=0;
                                    }
                                    return false;
                                break;
                                case "price":
                                    price= price-value;
                                    price = Math.ceil(price * Math.pow(10, tm_final_price_js.currency_format_num_decimals) - 0.5) * Math.pow(10, -( parseInt(tm_final_price_js.currency_format_num_decimals) ));
                                    if(price < 0){
                                        price=0;
                                    }
                                    return false;
                                break;
                                case "fixed":
                                    price= value;
                                    price = Math.ceil(price * Math.pow(10, tm_final_price_js.currency_format_num_decimals) - 0.5) * Math.pow(10, -( parseInt(tm_final_price_js.currency_format_num_decimals) ));
                                    if(price < 0){
                                        price=0;
                                    }
                                    return false;
                                break;
                            }
                            
                        }
                    });    
                }
                
            }
            return price;
        }

        function tm_set_tax_price(value, _cart) {
            if (_cart){
                var taxable = _cart.attr("data-taxable"),
                    tax_rate = _cart.attr("data-tax-rate"),
                    tax_string = _cart.attr("data-tax-string"),
                    tax_display_mode = _cart.attr("data-tax-display-mode"),
                    prices_include_tax = _cart.attr("data-prices-include-tax");

                if ( taxable ) {
                    if ( tax_display_mode== 'excl' ) {// Display without taxes
                        if (prices_include_tax=="1"){
                            value = parseFloat(value)/(1+(tax_rate/100));
                        }else{
                            
                        }                        

                    } else {// Display with taxes
                        if (prices_include_tax=="1"){
                            
                        }else{
                            value = parseFloat(value)*(1+(tax_rate/100));
                        }

                    }
                }

            }
            return value;
        }

        var main_cart = $('.cart:last');
        var $form = main_cart.parent();

        var $cart = $('.cart');
        var $totals_holder = $cart.find('#tm-final-price-total');

        /**
         * Set event handlers
         */
        function tm_custom_prices_init() {

            $form.find('.cart .total_price').on('wc-measurement-price-calculator-total-price-change', function(e,d,v) {
                $(this).closest('.cart').trigger('tm-epo-update');
            });
            $form.find('.cart .product_price').on('wc-measurement-price-calculator-product-price-change', function(e,d,v) {
                $totals_holder.data('price',v);
                $(this).closest('.cart').trigger('tm-final-price-update');
            });

            // trigger global custom update event when variation or quantity changes
            $form.find('.cart').on('change', 'input[name=variation_id], input.qty', function(event) {
                $(this).closest('.cart').trigger('tm-final-price-update');
            });

            // global custom update event
            $form.find('.cart').bind('tm-final-price-update', function() {
                var $cart = $(this);
               
                var $tm_custom_price_plugin_check = $('#tm-custom-prices .tmcp-field');
                
                if ($tm_custom_price_plugin_check.length>0 ){
                    $totals_holder.hide();
                    return;
                }
                
                var product_price = tm_calculate_product_price($totals_holder);
                var product_type = $totals_holder.data('type');

                if (product_type == 'variable') {
                    $cart.find('.single_variation').after($totals_holder);
                }                
                var qty_element         = $cart.find('input.qty'),
                    qty                 = parseFloat(qty_element.val());

                if (isNaN(qty)){
                    if ($totals_holder.attr("data-is-sold-individually") || qty_element.length==0){
                        qty=1;
                    }
                }
                               
                if (qty > 0) {                   
                    var formatted_final_total;
                    if (product_price) {
                        product_price=tm_set_tax_price(product_price,$totals_holder);
                        var product_total_price = parseFloat(product_price * qty);
                        var extra_fee=0;
                        if (tm_final_price_js.extra_fee){
                            extra_fee=parseFloat(tm_final_price_js.extra_fee);
                            if (isNaN(extra_fee)){
                                extra_fee=0;
                            }
                        }
                        formatted_final_total = tm_set_price(product_total_price + extra_fee);
                    }
                    var html = '<dl class="tm-final-price-totals">';
                    if (formatted_final_total) {
                        html = html + '<dt>' + tm_final_price_js.i18n_final_total + '</dt><dd><span class="amount final">' + formatted_final_total + '</span></dd>';
                    }
                    html = html + '</dl>';
                    $totals_holder.html(html);
                } else {
                    $totals_holder.empty();
                }
            });

            // update prices when a variation is found
            $('.variations_form').on('found_variation', function(event, variation) {
                var $variation_form = $(this);                
                
                var variations=$totals_holder.data('variations');                
                var product_price;
                if (variations && variation.variation_id && variations[variation.variation_id]){
                    product_price=variations[variation.variation_id];
                    $totals_holder.data('price', product_price);
                }
                else if ($(variation.price_html).find('.amount:last').size()) {
                    product_price = $(variation.price_html).find('.amount:last').text();
                    product_price = product_price.replace(tm_final_price_js.currency_format_thousand_sep, '');
                    product_price = product_price.replace(tm_final_price_js.currency_format_decimal_sep, '.');
                    product_price = product_price.replace(/[^0-9\.]/g, '');
                    product_price = parseFloat(product_price);
                    $totals_holder.data('price', product_price);
                }                            
                $variation_form.trigger('tm-final-price-update');
            });

            $('.variations select').on('blur',function() {
                var $variation_form = $(this).closest('.cart');
                $variation_form.trigger('tm-final-price-update');
            });
        }

        if ($('.variations_form').length > 0) {
            $('.variations_form').on('wc_variation_form', function() {
                tm_custom_prices_init();
                $('.cart').trigger('tm-final-price-update');
            });
        } else {
            tm_custom_prices_init();
            $('.cart').trigger('tm-final-price-update');
        }

    });
})(jQuery);