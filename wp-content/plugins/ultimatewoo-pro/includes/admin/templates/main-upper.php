<?php
/**
 *	Main (upper) HTML markup for settings page.
 *
 *	@package UltimateWoo Pro
 *	@author UltimateWoo
 *	@since 1.0
 */
?>

<div id="ultimatewoo-settings" class="wrap">

	<div id="icon-options-general" class="icon32"></div>
	<h1><?php esc_attr_e( 'UltimateWoo Settings', 'ultimatewoo-pro' ); ?></h1>

	<div id="poststuff">

		<?php do_action( 'ultimatewoo_settings_page_before_module_settings' ); ?>

		<div id="post-body" class="metabox-holder columns-2">

			<!-- main content -->
			<div id="post-body-content">

				<div class="meta-box-sortables ui-sortable">