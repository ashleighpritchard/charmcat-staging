<?php 
class IGN_UPS_Freight_Mapping {
	
	var $classes;
			
	public function __construct() {

		if ( class_exists( 'IGN_Fedex_Freight_Mapping' ) ) { 
		
			// If the Fedex freight mapping class exists then these hooks were already defined so just return
			return;
		}
		
		$this->classes = array(
			'freight_50'   => 'CLASS_050',
			'freight_55'   => 'CLASS_055',
			'freight_60'   => 'CLASS_060',
			'freight_65'   => 'CLASS_065',
			'freight_70'   => 'CLASS_070',
			'freight_77.5' => 'CLASS_077_5',
			'freight_85'   => 'CLASS_085',
			'freight_92.5' => 'CLASS_092_5',
			'freight_100'  => 'CLASS_100',
			'freight_110'  => 'CLASS_100',
			'freight_125'  => 'CLASS_125',
			'freight_150'  => 'CLASS_150',
			'freight_175'  => 'CLASS_175',
			'freight_200'  => 'CLASS_200',
			'freight_250'  => 'CLASS_250',
			'freight_300'  => 'CLASS_300',
			'freight_400'  => 'CLASS_400',
			'freight_500'  => 'CLASS_500'
		);
		
		/*
		add_action( 'init', array( $this, 'drop_shippers' ), 15 );
		add_action( 'add_meta_boxes',  array( $this, 'drop_shippers_meta' ), 15 );
		add_action( 'save_post',  array( $this, 'save_shipper' ), 1, 1 );
		*/
		
		add_action( 'product_shipping_class_add_form_fields', array( $this, 'add_form' ) );
		add_action( 'product_shipping_class_edit_form_fields', array( $this, 'edit_form' ), 10, 2 );
		add_action( 'created_term', array( $this, 'save' ), 10, 3 );
		add_action( 'edit_term', array( $this, 'save' ), 10, 3 );
		add_filter( 'manage_edit-product_shipping_class_columns', array( $this, 'columns' ) );
		add_filter( 'manage_product_shipping_class_custom_column', array( $this, 'column' ), 10, 3 );
	}

	/*
	public function drop_shippers_meta() { 
		add_meta_box( 'fedex_shipper', __( 'LTL Account Information', 'woocommerce' ), array( &$this, 'fedex_address' ), 'fedex_ltl_shippers', 'normal', 'high' );
	}
	
	function save_shipper( $post_id ) { 
	
		if ( wp_is_post_revision( $post_id ) )
			return;
			
		if ( empty( $_POST['ltl'] ) )
			return;
			
		update_post_meta( $post_id, 'fedex_ltl_info', $_POST['ltl'] );
	}
	
	
	public function fedex_address() { 
		global $post;
		
		?>
		<style>
		.form-field input, .form-field textarea {
			width: 100%;
		}
		.form-field input[type="checkbox"] {
			width: 26px;
			float:left;
		}
		</style>
		<?php 
		
		$meta = get_post_meta( $post->ID, 'fedex_ltl_info', true );
		
		_e( '* All fields are required', 'woocommerce' );
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[account]', 'label' => __( 'LTL Account Number', 'woocommerce' ), 'placeholder' => '', 'value' => isset( $meta['account'] ) ? $meta['account'] : '' ) );
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[billing_street_address]', 'label' => __( 'Billing Street Address', 'woocommerce' ), 'placeholder' => '', 'value' => isset( $meta['billing_street_address'] ) ? $meta['billing_street_address'] : '' ) );
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[billing_street_address_2]', 'label' => __( 'Billing Street Address 2', 'woocommerce' ), 'value' => isset( $meta['billing_street_address_2'] ) ? $meta['billing_street_address_2'] : '' ) );
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[billing_city]', 'label' => __( 'Billing City', 'woocommerce' ), 'value' => isset( $meta['billing_city'] ) ? $meta['billing_city'] : '' ) );
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[billing_state]', 'label' => __( 'Billing State', 'woocommerce' ), 'placeholder' => '', 'description' => __( '2 letter ISO state code', 'woocommerce' ), 'value' => isset( $meta['billing_state'] ) ? $meta['billing_state'] : '' ) );
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[billing_postcode]', 'label' => __( 'Billing Postal Code', 'woocommerce' ), 'placeholder' => '', 'value' => isset( $meta['billing_postcode'] ) ? $meta['billing_postcode'] : '' ) );
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[billing_country]', 'label' => __( 'Billing Country', 'woocommerce' ), 'placeholder' => '', 'description' => __( '2 letter ISO country code', 'woocommerce' ), 'value' => isset( $meta['billing_country'] ) ? $meta['billing_country'] : '' ) );

		woocommerce_wp_text_input( array( 'id' => 'ltl[shipping_street_address]', 'label' => __( 'Shipping Street Address', 'woocommerce' ), 'placeholder' => '', 'value' => isset( $meta['shipping_street_address'] ) ? $meta['shipping_street_address'] : '' ));
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[shipping_street_address_2]', 'label' => __( 'Shipping Street Address 2', 'woocommerce' ), 'placeholder' => '', 'value' => isset( $meta['shipping_street_address_2'] ) ? $meta['shipping_street_address_2'] : '' ));
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[shipping_city]', 'label' => __( 'Shipping City', 'woocommerce' ), 'placeholder' => '', 'value' => isset( $meta['shipping_city'] ) ? $meta['shipping_city'] : '' ));
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[shipping_state]', 'label' => __( 'Shipping State', 'woocommerce' ), 'placeholder' => '', 'description' => __( '2 letter ISO state code', 'woocommerce' ), 'value' => isset( $meta['shipping_state'] ) ? $meta['shipping_state'] : '' ) );
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[shipping_postcode]', 'label' => __( 'Shipping Postal Code', 'woocommerce' ), 'placeholder' => '', 'value' => isset( $meta['shipping_postcode'] ) ? $meta['shipping_postcode'] : '' ) );
		
		woocommerce_wp_text_input( array( 'id' => 'ltl[shipping_country]', 'label' => __( 'Shipping Country', 'woocommerce' ), 'placeholder' => '', 'description' => __( '2 letter ISO country code', 'woocommerce' ), 'value' => isset( $meta['shipping_country'] ) ? $meta['shipping_country'] : '' ) );
		
		woocommerce_wp_checkbox( array( 'id' => 'ltl[residential]', 'label' => __( 'This is a residential shipping address', 'woocommerce' ), 'placeholder' => '', 'value' => !empty( $meta['residential'] ) ? $meta['residential'] : '', 'cbvalue' => 'yes' ) );
	
	}
	
	public function drop_shippers() {
         
		register_post_type( "fedex_ltl_shippers",
		
			array(
						'labels' => array(
								'name' 					=> __( 'Fedex LTL Shippers', 'woocommerce' ),
								'singular_name' 		=> __( 'Fedex LTL Shipper', 'woocommerce' ),
								'menu_name'				=> _x( 'Fedex LTL Shippers', 'Admin menu name', 'woocommerce' ),
								'add_new' 				=> __( 'Add Fedex LTL Shipper', 'woocommerce' ),
								'add_new_item' 			=> __( 'Add New Fedex LTL Shipper', 'woocommerce' ),
								'edit' 					=> __( 'Edit', 'woocommerce' ),
								'edit_item' 			=> __( 'Edit Fedex LTL Shipper', 'woocommerce' ),
								'new_item' 				=> __( 'New Fedex LTL Shipper', 'woocommerce' ),
								'view' 					=> __( 'View Fedex LTL Shippers', 'woocommerce' ),
								'view_item' 			=> __( 'View Fedex LTL Shipper', 'woocommerce' ),
								'search_items' 			=> __( 'Search Fedex LTL Shippers', 'woocommerce' ),
								'not_found' 			=> __( 'No Fedex LTL Shippers found', 'woocommerce' ),
								'not_found_in_trash' 	=> __( 'No Coupons found in trash', 'woocommerce' ),
								'parent' 				=> __( 'Parent Fedex LTL Shipper ', 'woocommerce' )
							),
						'description' 			=> __( 'This is where you can add new Fedex LTL drop shippers.', 'woocommerce' ),
						'public' 			=> false,
						'show_ui' 			=> true,
						'capability_type' 		=> 'shop_order',
						'map_meta_cap'			=> true,
						'publicly_queryable' 		=> false,
						'exclude_from_search' 		=> true,
						'show_in_menu' 			=> current_user_can( 'manage_woocommerce' ) ? 'woocommerce' : true,
						'hierarchical' 			=> false,
						'rewrite' 			=> false,
						'query_var' 			=> false,
						'supports' 			=> array( 'title' ),
						'show_in_nav_menus'		=> false,
						'show_in_admin_bar'     	=> true
					)
			
		);
	}
	*/
	
	/**
	 * Add term - fields
	 */
	public function add_form() {
		?>
		<div class="form-field">
			<label for="fedex_freight_class"><?php _e( 'Freight Class', 'woocommerce' ); ?></label>
			<select id="fedex_freight_class" name="fedex_freight_class" class="postform">
				<option value=""><?php _e( 'Default', 'woocommerce' ); ?></option>
				<?php foreach ( $this->classes as $key => $value ) : ?>
					<option value="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $value ); ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<?php
	}

	/**
	 * Edit term - fields
	 * @param mixed $term Term (category) being edited
	 * @param mixed $taxonomy Taxonomy of the term being edited
	 */
	public function edit_form( $term, $taxonomy ) {
		$fedex_freight_class = get_woocommerce_term_meta( $term->term_id, 'fedex_freight_class', true );
		?>
		<tr class="form-field">
			<th scope="row" valign="top"><label for="fedex_freight_class"><?php _e( 'Freight Class', 'woocommerce' ); ?></label></th>
			<td>
				<select id="fedex_freight_class" name="fedex_freight_class" class="postform">
					<option value=""><?php _e( 'Default', 'woocommerce' ); ?></option>
					<?php foreach ( $this->classes as $key => $value ) : ?>
						<option <?php selected( $fedex_freight_class, $key ); ?> value="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $value ); ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<?php
	}

	/**
	 * Save the freight class
	 * 
	 * @param  int $term_id
	 * @param  int $tt_id
	 * @param  string $taxonomy
	 */
	public function save( $term_id, $tt_id, $taxonomy ) {
		if ( isset( $_POST['fedex_freight_class'] ) ) {
			update_woocommerce_term_meta( $term_id, 'fedex_freight_class', sanitize_text_field( $_POST['fedex_freight_class'] ) );
		}
	}
	/**
	 * Column added to shipping class admin.
	 *
	 * @param mixed $columns
	 * @return array
	 */
	public function columns( $columns ) {
		$columns['fedex_freight_class'] = __( 'Freight Class', 'woocommerce' );

		return $columns;
	}

	/**
	 * Column value added to shipping class admin.
	 *
	 * @param mixed $columns
	 * @param mixed $column
	 * @param mixed $id
	 * @return array
	 */
	public function column( $columns, $column, $id ) {
		if ( $column == 'fedex_freight_class' ) {
			$fedex_freight_class = get_woocommerce_term_meta( $id, 'fedex_freight_class', true );
			$columns             .= $fedex_freight_class ? $this->classes[ $fedex_freight_class ] : '-';
		}

		return $columns;
	}
}

new IGN_UPS_Freight_Mapping();