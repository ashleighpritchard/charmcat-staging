<?php
/**
 * Financial Quarterly Periods
 *
 * Calculates the current and past financial quarterly periods.
 *
 * @version   0.1
 * @author    CharmCat
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( !class_exists( 'PF_Affiliate_Quarter' ) ) {

class PF_Affiliate_Quarter extends PF_Affiliate {
    
    //define
    var $quarter;
    var $start_date;
    var $end_date;
    var $deposit_date;
    
    //construct
    function __construct ( $quarter, $start_date, $end_date, $deposit_date ) {	
        $this->quarter = $quarter;		
        $this->start_date = $start_date;		
        $this->end_date = $end_date;		
        $this->deposit_date = $deposit_date;		
    }
        
    /**
     * Returns start date
     */
    public function get_quarter_start_date(){

        return ( date('M d', $this->start_date ) );
    }
    //add_shortcode( 'quarter-start', 'get_quarter_start_date' ); 

    
}//end class
}//end if class exists