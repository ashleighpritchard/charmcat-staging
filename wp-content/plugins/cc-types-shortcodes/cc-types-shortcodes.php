<?php
/**
 * Plugin Name: CharmCat's Types Shortcodes - Customizing Types!
 * Description: All the shortcodes I've created custom for using with Types as well as Types-related functions.
 * Version: 1.0
 * Author: CharmCat
 * Author URI: http://charmcat.net/
 * Developer: Ashleigh Pritchard
 * *
 * Copyright: © 2016 CharmCat Stationery & Design LLC
 */

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}


//**********FUNCTIONS**********//

//
//Large Joins
//Eliminates problems with toolset saving large edits
//
add_action('init', 'toolset_allow_large_joins');
function toolset_allow_large_joins(){
        global $wpdb;
        $wpdb->query('SET SQL_BIG_SELECTS=1');
}

//
//Remove Views Pagination CSS
//
add_filter('wp_enqueue_scripts', 'my_cleanup_scripts', 11);
function my_cleanup_scripts() {
	wp_dequeue_style( 'views-pagination-style' );
}

//
//Function for returning ancestors of the current product
//
if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
	function post_is_in_descendant_category( $cats, $_post = null ) {
        foreach ( (array) $cats as $cat ) {
            if (!absint($cat)) {
              $cat = get_term_by('slug',$cat,'product_cat'); // option two
              if(!$cat) return false;
              $cat = $cat->term_id;
            }
            // get_term_children() accepts integer ID only
            $descendants = get_term_children( (int) $cat, 'product_cat' );
            if ( $descendants && in_category( $descendants, $_post ) )
                return true;
        }
        return false;
	}
}


/* For taxonomies */
if ( ! function_exists( 'post_is_in_descendant_taxonomy' ) ) :
function post_is_in_descendant_taxonomy( $terms, $taxonomy, $_post = null ) {
	foreach ( (array) $terms as $term ) {
		// get_term_children() accepts integer ID only
		$descendants = get_term_children( (int) $term, $taxonomy );
		if ( $descendants && has_term( $descendants, $taxonomy, $_post ) )
			return true;
	}
	return false;
}
function post_is_in_taxonomy_slug($slug, $taxonomy){
	global $post;
	$post_id = ( isset($post->ID) ) ? $post->ID : '';
	if ( has_term( $slug, $taxonomy, $post_id ) || post_is_in_descendant_taxonomy( get_term_by('slug',$slug,$taxonomy), $taxonomy, $post_id ) ){
		return true;
	} else {
		return false;
	}
}
endif; //function_exists 'post_is_in_descendant_taxonomy'



//**********SHORTCODES**********//

//
//Shortcode for logout url
//
function cc_logout_url_shortcode() {
	return wp_logout_url( home_url() );
}
add_shortcode( 'logout-url', 'cc_logout_url_shortcode' );


//
//Shortcode for current time
//
function cc_current_time(){
	return current_time( 'timestamp' );  
}
add_shortcode( 'current-time', 'cc_current_time' );


//
//This shortcode returns the value of any url parameter
//
add_shortcode( 'wpv-url-param', 'wpv_post_url_shortcode' );
function wpv_post_url_shortcode( $atts ) {
  if ( !empty( $atts['var'] ) ) {
    $var = (array)$_GET[$atts['var']];
    return esc_html( implode( ', ', $var ) );
  }
}


//
//Shortcode attribute for custom URL into View
//
add_shortcode('wpv-view-shortcode-attribute', 'wpv_view_shortcode_attribute');
function wpv_view_shortcode_attribute($atts) {
    extract(  shortcode_atts( array(
		'attr' => 'seemore',
	), $atts ));
	global $WP_Views;
    if(isset($WP_Views->view_shortcode_attributes[0][$attr]))
	return $WP_Views->view_shortcode_attributes[0][$attr];
}

//
//Shortcode for woocommerce product type
//
function cc_product_type(){
	
	global $post;
	if( function_exists('get_product') ){
		//get product id
		$product = get_product( $post->ID );

		//get product type
		$product_type = $product->get_type();

	}
	
	return $product_type;
}
add_shortcode( 'cc-product-type', 'cc_product_type' );


//
// united states states shortcode for generic fields
//
function cc_allowed_states_us(){
    $output = '{"value":"AL","label":"Alabama"},{"value":"AK","label":"Alaska"},{"value":"AZ","label":"Arizona"},{"value":"AR","label":"Arkansas"},{"value":"CA","label":"California"},{"value":"CO","label":"Colorado"},{"value":"CT","label":"Connecticut"},{"value":"DE","label":"Delaware"},{"value":"DC","label":"District of Columbia"},{"value":"FL","label":"Florida"},{"value":"GA","label":"Georgia"},{"value":"HI","label":"Hawaii"},{"value":"ID","label":"Idaho"},{"value":"IL","label":"Illinois"},{"value":"IN","label":"Indiana"},{"value":"IA","label":"Iowa"},{"value":"KS","label":"Kansas"},{"value":"KY","label":"Kentucky"},{"value":"LA","label":"Louisiana"},{"value":"ME","label":"Maine"},{"value":"MD","label":"Maryland"},{"value":"MA","label":"Massachusetts"},{"value":"MI","label":"Michigan"},{"value":"MN","label":"Minnesota"},{"value":"MS","label":"Mississippi"},{"value":"MO","label":"Missouri"},{"value":"MT","label":"Montana"},{"value":"NE","label":"Nebraska"},{"value":"NV","label":"Nevada"},{"value":"NH","label":"New Hampshire"},{"value":"NJ","label":"New Jersey"},{"value":"NM","label":"New Mexico"},{"value":"NY","label":"New York"},{"value":"NC","label":"North Carolina"},{"value":"ND","label":"North Dakota"},{"value":"OH","label":"Ohio"},{"value":"OK","label":"Oklahoma"},{"value":"OR","label":"Oregon"},{"value":"PA","label":"Pennsylvania"},{"value":"RI","label":"Rhode Island"},{"value":"SC","label":"South Carolina"},{"value":"SD","label":"South Dakota"},{"value":"TN","label":"Tennessee"},{"value":"TX","label":"Texas"},{"value":"UT","label":"Utah"},{"value":"VT","label":"Vermont"},{"value":"VA","label":"Virginia"},{"value":"WA","label":"Washinton"},{"value":"WV","label":"West Virginia"},{"value":"WI","label":"Wisconsin"},{"value":"WY","label":"Wyoming"}';
        
    return $output;  
}
add_shortcode( 'allowed-states-us', 'cc_allowed_states_us' );