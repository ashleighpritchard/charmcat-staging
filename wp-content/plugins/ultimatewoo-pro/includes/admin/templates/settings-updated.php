<?php
/**
 *	Settings Updated notice
 *
 *	@package UltimateWoo Pro
 *	@author UltimateWoo
 *	@since 1.0
 */
?>

<div class="notice updated">
	<p><?php _e( 'Settings updated!', 'ultimatewoo-pro' ); ?></p>
</div>