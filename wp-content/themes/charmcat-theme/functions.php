<?php // Opening PHP tag - nothing should be before this, not even whitespace
	
	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

//Add Google Fonts
function wpb_add_google_fonts() {
    wp_register_style('wpb-googleFonts', 'http://fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700');
    wp_enqueue_style( 'wpb-googleFonts');
}
add_action('wp_print_styles', 'wpb_add_google_fonts');

//Disable Admin bar for non-admins
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

//try to fix woocommerce 404 pages
add_action( 'wp_enqueue_scripts', 'custom_frontend_scripts' );function custom_frontend_scripts() {global $post, $woocommerce;

$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? : '.min';
wp_deregister_script( 'jquery-cookie' );
wp_register_script( 'jquery-cookie', $woocommerce->plugin_url() . '/assets/js/jquery-cookie/jquery_cookie' . $suffix . '.js', array( 'jquery' ), '1.3.1', true );

}

//Affiliate disclaimer shortcode function
function cc_affiliate_disclaimer_block(){
		
	$output = '<div class="row affiliate" style="margin:30px;"><div class="col-sm-12" style="padding:15px;"><span class="fa-stack fa-2x pull-left" style="margin-right:15px;"><i class="fa fa-certificate fa-stack-2x"></i><i class="fa fa-paw fa-stack-1x"></i></span><span style="font-weight:600;font-style:italic;">This post features affiliate links, which means that if you purchase something, you will be contributing to my cat treat fund! There\'s no cost to you, and I only recommend products or companies that I personally approve of.</span></div></div>';
    
    return $output;
    
} 
add_shortcode( 'cc-affiliate-disclaimer', 'cc_affiliate_disclaimer_func' );
function cc_affiliate_disclaimer_func(){
    $output = cc_affiliate_disclaimer_block();

    return $output;
}

//Badges!
function cc_referral_badges(){

    $output = '<div class="text-center badges">
        <span class="review-badge"><a href="http://vendors.offbeatbride.com"><img src="http://media.offbeatempire.com/wp-content/blogs.dir/7/files/2015/08/vendor-badge-illo-pink1.png" alt="" height="127px" width="auto"/></a></span>
        <span class="review-badge"><script src="//www.weddingwire.com/assets/vendor/widgets/ww-rated-2013.js" type="application/javascript"></script><div id="ww-widget-wwrated-2013"><a class="ww-top" href="http://www.weddingwire.com" target="_blank" title="Weddings, Wedding, Wedding Venues"></a><a class="ww-bottom" href="http://www.weddingwire.com/reviews/paper-by-charmcat-frankford/f833bfd0a65ddd82.html" target="_blank" title="Paper by CharmCat Reviews, Invitations"></a></div><script>  WeddingWire.ensureInit(function() {WeddingWire.createWWRated2013({"vendorId":"f833bfd0a65ddd82" }); });</script></span>
        <span class="review-badge"><a href="http://gayweddings.com/vendors/?e-detail=1&vid=f833bfd0a65ddd82" alt="Click to view our profile on Gay Weddings."><img src="/wp-content/uploads/pros-badge-e1427004997746.gif"></a></span>
        <span class="review-badge"><a href="http://beta.theknot.com/marketplace/charmcat-frankford-de-638363?utm_source=vendor_website&utm_medium=banner&utm_term=1a93def7-0bb3-4a3c-bda8-a32501568902&utm_campaign=vendor_badge_assets"><img src="//www.xoedge.com/myaccount/2012/grab-a-badge/as-seen-in/TK_lg.png" width="125" height="125" alt="The Knot | TheKnot.com" border="0"></a></span>
        <span class="review-badge"><a href="http://www.bespoke-bride.com/?s=charmcat"><img src="/wp-content/uploads/bespoke-bride-badge.png" alt="Bespoke Bride Blog" border="0"></a></span>
        <span class="review-badge"><a href="http://www.lovesickexpo.com/newyork/"><img src="/wp-content/uploads/LovesickVendor2016-small.png" alt="Lovesick Expo 2016 in New York City" border="0"></a></span></div>';
        
    return $output;
    
}
add_shortcode( 'cc-referral-badges', 'cc_referral_badges_func' );
function cc_referral_badges_func(){
    $output = cc_referral_badges();

    return $output;
}