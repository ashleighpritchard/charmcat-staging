<?php
/**
 *	Form for managing license
 *
 *	@package UltimateWoo Pro
 *	@author UltimateWoo
 *	@since 1.0
 */
?>

<?php 

// License variables
$license_key = isset( $ultimatewoo_options['license']['license_key'] ) ? $ultimatewoo_options['license']['license_key'] : '';
$site_status = isset( $ultimatewoo_options['license']['site_status'] ) ? $ultimatewoo_options['license']['site_status'] : '';
$license_exp_date = isset( $ultimatewoo_options['license']['license_exp_date'] ) ? $ultimatewoo_options['license']['license_exp_date'] : '';
$license_limit = isset( $ultimatewoo_options['license']['license_limit'] ) ? $ultimatewoo_options['license']['license_limit'] : '';
$activations_left = isset( $ultimatewoo_options['license']['activations_left'] ) ? $ultimatewoo_options['license']['activations_left'] : '';
$db_version = isset( $ultimatewoo_options['license']['db_version'] ) ? $ultimatewoo_options['license']['db_version'] : '';

?>

<div class="postbox">

	<div class="handlediv" title="Click to toggle"><br></div>

	<h2 class="hndle"><?php esc_attr_e( 'License Key', 'ultimatewoo-pro' ); ?></h2>

	<div class="inside">
		
		<?php if ( $site_status != 'valid' ) : ?>
			<h4><?php _e( 'Instructions on Activating Your License', 'ultimatewoo-pro' ); ?></h4>
		<?php else : ?>
			<h4><?php _e( 'Instructions on Deactivating Your License', 'ultimatewoo-pro' ); ?></h4>
		<?php endif; ?>

		<ol>
			<?php if ( $site_status != 'valid' ) : ?>
				<li><?php _e( 'Retrieve and enter your license key in the field below.', 'ultimatewoo-pro' ); ?></li>
				<li><?php _e( 'Click "Activate License" to save and activate your license key.', 'ultimatewoo-pro' ); ?></li>
			<?php else : ?>
				<li><?php _e( 'Simply click the "Deactivate License" button.', 'ultimatewoo-pro' ); ?></li>
			<?php endif; ?>
		</ol>

		<form action="<?php echo add_query_arg( 'ultimatewoo-settings', 'license', ULTIMATEWOO_SETTINGS_PAGE_URL ); ?>" method="post" id="ultimatewoo-license-form">
			<input type="text" class="regular-text" name="ultimatewoo[license][license_key]" id="ultimatewoo[license][license_key]" value="<?php echo $license_key; ?>" <?php if ( $site_status == 'valid' ) echo 'readonly'; ?>>
			<input type="hidden" name="ultimatewoo[license][site_status]" id="ultimatewoo[license][site_status]" value="<?php echo $site_status; ?>">
			<input type="hidden" name="ultimatewoo[license][license_exp_date]" id="ultimatewoo[license][license_exp_date]" value="<?php echo $license_exp_date; ?>">
			<input type="hidden" name="ultimatewoo[license][license_limit]" id="ultimatewoo[license][license_limit]" value="<?php echo $license_limit; ?>">
			<input type="hidden" name="ultimatewoo[license][activations_left]" id="ultimatewoo[license][activations_left]" value="<?php echo $activations_left; ?>">
			
			<?php // Data for selecting action to take on license ?>
			<?php if ( $site_status != 'valid' ) : ?>
				<input type="hidden" name="activate-license" id="activate-license" value="1">
			<?php else : ?>
				<input type="hidden" name="deactivate-license" id="deactivate-license" value="1">
			<?php endif; ?>
			
			<?php wp_nonce_field( 'ultimatewoo_license_nonce', 'ultimatewoo_license_nonce' ); ?>
			
			<?php // Site is not active, so output activate button ?>
			<?php if ( $site_status != 'valid' ) : ?>
				<input type="submit" class="button-primary ultimatewoo-button" value="<?php _e( 'Activate License', 'ultimatewoo-pro' ); ?>">
			<?php else : ?>
				<input type="submit" class="button-primary ultimatewoo-button deactivate" value="<?php _e( 'Deactivate License', 'ultimatewoo-pro' ); ?>">
			<?php endif; ?>
		</form>

		<?php do_action( 'ultimatewoo_settings_page_after_license_form' ); ?>

	</div><!-- .inside -->

</div><!-- .postbox -->