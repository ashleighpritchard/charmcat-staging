<?php

/**
 * Plugin Name: WooCommerce Wedding Client Manager — Affiliates Module
 * Description: Manage referrals and rewards from other wedding professionals through any WooCommerce-powered website.
 * Version: 0.2
 * Author: CharmCat
 * Author URI: http://charmcat.net/
 * Developer: Ashleigh Pritchard
 * *
 * Copyright: © 2016 CharmCat Stationery & Design LLC
 */

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

require_once("lib/wwcm-affiliates-coupons.php");//Hack
require_once("lib/class-wwcm-affiliates-info.php");//PF_Affiliate class
require_once("lib/class-wwcm-affiliates-financial-quarters.php");//PF_Affiliate_Quarter extends PF_Affiliate
require_once("lib/class-wwcm-affiliates-totals.php");//PF_Affiliate_Totals extends PF_Affiliate
require_once("wwcm-affiliates-myaccount-endpoint.php");//Endpoint for my account
require_once("lib/class-wwcm-affiliates-shortcodes.php");//shortcodes