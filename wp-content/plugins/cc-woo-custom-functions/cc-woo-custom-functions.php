<?php
/**
 * Plugin Name: CharmCat's Woocommerce Tweaks - Customizing Woo!
 * Description: Functions that alter the functionality of Woocommerce.
 * Version: 1.0
 * Author: CharmCat
 * Author URI: http://charmcat.net/
 * Developer: Ashleigh Pritchard
 * *
 * Copyright: © 2016 CharmCat Stationery & Design LLC
 */

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}


//**********VIEWS-RELATED**********//

//
//Woocommerce invoice pay url
//Creates a shortcode for including the Pay Now url anywhere
//
function cc_pay_invoice_url(){
    
    global $woocommerce;
    
    $order_id = get_the_ID();   //get current post id
    $order_details = wc_get_order( $order_id );   //get order object
    
    $pay_link = $order_details->get_checkout_payment_url();    //get pay link
    
    $output = esc_url( $pay_link );    //generate pay link
    
    return $output;

}

add_shortcode( 'cc-woo-pay-invoice-url', 'cc_pay_invoice_url' );
function cc_pay_invoice_url_func(){
    $output = cc_pay_invoice_url();

    return $output;
}

//
//Woocommerce itemized invoice
//Creates a shortcode that shows the body of a woocommerce order
//
add_shortcode( 'cc-woo-order-itemized', 'cc_order_itemized_func' );
function cc_order_itemized_func(){

    $output = '';

    global $woocommerce;

    $order_id = get_the_ID();   //get current post id
    $order = wc_get_order( $order_id );
    
    $output = '<table class="shop_table order_details table table-condensed"><tbody>'; //open table
    
    $items = $order->get_items();
        foreach ( $items as $item ) {
            $product_id = $item['product_id'];
            $product_name = $item['name'];
            
            $loop = 0;
            $i = 0;//set everything to 0 to start

            $output .= '<tr class="'; //open row
            
            $output .=  esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order_details ) );//add class to row

            $output .= '"><td class="product-name">';//open product name cell
    
            $output .= sprintf( '<a href="%s">%s</a>', get_permalink( $item['product_id'] ), $item['name'] );//item name

            $output .= apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );//item quantity

            $output .= '</td><td class="product-total">';//close product name and open product total
            
        
            $product_subtotal = $order->get_formatted_line_subtotal( $item ); //retrieve product subtotal
    
            $output .= $product_subtotal;//show product subtotal
    
            $output .= '</td></tr>';//close item row
        
            $loop++;
            $i++;//and iterate
        }
    
if ( $show_purchase_note && $purchase_note ) :
    $output .= '<tr class="product-purchase-note"><td colspan="3">';
    $output .= wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) );//show purchase note
    $output .= '</td></tr>';
endif;
    
    $output .= '</tbody><tfoot>';//begin footer
    
	foreach ( $order->get_order_item_totals() as $key => $total ) {//get totals
    
        $loop = 0;
        $i = 0;//set everything to 0 to start
                
        $output .= sprintf( '<tr><th scope="row">%s</th><td>%s</td></tr>', $total["label"], $total["value"], $total[$i] );//output total

        $loop++;
        $i++;//and iterate for each total
    }
    
    $output .= '</tfoot></table>';//close table
    
    return $output;

}

//
//Woocommerce invoice order total
//Creates a shortcode that shows the total of the order
//
function cc_order_total(){
    
    global $woocommerce;
    
    $order_id = get_the_ID();   //get current post id
    $order = wc_get_order( $order_id );   //get order object
    $total = $order->get_formatted_order_total();  //get order total for post id
    
	$output = $total;
    
    return $output;

}
add_shortcode( 'cc-woo-order-total', 'cc_order_total_func' );
function cc_order_total_func(){
    $output = cc_order_total();

    return $output;
}



//**********TYPES-RELATED**********//

//
//Edit WooCommerce orders to use with Views
//
add_action('init', 'wc_orders_public');
function wc_orders_public() {
        global $wp_post_types;
        $wp_post_types['shop_order']->public = true;
        $wp_post_types['shop_order']->publicly_queryable = true;
        $wp_post_types['shop_order']->hierarchical = true;
}

//make all orders editable
add_filter( 'wc_order_is_editable', '__return_true' );

//
//Edit WooCommerce attributes/taxonomy to use with Views
//
add_action('init', 'edit_product_attribute');
function edit_product_attribute() {
    global $wp_taxonomies;
        $wp_taxonomies['pa_custom-color']->public = true;
        $wp_taxonomies['pa_custom-color']->publicly_queryable = true;
        $wp_taxonomies['pa_custom-color']->hierarchical = true;
        $wp_taxonomies['pa_custom-color']->show_ui = true;
        $wp_taxonomies['pa_custom-color']->show_in_menu = true;
        $wp_taxonomies['pa_custom-color']->meta_box_cb = 'post_categories_meta_box'; 
        $wp_taxonomies['pa_custom-color']->show_admin_column  = true;
}
add_action( 'init', 'add_color_taxonomy_to_events', 200 );
function add_color_taxonomy_to_events() {
	register_taxonomy_for_object_type( 'pa_custom-color', 'event' );
	register_taxonomy_for_object_type( 'pa_custom-color', 'cc-colors' );
}



//**********THEME-RELATED**********//

//Remove Woo Breadcrumbs
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
//Remove WooCommerce default CSS
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );



//**********COMPOSITE PRODUCTS**********//


//restrict composite review section to three columns
add_filter( 'woocommerce_composite_component_summary_max_columns', 'wc_cp_summary_max_columns', 10, 2 );
function wc_cp_summary_max_columns( $cols, $composite ) {
	$cols = 3;
	return $cols;
}

//hide incompatible components in composite scenarios
add_filter( 'woocommerce_component_options_hide_incompatible', 'wc_cp_component_options_hide_incompatible', 10, 3 );
function wc_cp_component_options_hide_incompatible( $hide, $component_id, $composite ) {
	return true;
}

//hide “Free!” price strings from drop-downs and selected product details
add_filter( 'woocommerce_free_price_html', 'wc_cp_composited_product_free_price_html', 10, 2 );
function wc_cp_composited_product_free_price_html( $price, $product ) {
	if ( did_action( 'woocommerce_composite_products_apply_product_filters' ) > did_action( 'woocommerce_composite_products_remove_product_filters' ) ) {
		$price = ''; // wc_price( 0.0 );
	}
	return $price;
}
add_filter( 'woocommerce_composited_product_price_string_inner', 'wc_cp_composited_product_free_price_string_inner', 10, 10 );
function wc_cp_composited_product_free_price_string_inner( $price_string_inner, $price_string, $qty_suffix, $suffix, $price, $is_range, $has_multiple, $product_id, $component_id, $composited_product ) {
	if ( $price == 0 ) {
		$price_string_inner = ''; // sprintf( __( '%1$s %2$s %3$s', 'dropdown price followed by per unit suffix and discount suffix', 'woocommerce-composite-products' ), WC_CP()->api->get_composited_item_price_string_price( $price ), $qty_suffix, $suffix );
	}
	return $price_string_inner;
}

//hide all line items associated with Components from the cart 
add_filter( 'woocommerce_order_item_visible', 'wc_cp_order_item_visible', 10, 2 );
add_filter( 'woocommerce_widget_cart_item_visible', 'wc_cp_cart_item_visible', 10, 3 );
add_filter( 'woocommerce_cart_item_visible', 'wc_cp_cart_item_visible' , 10, 3 );
add_filter( 'woocommerce_checkout_cart_item_visible', 'wc_cp_cart_item_visible', 10, 3 );
/**
 * Visibility of components in cart.
 *
 * @param  boolean $visible
 * @param  array   $cart_item
 * @param  string  $cart_item_key
 * @return boolean
 */
function wc_cp_cart_item_visible( $visible, $cart_item, $cart_item_key ) {
    if ( ! empty( $cart_item[ 'composite_parent' ] ) ) {
        $visible = false;
    }
    return $visible;
}


//**********BUNDLED PRODUCTS**********//


//remove the "- optional" suffix displayed next to the title of optional bundled items 
add_filter( 'woocommerce_bundles_optional_bundled_item_suffix', 'wc_pb_remove_optional_suffix', 10, 3 );
function wc_pb_remove_optional_suffix( $suffix, $bundled_item, $bundle ) {
	return '';
}


//end of plugin
?>