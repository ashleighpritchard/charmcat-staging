<?php
/**
 * Shortcodes
 *
 * Creates shortcodes for ProFete Affiliates module.
 *
 * @version   0.1
 * @author    CharmCat
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

class PF_Affiliate_Shortcodes {

	/**
	 * Add shortcodes.
	 */
    public function __construct() {
		add_shortcode( 'pf-affiliate-lifetime-balance', array($this, 'get_affiliate_lifetime_balance') );
    }
    
    /**
	 * Affiliate lifetime balance.
	 */
	public function get_affiliate_lifetime_balance(){
        $earned_amount_total_formatted = PF_Affiliate_Totals::get_affiliate_lifetime_earned_total();
        
        return $earned_amount_total_formatted;
    }
    
}
$affiliate_shortcodes = new PF_Affiliate_Shortcodes();