<?php
/**
 * Plugin Name: CharmCat's Owl Plugin - Customizing Carousels!
 * Description: Adds Owl Carousel 2.0.0 Beta 3 files and creates the Woo Owl Carousel.
 * Version: 1.2
 * Author: CharmCat
 * Author URI: http://charmcat.net/
 * Developer: Ashleigh Pritchard
 * *
 * Uses Owl Carousel 2.0.0 Beta 3
 * Copyright: © 2016 CharmCat Stationery & Design LLC
 */

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}


//**********INITIALIZE**********//
//Add the OWL 2 Plugin
function add_owl_plugin() {
	wp_enqueue_style('owl-carousel-css', '/wp-content/plugins/cc-owl-2-plugin/assets/owl.carousel.min.css');
	wp_enqueue_style('cc-owl-carousel-css', '/wp-content/plugins/cc-owl-2-plugin/assets/cc.owl.carousel.css');
    wp_enqueue_script( 'owl-carousel', '/wp-content/plugins/cc-owl-2-plugin/owl.carousel.min.js', array('jquery'));	
}
add_action('wp_enqueue_scripts', 'add_owl_plugin');



//**********Shortcode for Owl Gallery on Woocommerce Products**********//

//OWL Variable Product Gallery - variable products with only one main image
function owl_woo_main_product_image(){
	
	$output = '';

	global $post, $woocommerce, $product;

	$output .= '<div id="gallery" class="images owl-carousel">';
    
    $attachment_ids = $product->get_gallery_attachment_ids();//Get image IDs
    if ( $attachment_ids ) {//Execute if there are image attachments
        
    foreach ( $attachment_ids as $attachment_id ) {//for each image attached

        $image_link     = wp_get_attachment_url( $attachment_id );//img link
        $image_title    = esc_attr( get_the_title( $attachment_id ) );//Alt text
        
        
        $loop = 0;
        $i = 0;//set everything to 0 to start
        
        $output .=  sprintf( '<div class="item"><a href="" itemprop="image" class="woocommerce-main-image zoom" title="%2$s"><img src="%1$s" alt="%2$s" data-index="%3$s"/></a></div>', $image_link, $image_title, $i );//output html
        
        $loop++;
        $i++;//and iterate
        }

    } else {//if there are no attachments

        $output .=  apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );
			
    }
	
	$output .= '</div>';//end div
			
	return $output;

}
add_shortcode( 'owl-woo-product-image', 'owl_woo_product_image_func' );
function owl_woo_product_image_func(){
    $output = owl_woo_main_product_image();

    return $output;
}

?>