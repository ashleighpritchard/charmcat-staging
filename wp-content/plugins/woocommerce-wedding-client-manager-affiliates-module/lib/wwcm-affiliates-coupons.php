<?php
/**
 * Coupons
 *
 * Makes coupons publicly queryable for Types access. Adds an author box for assigning the code to an affiliate member.
 *
 * @version   0.1
 * @author    CharmCat
 */
add_action('init', 'wc_coupons_public');
function wc_coupons_public() {
    global $wp_post_types;
    $wp_post_types['shop_coupon']->public = true;
    $wp_post_types['shop_coupon']->publicly_queryable = true;
    
    add_post_type_support( 'shop_coupon', 'author' );
}