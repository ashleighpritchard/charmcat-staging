<?php
 /**
  * Copyright 2016 75nineteen Media LLC.  (email : scott@75nineteen.com)
  * 
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

/** Path and URL constants **/
define( 'FUE_VERSION', '4.4.13' );
define( 'FUE_KEY', 'aHR0cDovLzc1bmluZXRlZW4uY29tL2Z1ZS5waH' );
define( 'FUE_FILE', __FILE__ );
define( 'FUE_URL', ULTIMATEWOO_MODULES_URL . '/follow-up-emails' );
define( 'FUE_DIR', ULTIMATEWOO_MODULES_DIR . '/follow-up-emails' );
define( 'FUE_INC_DIR', FUE_DIR .'/includes' );
define( 'FUE_INC_URL', FUE_URL .'/includes' );
define( 'FUE_ADDONS_DIR', FUE_DIR .'/addons' );
define( 'FUE_ADDONS_URL', FUE_URL .'/addons' );
define( 'FUE_TEMPLATES_DIR', FUE_DIR .'/templates' );
define( 'FUE_TEMPLATES_URL', FUE_URL .'/templates' );

global $fue, $wpdb;
require_once FUE_INC_DIR .'/class-follow-up-emails.php';
$fue = new Follow_Up_Emails( $wpdb );

//4.4.13