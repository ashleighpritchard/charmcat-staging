<?php
// Direct access security
if ( !defined( 'TM_FP_PLUGIN_SECURITY' ) ) {
	die();
}

/**
 * Custom Final Price Class
 *
 * This class is responsible for displaying the Final Price on the frontend.
 */

class TM_Final_Price {

	var $version        = '1.3.0';
	var $_namespace     = 'tm-final-price';
	var $plugin_path;
	var $template_path;
	var $plugin_url;

	public function __construct() {
		$this->plugin_path      = untrailingslashit( plugin_dir_path(  __FILE__  ) );
		$this->template_path    = $this->plugin_path.'/templates/';
		$this->plugin_url       = untrailingslashit( plugins_url( '/', __FILE__ ) );

		/**
		 * Load js,css files
		 */
		add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts' ) );

		/**
		 * Display in frontend
		 */
		add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'frontend_display' ), 50 );
	}

	public function frontend_display() {
		global $product;
		$this->print_price_fields( $product );
	}

	

	public function frontend_scripts() {
		if ( !is_product() ) {
			return;
		}
		$product = get_product();

		wp_enqueue_style( 'tm-final-price-css', $this->plugin_url . '/css/tm-final-price.css' );

		wp_register_script( 'accounting', $this->plugin_url . '/js/accounting.min.js', '', '0.3.2' );
		wp_enqueue_script( 'tm-final-price', $this->plugin_url. '/js/tm-final-price.js', array( 'jquery', 'accounting' ), $this->version, true );
		$extra_fee=0;
		$args = array(
			'extra_fee' 				=> apply_filters( 'woocommerce_tm_final_price_extra_fee', $extra_fee,$product ),
			'i18n_final_total'             => __( 'Final total:', TM_FP_TRANSLATION ),
			'currency_format_num_decimals' => absint( get_option( 'woocommerce_price_num_decimals' ) ),
			'currency_format_symbol'       => get_woocommerce_currency_symbol(),
			'currency_format_decimal_sep'  => esc_attr( stripslashes( get_option( 'woocommerce_price_decimal_sep' ) ) ),
			'currency_format_thousand_sep' => esc_attr( stripslashes( get_option( 'woocommerce_price_thousand_sep' ) ) ),
			'currency_format'              => esc_attr( str_replace( array( '%1$s', '%2$s' ), array( '%s', '%v' ), get_woocommerce_price_format() ) )
		);
		wp_localize_script( 'tm-final-price', 'tm_final_price_js', $args );
	}

	// get WooCommerce Dynamic Pricing & Discounts price for options
	// modified from get version from Pricing class
	private function get_RP_WCDPD_single($field_price,$cart_item_key,$pricing){
		if ($this->tm_epo_dpd_enable = 'no' || !isset($pricing->items[$cart_item_key])) {
			return $field_price;
		}

            $price = $field_price;
            $original_price = $price;

            if (in_array($pricing->pricing_settings['apply_multiple'], array('all', 'first'))) {
                foreach ($pricing->apply['global'] as $rule_key => $apply) {
                    if ($deduction = $pricing->apply_rule_to_item($rule_key, $apply, $cart_item_key, $pricing->items[$cart_item_key], false, $price)) {

                        if ($apply['if_matched'] == 'other' && isset($pricing->applied) && isset($pricing->applied['global'])) {
                            if (count($pricing->applied['global']) > 1 || !isset($pricing->applied['global'][$rule_key])) {
                                continue;
                            }
                        }

                        $pricing->applied['global'][$rule_key] = 1;
                        $price = $price - $deduction;
                    }
                }
            }
            else if ($pricing->pricing_settings['apply_multiple'] == 'biggest') {

                $price_deductions = array();

                foreach ($pricing->apply['global'] as $rule_key => $apply) {

                    if ($apply['if_matched'] == 'other' && isset($pricing->applied) && isset($pricing->applied['global'])) {
                        if (count($pricing->applied['global']) > 1 || !isset($pricing->applied['global'][$rule_key])) {
                            continue;
                        }
                    }

                    if ($deduction = $pricing->apply_rule_to_item($rule_key, $apply, $cart_item_key, $pricing->items[$cart_item_key], false)) {
                        $price_deductions[$rule_key] = $deduction;
                    }
                }

                if (!empty($price_deductions)) {
                    $max_deduction = max($price_deductions);
                    $rule_key = array_search($max_deduction, $price_deductions);
                    $pricing->applied['global'][$rule_key] = 1;
                    $price = $price - $max_deduction;
                }

            }

            // Make sure price is not negative
            // $price = ($price < 0) ? 0 : $price;

            if ($price != $original_price) {
                return $price;
            }
            else {
                return $field_price;
            }
	}

	// get WooCommerce Dynamic Pricing & Discounts price rules
	private function get_RP_WCDPD($product,$field_price=null,$cart_item_key=null){
		$price = null;
				
		if(class_exists('RP_WCDPD') && !empty($GLOBALS['RP_WCDPD'])){
			
			$tm_RP_WCDPD=$GLOBALS['RP_WCDPD'];

			$selected_rule = null;

			if ($field_price!==null && $cart_item_key!==null){

	            return $this->get_RP_WCDPD_single($field_price,$cart_item_key,$tm_RP_WCDPD->pricing);
	        }

			// Iterate over pricing rules and use the first one that has this product in conditions (or does not have if condition "not in list")
			if (isset($tm_RP_WCDPD->opt['pricing']['sets']) 
				&& count($tm_RP_WCDPD->opt['pricing']['sets']) ) {
				foreach ($tm_RP_WCDPD->opt['pricing']['sets'] as $rule_key => $rule) {
					if ($rule['method'] == 'quantity' && $validated_rule = RP_WCDPD_Pricing::validate_rule($rule)) {
						if ($validated_rule['selection_method'] == 'all' && $tm_RP_WCDPD->user_matches_rule($validated_rule['user_method'], $validated_rule['roles'])) {
							$selected_rule = $validated_rule;
							break;
						}
						if ($validated_rule['selection_method'] == 'categories_include' && count(array_intersect($tm_RP_WCDPD->get_product_categories($product->id), $validated_rule['categories'])) > 0 && $tm_RP_WCDPD->user_matches_rule($validated_rule['user_method'], $validated_rule['roles'])) {
							$selected_rule = $validated_rule;
							break;
						}
						if ($validated_rule['selection_method'] == 'categories_exclude' && count(array_intersect($tm_RP_WCDPD->get_product_categories($product->id), $validated_rule['categories'])) == 0 && $tm_RP_WCDPD->user_matches_rule($validated_rule['user_method'], $validated_rule['roles'])) {
							$selected_rule = $validated_rule;
							break;
						}
						if ($validated_rule['selection_method'] == 'products_include' && in_array($product->id, $validated_rule['products']) && $tm_RP_WCDPD->user_matches_rule($validated_rule['user_method'], $validated_rule['roles'])) {
							$selected_rule = $validated_rule;
							break;
						}
						if ($validated_rule['selection_method'] == 'products_exclude' && !in_array($product->id, $validated_rule['products']) && $tm_RP_WCDPD->user_matches_rule($validated_rule['user_method'], $validated_rule['roles'])) {
							$selected_rule = $validated_rule;
							break;
						}
					}
				}
			}
			
			if (is_array($selected_rule)) {

	            // Quantity
	            if ($selected_rule['method'] == 'quantity' && isset($selected_rule['pricing']) && in_array($selected_rule['quantities_based_on'], array('exclusive_product','exclusive_variation','exclusive_configuration')) ) {

	            	

		                if ($product->product_type == 'variable') {
		                    $product_variations = $product->get_available_variations();
		                }

		                // For variable products only - check if prices differ for different variations
		                $multiprice_variable_product = false;

		                if ($product->product_type == 'variable' && !empty($product_variations)) {
		                    $last_product_variation = array_slice($product_variations, -1);
		                    $last_product_variation_object = new WC_Product_Variable($last_product_variation[0]['variation_id']);
		                    $last_product_variation_price = $last_product_variation_object->get_price();

		                    foreach ($product_variations as $variation) {
		                        $variation_object = new WC_Product_Variable($variation['variation_id']);

		                        if ($variation_object->get_price() != $last_product_variation_price) {
		                            $multiprice_variable_product = true;
		                        }
		                    }
		                }

		                if ($multiprice_variable_product) {
		                    $variation_table_data = array();

		                    foreach ($product_variations as $variation) {
		                        $variation_product = new WC_Product_Variation($variation['variation_id']);
		                        $variation_table_data[$variation['variation_id']] = $tm_RP_WCDPD->pricing_table_calculate_adjusted_prices($selected_rule['pricing'], $variation_product->get_price());
		                    }
		                    $price=array();
		                    $price['is_multiprice']=true;
		                    $price['rules']=$variation_table_data;
		                }
		                else {
		                    if ($product->product_type == 'variable' && !empty($product_variations)) {
		                        $variation_product = new WC_Product_Variation($last_product_variation[0]['variation_id']);
		                        $table_data = $tm_RP_WCDPD->pricing_table_calculate_adjusted_prices($selected_rule['pricing'], $variation_product->get_price());
		                    }
		                    else {
		                        $table_data = $tm_RP_WCDPD->pricing_table_calculate_adjusted_prices($selected_rule['pricing'], $product->get_price());
		                    }
		                    $price=array();
		                    $price['is_multiprice']=false;
		                    $price['rules']=$table_data;
		                }
	            	

	            }

	        }
	    }
        if ($field_price!==null){
        	$price=$field_price;
        }
        return $price;
	}

	private function print_price_fields( $product ) {

		// WooCommerce Dynamic Pricing & Discounts
		if(class_exists('RP_WCDPD') && !empty($GLOBALS['RP_WCDPD'])){
			$price=$this->get_RP_WCDPD($product);
			if ($price){
				$price['product']=array();
				if ($price['is_multiprice']){
					foreach ($price['rules'] as $variation_id => $variation_rule) {
						foreach ($variation_rule as $rulekey => $pricerule) {
							$price['product'][$variation_id][]=array(
								"min"=>$pricerule["min"],
								"max"=>$pricerule["max"],
								"value"=>($pricerule["type"]!="percentage")?apply_filters( 'woocommerce_tm_epo_price', $pricerule["value"],""):$pricerule["value"],
								"type"=>$pricerule["type"]
								);
						}
					}
				}else{
					foreach ($price['rules'] as $rulekey => $pricerule) {
						$price['product'][0][]=array(
							"min"=>$pricerule["min"],
							"max"=>$pricerule["max"],
							"value"=>($pricerule["type"]!="percentage")?apply_filters( 'woocommerce_tm_epo_price', $pricerule["value"],""):$pricerule["value"],
							"type"=>$pricerule["type"]
							);
					}
				}
			}else{
				$price=array();
				$price['product']=array();
			}

			$price['price']=apply_filters( 'woocommerce_tm_epo_price', $product->get_price(),"");
            
		}else{

			if (class_exists('WC_Dynamic_Pricing')){
				$id = isset($product->variation_id) ? $product->variation_id : $product->id;
				$dp=WC_Dynamic_Pricing::instance();
				if ($dp && 
					is_object($dp) && property_exists($dp, "discounted_products") 
					&& isset($dp->discounted_products[$id]) ){
					$_price= $dp->discounted_products[$id];
				}else{
					$_price=$product->get_price();
				}

			}else{
				$_price=$product->get_price();
			}
			$price=array();
			$price['product']=array();
			$price['price']=apply_filters( 'woocommerce_tm_epo_price', $_price,"");
		}

		$variations = array();

		foreach ( $product->get_children() as $child_id ) {

			$variation = $product->get_child( $child_id );

			if ( ! $variation->exists() )
				continue;

			$variations[$child_id] = $variation->get_price();

		}

		global $woocommerce;
		$cart = $woocommerce->cart;
		$_tax  = new WC_Tax();
		$taxrates=$_tax->get_rates( $product->get_tax_class() );
		unset($_tax);
		$tax_rate=0;
		foreach ($taxrates as $key => $value) {
			$tax_rate=$tax_rate+floatval($value['rate']);
		}
		$taxable 			= $product->is_taxable();
		$tax_display_mode 	= get_option( 'woocommerce_tax_display_shop' );
		$tax_string 		= "";
		if ( $taxable ) {

			if ( $tax_display_mode == 'excl' ) {

				if ( $cart->tax_total > 0 && $cart->prices_include_tax ) {
					$tax_string = ' <small>' . WC()->countries->ex_tax_or_vat() . '</small>';
				}
		
			} else {

				if ( $cart->tax_total > 0 && !$cart->prices_include_tax ) {
					$tax_string = ' <small>' . WC()->countries->inc_tax_or_vat() . '</small>';
				}

			}

		}
		
		wc_get_template(
			'totals.php',
			array(
				'variations' 			=> esc_html(json_encode( (array) $variations ) ),
				'is_sold_individually' 	=> $product->is_sold_individually(),
				'type'  				=> esc_html( $product->product_type ),
				'price' 				=> esc_html( ( is_object( $product ) ? apply_filters( 'woocommerce_tm_final_price', $price['price'],$product ) : '' ) ),
				'taxable' 				=> $taxable,
				'tax_display_mode' 		=> $tax_display_mode,
				'prices_include_tax' 	=> $cart->prices_include_tax,
				'tax_rate' 				=> $tax_rate,
				'tax_string' 			=> $tax_string,
				'product_price_rules' 	=> esc_html(json_encode( (array) $price['product'] ) ),
			) ,
			$this->_namespace,
			$this->template_path
		);
	}

}
?>
