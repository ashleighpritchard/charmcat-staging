<?php
/*
Plugin Name: WooCommerce UPS Drop Shipping Pro
Plugin URI:  http://ignitewoo.com
Description: Provides UPS shipping rates. Capable of calculating a maximum box size and weight for all items in a cart to shipped in one package. (Commercial Software)
Version: 2.4.8
Author: <strong>IgniteWoo.com - Custom Add-ons for WooCommerce!</strong>
Author URI: http://ignitewoo.com

Copyright (c) 2012 - IgniteWoo.com -- ALL RIGHTS RESERVED

LICENSE: 
This is a single site license product. 
You must purchase a copy of this software from IgniteWoo.com in order to receive plugin updates/bug fixes and support. 

NOTE: 

Designers can use the following template tag to insert tracking numbers in their My Account template ( shortcode-my_account.php )


<?php insert_ups_drop_ship_track_ids( $order->id, $before = '<div>', $after = '</div>' ); ?>

Each tracking number is output in a HREF tag that links directly to the UPS tracking information page.
The link itself is wrapped in whatever you set for $before and $after. Defaults are shown above.


CHANGELOG: 

SEE THE README.txt FILE

*/ 

class IGN_UPS_Drop_Shipper { 

	var $settings = null;

	function __construct() { 
	
		add_action( 'woocommerce_shipping_init', array( &$this, 'init_ups_drop_shipping' ) );
		add_action( 'init', array( &$this, 'ups_drop_shipping_product_settings' ), 15 );
		add_action( 'init', array( &$this, 'ign_drop_ship_cache_buster' ), 50 );
		add_action( 'wp_head', array( &$this, 'ign_drop_ship_maybe_add_checkout_css' ), -1 );		
		add_filter( 'woocommerce_cart_shipping_packages', array( &$this, 'ign_ups_drop_ship_generate_packages' ), -9 );
		add_action( 'woocommerce_add_order_item_meta', array( &$this, 'ign_drop_ship_add_order_item_meta'), 1, 3 );

		// Order meta display filter
		add_filter( 'woocommerce_hidden_order_itemmeta', array( &$this, 'hide_order_meta' ), 9999, 1 );

		$this->settings = get_option( 'woocommerce_ups_drop_shipping_rate_settings' );
	}
	

	// Helper for Vendor Stores
	public static function get_settings() { 
	
		if ( empty( $this->settings ) ) 
			$this->settings = get_option( 'woocommerce_ups_drop_shipping_rate_settings' );
			
		return $this->settings;
	
	}
	
	function init_ups_drop_shipping() { 

		require_once( dirname(__FILE__) . '/ups_class.php' );
		
		add_filter('woocommerce_shipping_methods', array( &$this, 'add_ups_drop_shipping_rate_method' ), 10 );
		
		// order page meta box
		//add_action( 'add_meta_boxes', array( 'ups_drop_shipping_rate', 'add_order_meta_box' ), -1 );
	}

	
	function add_ups_drop_shipping_rate_method( $methods ) {

		$methods[] = 'ups_drop_shipping_rate'; 
		return $methods;
	}

	

	function ups_drop_shipping_product_settings() { 

		if ( !is_admin() )
			return;
			
		require_once( dirname( __FILE__ ) . '/ups_drop_shipping_product_settings.php' );

		require_once( dirname(__FILE__) . '/ups_shipping_class_settings.php' );
		
	}



	function ign_drop_ship_cache_buster() { 
		global $wpdb;

		if ( empty( $this->settings ) ) 
			$this->settings = get_option( 'woocommerce_ups_drop_shipping_rate_settings' );
		
		if ( empty( $this->settings['debug'] ) || 'yes' != $this->settings['debug'] )
			return;
			
		$sql = 'delete from ' . $wpdb->options . ' where option_name like "_transient_timeout_wc_ship%" or option_name like "_transient_wc_ship%" ';

		$wpdb->query( $sql );

	}


	function ign_drop_ship_maybe_add_checkout_css() { 

		if ( !function_exists('is_checkout' ) || !function_exists( 'is_cart') && ( !is_checkout() && !is_cart() ) )
			return;
		
		?>
		<style>
		.woocommerce table.shop_table td .woocommerce-shipping-contents small { font-size: 1.1em; font-weight: bold; }
		.woocommerce-cart .cart_totals  table td .woocommerce-shipping-contents small { font-size: 1.1em; font-weight: bold; }
		</style>

		<?php
		
	}


	function ign_ups_drop_ship_generate_packages( $packages ) {
		global $woocommerce;
		
		if ( empty( $this->settings ) ) 
			$this->settings = get_option( 'woocommerce_ups_drop_shipping_rate_settings' );

		if ( empty( $this->settings['enabled'] ) || 'yes' != $this->settings['enabled'] )
			return;
			
		if ( empty( $this->settings['shipping_style'] ) || 'multi_package' !== $this->settings['shipping_style'] || empty( $packages ) )
			return $packages;

		$packages = array();
		
		foreach ( $woocommerce->cart->get_cart() as $key => $item ) {

			if ( !$item['data']->needs_shipping() )
				continue;
			
			$item['shipping_item_key'] = $key;
			
			$packages[] = array(
				'contents' => array($item),
				'contents_cost' => array_sum( wp_list_pluck( array($item), 'line_total' ) ),
				'applied_coupons' => $woocommerce->cart->applied_coupons,
				'destination' => array(
					'country' => $woocommerce->customer->get_shipping_country(),
					'state' => $woocommerce->customer->get_shipping_state(),
					'postcode' => $woocommerce->customer->get_shipping_postcode(),
					'city' => $woocommerce->customer->get_shipping_city(),
					'address' => $woocommerce->customer->get_shipping_address(),
					'address_2' => $woocommerce->customer->get_shipping_address_2()
				)
			);
			
		}
		
		/** 
		
		Group items by vendor. Technically this may work, but the main class calculate_shipping() needs to 
		NOT use per_item style of calculation, instead it needs to pool items together into one package BEFORE
		the code below is implemented.
		
		In the cart/cart-shipping.php template use this to display the store label when grouping packages: 
		
		if ( !empty( $package['label'] ) ) {
			echo $package['label'];
		} else if ( $show_package_details ) {
			printf( __( 'Shipping #%d', 'woocommerce' ), $index + 1 );
		} else {
			_e( 'Shipping and Handling', 'woocommerce' );
		}

		$packages = array();
		*/
		
		/*************** FUTURE USE FOR GROUPING ITEMS INTO PACKAGES PER VENDOR ******
		$vendor_items = array();
		$store_items = array();
		
		// Arrange items based on seller 
		foreach ( WC()->cart->get_cart() as $item_key => $item ) {

			if ( !$item['data']->needs_shipping() )
				continue;
			
			$vendor = ign_get_product_vendors( $item['product_id'] );


			$item['shipping_item_key'] = $item_key;
			
			if ( empty( $vendor ) ) {
			
				$store_items[] = $item;
			
			} else {

				// Store this to avoid addition database lookups:
				$item['vendor_title'] = $vendor[0]->title;
				
				$vendor_items[ $vendor[0]->ID ][] = $item;
		
			}
		}
		

		if ( $vendor_items )
		foreach( $vendor_items as $vid => $items ) {
			$packages[ $vid ] = array(
			'label' => $items[0]['vendor_title'],
			'contents' => $items,
			'contents_cost' => array_sum( wp_list_pluck( $items, 'line_total' ) ),
			'applied_coupons' => WC()->cart->applied_coupons,
			'destination' => array(
				'country' => WC()->customer->get_shipping_country(),
				'state' => WC()->customer->get_shipping_state(),
				'postcode' => WC()->customer->get_shipping_postcode(),
				'city' => WC()->customer->get_shipping_city(),
				'address' => WC()->customer->get_shipping_address(),
				'address_2' => WC()->customer->get_shipping_address_2()
			)
			);
		}
		
		if ( $store_items ) {
			$packages[0] = array(
			'contents' => $store_items,
			'contents_cost' => array_sum( wp_list_pluck( $store_items, 'line_total' ) ),
			'applied_coupons' => WC()->cart->applied_coupons,
			'destination' => array(
				'country' => WC()->customer->get_shipping_country(),
				'state' => WC()->customer->get_shipping_state(),
				'postcode' => WC()->customer->get_shipping_postcode(),
				'city' => WC()->customer->get_shipping_city(),
				'address' => WC()->customer->get_shipping_address(),
				'address_2' => WC()->customer->get_shipping_address_2()
			)
			);
		}    
		*/

		return $packages;

	}


	function ign_drop_ship_add_order_item_meta( $item_id, $values, $cart_item_key ) { 
		global $woocommerce;
		
		if ( empty( $this->settings ) ) 
			$this->settings = get_option( 'woocommerce_ups_drop_shipping_rate_settings' );

		if ( empty( $this->settings['enabled'] ) || 'yes' != $this->settings['enabled'] )
			return;
				
		if ( empty( $this->settings['shipping_style'] ) || 'multi_package' !== $this->settings['shipping_style'] )
			return;
		
		if ( empty( $_POST['shipping_method'] ) )
			return;
			
		if ( !is_array( $_POST['shipping_method'] ) )
			return;
		
		$packages = $woocommerce->cart->get_shipping_packages();

		if ( count( $packages ) < 1 )
			return;
		
		$i = 0;
		
		$packages = WC()->shipping->get_packages();

		foreach ( $packages as $i => $package ) {

			if ( $cart_item_key != $package['contents'][0]['shipping_item_key'] ) {
				continue;
			}
			
			if ( isset( $package['rates'][ $woocommerce->checkout->shipping_methods[ $i ] ] ) ) {
				
				$method = $package['rates'][ $woocommerce->checkout->shipping_methods[ $i ] ];

				if ( 'ups_drop_shipping_rate' != $method->method_id )
					continue; 

			 	// Ensure visible fields can be translated, store it twice
				wc_add_order_item_meta( $item_id, __( 'Shipping Method', 'woocommerce' ), $method->label );
				wc_add_order_item_meta( $item_id, '_ign_shipping_method', $method->id );
				
				wc_add_order_item_meta( $item_id, __( 'Shipping Cost', 'woocommerce' ), $method->cost );
				wc_add_order_item_meta( $item_id, '_ign_shipping_cost', $method->cost );
				

			}
		}

		
	}
	
		
	function hide_order_meta( $meta = array() ) { 
	
		$meta[] = '_ign_shipping_cost';
		
		$meta[] = '_ign_shipping_method';
		
		return $meta;
	
	}

}

$ign_ups_drop_shipper = new IGN_UPS_Drop_Shipper();


if ( ! function_exists( 'ignitewoo_queue_update' ) )
	require_once( dirname( __FILE__ ) . '/ignitewoo_updater/ignitewoo_update_api.php' );

$this_plugin_base = plugin_basename( __FILE__ );

add_action( "after_plugin_row_" . $this_plugin_base, 'ignite_plugin_update_row', 1, 2 );

ignitewoo_queue_update( plugin_basename( __FILE__ ), 'daccf803a05591fd008fe22451af8924', '11463' );


