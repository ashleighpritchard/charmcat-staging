<?php
// Direct access security
if (!defined('TM_FP_PLUGIN_SECURITY')){
	die();
}
?>
<div 
id="tm-final-price-total" 
class="tm-final-price-total" 
data-is-sold-individually="<?php echo $is_sold_individually;?>" 
data-type="<?php echo $type;?>" 
data-price="<?php echo $price;?>"
data-product-price-rules="<?php echo $product_price_rules;?>" 
data-taxable="<?php echo $taxable;?>" 
data-tax-rate="<?php echo $tax_rate;?>" 
data-tax-string="<?php echo $tax_string;?>" 
data-tax-display-mode="<?php echo $tax_display_mode;?>" 
data-prices-include-tax="<?php echo $prices_include_tax;?>" 
data-variations="<?php echo $variations;?>"></div>