<?php

class PF_Affiliate_Status_My_Account_Endpoint {

	/**
	 * Custom endpoint name.
	 *
	 * @var string
	 */
	public static $affiliate_endpoint = 'affiliate-status';

	/**
	 * Plugin actions.
	 */
	public function __construct() {
		// Actions used to insert a new endpoint in the WordPress.
		add_action( 'init', array( $this, 'add_endpoints' ) );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );

		// Change the My Accout page title.
		add_filter( 'the_title', array( $this, 'endpoint_title' ) );

		// Insering your new tab/page into the My Account page.
		add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
		add_action( 'woocommerce_account_' . self::$affiliate_endpoint .  '_endpoint', array( $this, 'endpoint_content' ) );
	}

	/**
	 * Register new endpoint to use inside My Account page.
	 *
	 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
	 */
	public function add_endpoints() {
		add_rewrite_endpoint( self::$affiliate_endpoint, EP_ROOT | EP_PAGES );
	}

	/**
	 * Add new query var.
	 *
	 * @param array $vars
	 * @return array
	 */
	public function add_query_vars( $vars ) {
		$vars[] = self::$affiliate_endpoint;

		return $vars;
	}

	/**
	 * Set endpoint title.
	 *
	 * @param string $title
	 * @return string
	 */
	public function endpoint_title( $title ) {
		global $wp_query;

		$is_endpoint = isset( $wp_query->query_vars[ self::$affiliate_endpoint ] );

		if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
			// New page title.
			$title = __( 'Affiliate Status', 'woocommerce' );

			remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
		}

		return $title;
	}

	/**
	 * Insert the new endpoint into the My Account menu if the user has the role of affiliate.
	 *
	 * @param array $items
	 * @return array
	 */
    
	public function new_menu_items( $items ) {
        global $current_user; // Use global
        get_currentuserinfo(); // Make sure global is set, if not set it.
        
        if ( user_can( $current_user, "affiliate" ) || user_can( $current_user, "administrator" )) {// if user is affiliate

            // Remove the logout menu item.
            $logout = $items['customer-logout'];
            unset( $items['customer-logout'] );

            // Insert your custom endpoint.
            $items[ self::$affiliate_endpoint ] = __( 'Affiliate Status', 'woocommerce' );

            // Insert back the logout item.
            $items['customer-logout'] = $logout;

            return $items;
            
        }else{ //if user is not affiliate

            return;
        }

	}

	/**
	 * Endpoint HTML content.
	 */
    public function endpoint_content() {
                        
        echo PF_Affiliate_Totals::display_affiliate_orders_table();
        
	}

}

	/**
	 * Plugin install action.
	 * Flush rewrite rules to make our custom endpoint available.
	 */
    function install() {
		flush_rewrite_rules();
	}

new PF_Affiliate_Status_My_Account_Endpoint();

/**
 * Flush rewrite rules on plugin activation.
 */
function pf_affiliate_flush_rewrite_rules() {
    flush_rewrite_rules();
}

register_activation_hook( __FILE__, 'pf_affiliate_flush_rewrite_rules' );
register_deactivation_hook( __FILE__, 'pf_affiliate_flush_rewrite_rules' );
