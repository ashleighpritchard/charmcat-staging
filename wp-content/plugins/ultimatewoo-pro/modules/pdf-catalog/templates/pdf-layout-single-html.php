<?php
/**
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wc_posts;

?>

	<div class="single">
	<?php

		foreach( $wc_posts as $post ) {

			// create the product object
			$product = get_product( $post );

			$single_image_size = apply_filters( 'wc_store_catalog_pdf_download_single_image_size', array( 800, 9999, false ), $product );
	?>
			<?php do_action( 'wc_store_catalog_pdf_download_before_product', $product ); ?>
			
			<h2><a href="<?php echo get_permalink( $product->id ); ?>"><?php echo apply_filters( 'wc_store_catalog_pdf_download_show_product_title', $product->post->post_title, $product ); ?></a></h2>

			<a href="<?php echo get_permalink( $product->id ); ?>" class="single-image-link"><?php echo apply_filters( 'wc_store_catalog_pdf_download_show_product_image', has_post_thumbnail( $post ) ? $product->get_image( $single_image_size ) : WC_Store_Catalog_PDF_Download_Ajax::get_placeholder_image( array() ), $product ); ?></a>
			
			<div class="clear"></div>

			<?php echo apply_filters( 'wc_store_catalog_pdf_download_show_product_price', $product->get_price_html() ); ?>
			
			<p class="description"><?php echo apply_filters( 'wc_store_catalog_pdf_download_description', $product->post->post_content, $product ); ?></p>

			<?php apply_filters( 'wc_store_catalog_pdf_download_product_meta', include( WC_Store_Catalog_PDF_Download_Ajax::get_product_meta_template( $product ) ) ); ?>

			<?php do_action( 'wc_store_catalog_pdf_download_product_attr', $product ); ?>

			<?php do_action( 'wc_store_catalog_pdf_download_after_product', $product ); ?>
	<?php
		} 
	?>
	</div>
