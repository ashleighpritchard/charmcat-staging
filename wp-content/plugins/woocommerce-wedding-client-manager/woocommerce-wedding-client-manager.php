<?php

/**
 * Plugin Name: WooCommerce Wedding Client Manager
 * Description: Book, manage and interact with wedding and event clients through any WooCommerce-powered website.
 * Version: 0.1
 * Author: CharmCat
 * Author URI: http://charmcat.net/
 * Developer: Ashleigh Pritchard
 * *
 * Copyright: © 2016 CharmCat Stationery & Design LLC
 */

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

require_once("woocommerce-wedding-client-manager-myaccount-endpoint.php");