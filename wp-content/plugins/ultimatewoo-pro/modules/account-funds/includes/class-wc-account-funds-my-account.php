<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_Account_Funds_My_Account
 */
class WC_Account_Funds_My_Account {

	/**
	 * Constructor
	 */
	public function __construct() {
		add_action( 'wp', array( $this, 'topup_handler' ) );
		add_action( 'woocommerce_before_my_account', array( $this, 'my_account' ) );
	}

	/**
	 * Handle top-ups
	 */
	public function topup_handler() {
		if ( isset( $_POST['wc_account_funds_topup'] ) && isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'account-funds-topup' ) ) {
			$min          = max( 0, get_option( 'account_funds_min_topup' ) );
			$max          = get_option( 'account_funds_max_topup' );
			$topup_amount = wc_clean( $_POST['topup_amount'] );

			if ( $topup_amount < $min ) {
				wc_add_notice( sprintf( __( 'The minimum amount that can be topped up is %s', 'ultimatewoo-pro' ), wc_price( $min ) ), 'error' );
				return;
			} elseif ( $max && $topup_amount > $max ) {
				wc_add_notice( sprintf( __( 'The maximum amount that can be topped up is %s', 'ultimatewoo-pro' ), wc_price( $max ) ), 'error' );
				return;
			}

			WC()->cart->add_to_cart( wc_get_page_id( 'myaccount' ), true, '', '', array( 'top_up_amount' => $topup_amount ) );
			
			if ( 'yes' === get_option( 'woocommerce_cart_redirect_after_add' ) ) {
				wp_redirect( get_permalink( wc_get_page_id( 'cart' ) ) );
			}
		}
	}

	/**
	 * Show funds on account page
	 */
	public function my_account() {
		$funds = WC_Account_Funds::get_account_funds();

		echo '<h2>'. __( 'Account Funds', 'ultimatewoo-pro' ) .'</h2>';
		echo '<p>'. sprintf( __( 'You currently have <strong>%s</strong> worth of funds in your account.', 'ultimatewoo-pro' ), $funds ) . '</p>';

		if ( 'yes' === get_option( 'account_funds_enable_topup' ) ) {
			$this->my_account_topup();
		} else {
			$this->my_account_products();
		}

		$this->my_account_orders();
	}

	/**
	 * Show top up form
	 */
	public function my_account_topup() {
		$min_topup     = get_option( 'account_funds_min_topup' );
		$max_topup     = get_option( 'account_funds_max_topup' );
		$items_in_cart = $this->_get_topup_items_in_cart();
		$topup_in_cart = array_shift( $items_in_cart );
		if ( ! empty( $max_topup ) && ! empty( $topup_in_cart ) ) {
			printf(
				'<p class="woocommerce-info"><a href="%s" class="button wc-forward">%s</a> %s</p>',
				wc_get_page_permalink( 'cart' ),
				__( 'View Cart', 'ultimatewoo-pro' ),
				sprintf( __( 'You have "%s" in your cart.', 'ultimatewoo-pro' ), $topup_in_cart['data']->get_title() )
			);
			return;
		}
		?>

		<form method="post">
			<h3><label for="topup_amount"><?php _e( 'Top-up Account Funds', 'ultimatewoo-pro' ); ?></label></h3>
			<p class="form-row form-row-first">
				<input type="number" class="input-text" name="topup_amount" id="topup_amount" step="0.01" value="<?php echo esc_attr( $min_topup ); ?>" min="<?php echo esc_attr( $min_topup ); ?>" max="<?php echo esc_attr( $max_topup ); ?>" />
				<?php if ( $min_topup || $max_topup ) : ?>
				<span class="description">
					<?php
					printf(
						'%s%s%s',
						$min_topup ? sprintf( __( 'Minimum: <strong>%s</strong>.', 'ultimatewoo-pro' ), wc_price( $min_topup ) ) : '',
						$min_topup && $max_topup ? ' ' : '',
						$max_topup ? sprintf( __( 'Maximum: <strong>%s</strong>.', 'ultimatewoo-pro' ), wc_price( $max_topup ) ) : ''
					);
					?>
				</span>
				<?php endif; ?>
			</p>
			<p class="form-row">
				<input type="hidden" name="wc_account_funds_topup" value="true" />
				<input type="submit" class="button" value="<?php _e( 'Top-up', 'ultimatewoo-pro' ); ?>" />
			</p>
			<?php wp_nonce_field( 'account-funds-topup' ); ?>
		</form>
		<?php
	}

	/**
	 * Get topup items in cart.
	 *
	 * @since 2.0.6
	 *
	 * @return bool
	 */
	private function _get_topup_items_in_cart() {
		$topup_items = array();
		if ( ! WC()->cart->is_empty() ) {
			$topup_items = array_filter( WC()->cart->get_cart(), array( $this, 'filter_topup_items' ) );
		}

		return $topup_items;
	}

	/**
	 * Cart items filter callback to filter topup product.
	 *
	 * @since 2.0.6
	 *
	 * @return bool Returns true if item is topup product
	 */
	public function filter_topup_items( $item ) {
		if ( isset( $item['data'] ) && is_callable( array( $item['data'], 'get_type' ) ) ) {
			return ( 'topup' === $item['data']->get_type() );
		}

		return false;
	}

	/**
	 * Show top up products
	 */
	private function my_account_products() {
		$product_ids = get_posts( array(
			'post_type' => 'product',
			'tax_query' => array(
				array(
					'taxonomy' => 'product_type',
					'field'    => 'slug',
					'terms'    => 'deposit',
				)
			),
			'fields' => 'ids'
		) );
		if ( $product_ids ) {
			echo do_shortcode( '[products ids="' . implode( ',', $product_ids ) . '"]' );
		}
	}

	/**
	 * Show deposits
	 */
	private function my_account_orders() {
		$deposits = get_posts( array(
			'numberposts' => 10,
			'meta_key'    => '_customer_user',
			'meta_value'  => get_current_user_id(),
			'post_type'   => 'shop_order',
			'post_status' => array( 'wc-completed', 'wc-processing', 'wc-on-hold' ),
			'meta_query'  => array(
				array(
					'key'   => '_funds_deposited',
					'value' => '1',
				)
			)
		) );

		if ( $deposits ) {
			?>
			<h2><?php _e( 'Recent Deposits', 'ultimatewoo-pro' ); ?></h2>
			<table class="shop_table my_account_deposits">
				<thead>
					<tr>
						<th class="order-number"><span class="nobr"><?php _e( 'Order', 'ultimatewoo-pro' ); ?></span></th>
						<th class="order-date"><span class="nobr"><?php _e( 'Date', 'ultimatewoo-pro' ); ?></span></th>
						<th class="order-total"><span class="nobr"><?php _e(' Status', 'ultimatewoo-pro' ); ?></span></th>
						<th class="order-status"><span class="nobr"><?php _e( 'Amount Funded', 'ultimatewoo-pro' ); ?></span></th>
					</tr>
				</thead>
				<tbody><?php
					foreach ( $deposits as $deposit ) :
						$order  = new WC_Order();
						$order->populate( $deposit );
						$funded = 0;

						foreach ( $order->get_items() as $item ) {
							$product = $order->get_product_from_item( $item );

							if ( $product->is_type( 'deposit' ) || $product->is_type( 'topup' ) ) {
								$funded += $order->get_line_total( $item );
							}
						}
						?><tr class="order">
						<td class="order-number" data-title="<?php _e( 'Order Number', 'ultimatewoo-pro' ); ?>">
							<a href="<?php echo $order->get_view_order_url(); ?>">
								#<?php echo $order->get_order_number(); ?>
							</a>
						</td>
						<td class="order-date" data-title="<?php _e( 'Date', 'ultimatewoo-pro' ); ?>">
							<time datetime="<?php echo date( 'Y-m-d', strtotime( $order->order_date ) ); ?>" title="<?php echo esc_attr( strtotime( $order->order_date ) ); ?>"><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></time>
						</td>
						<td class="order-status" data-title="<?php _e( 'Status', 'ultimatewoo-pro' ); ?>" style="text-align:left; white-space:nowrap;">
							<?php echo wc_get_order_status_name( $order->get_status() ); ?>
						</td>
						<td class="order-total">
							<?php echo wc_price( $funded ); ?>
						</td>
					</tr><?php
					endforeach;
				?></tbody>
			</table>
			<?php
		}
	}
}

new WC_Account_Funds_My_Account();
