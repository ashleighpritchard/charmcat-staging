<?php
/**
 * Plugin Name: Colour Brightness
 * Description: Adds Color Brighness funtion
 * Version: 1
 * Author: CharmCat
 * Author URI: http://charmcat.net/
 * Developer: Ashleigh Pritchard
 * *
 * Copyright: © 2016 CharmCat Stationery & Design LLC
 */

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}


//**********INITIALIZE**********//
//Add the Colour Brightness Plugin
function add_brighteness_plugin() {
    wp_enqueue_script( 'colour-brightness', '/wp-content/plugins/colour-brightness/jquery.colourbrightness.min.js', array('jquery'));	
}
add_action('wp_enqueue_scripts', 'add_brighteness_plugin');

?>